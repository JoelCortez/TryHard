﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace TryHard_
{
    public partial class frm_record : Form
    {
        private int seconds, minutes;
        private int state = 1;
        public static int exercise = 0;
        public static int number_exercise = 0;        
        WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        private Form form, frm_to_open;
        private Classes.Record_audio rc = new Classes.Record_audio();

        public frm_record()
        {
            InitializeComponent();
        }

        private void tmr_timer_Tick(object sender, EventArgs e)
        {
            string text_timer = "";
            seconds++;            
            if(seconds == 60)
            {
                minutes++;
                seconds = 0;
                text_timer = "0" + minutes + ":00";
            }
            else
            {
                if(seconds < 10)
                {
                    text_timer = "0" + minutes + ":0" + seconds;
                }
                else if(seconds > 9 && seconds < 60)
                {
                    text_timer = "0" + minutes + ":" + seconds;
                }
            }
            lbl_timer.Text = text_timer;
        }

        private void frm_record_Load(object sender, EventArgs e)
        {
            seconds = 0;
            minutes = 0;
            form = this.Owner;
        }

        private void btn_record_Click(object sender, EventArgs e)
        {
            wmp.URL = "";
            if (state == 1)
            {
                tmr_timer.Start();
                btn_record.BackgroundImage = Properties.Resources.stop2;
                state = 2;
                btn_play.Visible = false;
                btn_stop.Visible = false;
                rc.record();
            }
            else if (state == 2)
            {
                select_part();
                Classes.Functions.Path = @"C:\TryHard\buffer\" + Classes.Vars.Name_File;
                rc.save_recording();
                lbl_timer.Text = "00:00";
                seconds = 0;
                minutes = 0;
                tmr_timer.Stop();
                btn_record.BackgroundImage = Properties.Resources.record2;
                state = 1;
                btn_play.Visible = true;
                btn_stop.Visible = true;
                save_filename();
                if (number_exercise != 0)
                {
                    if(Classes.Vars.State_exercise != 1)
                    {
                        form.Controls["lbl_done_" + number_exercise.ToString()].Text = "Done!";                        
                    }
                    else
                    {
                        form.Controls["lbl_done_" + number_exercise.ToString()].Text = "Changed!";
                    }
                    form.Controls["lbl_done_" + number_exercise.ToString()].Visible = true;
                }
                else
                {
                    if (Classes.Vars.State_exercise != 1)
                    {
                        form.Controls["lbl_done"].Text = "Done!";
                    }
                    else
                    {
                        form.Controls["lbl_done"].Text = "Changed!";
                    }
                    form.Controls["lbl_done"].Visible = true;
                }                
            }

        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_start_stop_Click(object sender, EventArgs e)
        {
            tmr_timer.Start();
        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            play_audio();
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void play_audio()
        {
            try
            {
                wmp.settings.volume = 100;
                wmp.URL = Classes.Functions.Path;
                wmp.controls.play();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frm_record_FormClosing(object sender, FormClosingEventArgs e)
        {
            wmp.controls.stop();
            wmp.URL = "";
        }

        private void select_part()
        {
            switch (exercise)
            {
                case 1:
                    Classes.Vars.Name_File = Classes.Vars.ID + "_respond_" + number_exercise + "_" + Classes.Vars.Number_toeic + ".mp3";
                    break;
                case 2:
                    Classes.Vars.Name_File = Classes.Vars.ID + "_respond_provided_" + number_exercise + "_" + Classes.Vars.Number_toeic + ".mp3";
                    break;
                case 3:
                    Classes.Vars.Name_File = Classes.Vars.ID + "_propose_" + Classes.Vars.Number_toeic + ".mp3";
                    break;
                case 4:
                    Classes.Vars.Name_File = Classes.Vars.ID + "_express_" + Classes.Vars.Number_toeic + ".mp3";
                    break;
            }
        }

        private void save_filename()
        {
            switch (exercise)
            {
                case 1:
                    frm_create_respondquestions.filenames[number_exercise - 1] = Classes.Functions.Path;
                    break;
                case 2:
                    frm_create_infoprovided.filenames[number_exercise - 1] = Classes.Functions.Path;
                    break;
                case 3:
                    frm_create_proposesolution.filename = Classes.Functions.Path;
                    break;
                case 4:
                    frm_create_expressopinion.filename = Classes.Functions.Path;
                    break;
            }
        }
    }
}
