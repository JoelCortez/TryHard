﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace TryHard_
{
    public partial class frm_microphone_verification : Form
    {
        WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        Classes.TimeTest timer;
        public frm_microphone_verification()
        {
            InitializeComponent();
            wmp.settings.volume = 100;
        }

        private void btn_record_Click(object sender, EventArgs e)
        {
            wmp.URL = "";
            btn_play.Visible = false;
            btn_stop.Visible = false;
            timer = new Classes.TimeTest();
            timer.test_microphone(lbl_timer, btn_play, btn_record, 10, "10");
            btn_record.Visible = false;
        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            wmp.URL = Classes.Functions.Path;
            wmp.controls.play();
            btn_play.Visible = false;
            btn_stop.Visible = true;
        }

        private void btn_continue_Click(object sender, EventArgs e)
        {
            //this.Close();
            //MessageBox.Show(this.Width.ToString());
            //Classes.Functions fun = new Classes.Functions();
            Form frm = new frm_directions();
            frm.TopLevel = false;            
            Classes.Functions.change_form(frm, Classes.Vars.Last_opened.ToString());
            Classes.Vars.Last_opened = "frm_directions";
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            btn_play.Visible = true;
            btn_stop.Visible = false;
            wmp.controls.stop();
        }

        Form change_form()
        {
            Form frm = new Form();
            switch (Classes.Vars.Part_selection)
            {
                case 4: frm = new frm_readaloud();
                    break;
            }            
            frm.TopLevel = false;
            return frm;
        }
    }
}
