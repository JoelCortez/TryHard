﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_login : Form
    {
        Thread t1,t2;
        public string[] texto;
        public frm_login()
        {
            InitializeComponent();
        }

        private void frm_login_Load(object sender, EventArgs e)
        {
            txt_id.Text = "Enter ID";            
            txt_password.UseSystemPasswordChar = false;
            txt_password.Text = "Enter Password";
            txt_password.GotFocus += new System.EventHandler(RemoveText2);
            try
            {
                Classes.Functions fun = new Classes.Functions();
                Classes.Functions.Path = @"C:\TryHard\Data";
                fun.CreateFolder();
                Classes.Functions.Path = @"C:\TryHard\Names";
                fun.CreateFolder();
            }
            catch (Exception)
            {                
            }
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            //Application.ex
            //this.Close();
            Application.Exit();
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(Classes.Cifrado.Desencriptar("XChyDNLU+zI="));
            this.Refresh();
            this.WindowState = FormWindowState.Minimized;
            this.Refresh();
        }

        private void lkl_createAccount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form singup = new frm_signup_datas();
            singup.Show();
            this.Hide();
        }

        private void txt_id_TextChanged(object sender, EventArgs e)
        {
        }     

        public void RemoveText(object sender, EventArgs e)
        {
            if (txt_id.Text == "Enter ID")
            {
                txt_id.Text = "";
            }
        }
        public void RemoveText2(object sender, EventArgs e)
        {
            txt_password.Text = "";
            txt_password.UseSystemPasswordChar = true;
        }

        private void txt_id_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Space)
            {
                e.Handled = true;
            }
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                Thread pressed = new Thread(key_pressed);
                pressed.Start();

            }            
        }

        private void key_pressed()
        {
            this.Invoke(new MethodInvoker(delegate {
                txt_id.ReadOnly = true;
                txt_password.ReadOnly = false;
            }));
            t1 = new Thread(() =>
            {

                txt_password.Invoke(new MethodInvoker(delegate
                {
                    txt_password.UseSystemPasswordChar = false;
                    txt_password.Text = "Enter Password";
                }));
                ID();
                PASS();
                btn_back.Invoke(new MethodInvoker(delegate { btn_back.Visible = true; }));
                this.Invoke((MethodInvoker)delegate
                {
                    Classes.Vars.FullName = Classes.Cifrado.Desencriptar(Classes.Vars.Name) + " " + Classes.Cifrado.Desencriptar(Classes.Vars.Last_Names);
                    lbl_info.Text = Classes.Vars.FullName;
                    int anchoP = pnl_id.Width / 2;
                    int anchoL = lbl_info.Width / 2;
                    lbl_info.Location = new Point(anchoP - anchoL, 81);
                    lbl_info.Visible = true;

                });


            });
            Classes.Vars.ID = Classes.Cifrado.Encriptar(txt_id.Text);
            string id = Classes.Vars.ID + ".tryhard";
            Classes.Mega host = new Classes.Mega();
            string path = @"C:\TryHard\Names\" + id;
            if (File.Exists(path))
            {
                this.Invoke(new MethodInvoker(delegate {
                    txt_id.ReadOnly = true;
                    txt_password.ReadOnly = false;
                }));
                texto = File.ReadAllLines(path);
                Classes.Vars.Privilege = texto[0];
                Classes.Vars.Name = texto[1];
                Classes.Vars.Last_Names = texto[2];
                Classes.Vars.Password = texto[3];
                Classes.Vars.Code = texto[4];
                if (Classes.Cifrado.Desencriptar(Classes.Vars.Privilege) != "User")
                {
                    Classes.Vars.State = 1;
                }
                else
                {
                    Classes.Vars.Provider = texto[5];
                    Classes.Vars.State = 2;
                }

                t1.Start();
            }
            else
            {
                if (host.fileExists(id) == true)
                {
                    if (id != "d")
                    {
                        txt_id.ReadOnly = true;
                        txt_password.ReadOnly = false;
                        Thread download = new Thread(() =>
                        {                            
                            try
                            {
                                if (!File.Exists(path))
                                {
                                    host.download(path, id);
                                }
                                else
                                {
                                    //File.Delete(path);
                                    //host.download(path, id);
                                }
                                texto = File.ReadAllLines(path);
                                Classes.Vars.Privilege = texto[0];
                                Classes.Vars.Name = texto[1];
                                Classes.Vars.Last_Names = texto[2];
                                Classes.Vars.Password = texto[3];
                                Classes.Vars.Code = texto[4];
                                if (Classes.Cifrado.Desencriptar(Classes.Vars.Privilege) != "User")
                                {
                                    Classes.Vars.State = 1;
                                }
                                else
                                {
                                    Classes.Vars.Provider = texto[5];
                                    Classes.Vars.State = 2;
                                }

                                t1.Start();
                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show(ex.Message, "Error");

                            }
                        });
                        download.Start();
                    }
                    else
                    {
                        MessageBox.Show("Incorrect ID", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        host.mega.Logout();
                        //txt_id.Text = "";
                        this.Invoke(new MethodInvoker(delegate {
                            txt_id.ReadOnly = false;
                            txt_password.ReadOnly = true;
                            txt_id.Focus();
                        }));
                    }
                }
                else
                {
                    MessageBox.Show("Incorrect ID", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    host.mega.Logout();
                    //txt_id.Text = "";
                    this.Invoke(new MethodInvoker(delegate {
                        txt_id.ReadOnly = false;
                        txt_password.ReadOnly = true;
                        txt_id.Focus();
                    }));                    
                }
            }
        }

        private void PASS()
        {
            this.Invoke((MethodInvoker)delegate
            {
                for (int i = 311; i >= 63; i -= 31)
                {
                    txt_password.Location = new Point(i, 101);
                    this.Refresh();
                    txt_password.Refresh();
                }
            });
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            txt_id.ReadOnly = false;
            t2 = new Thread(() =>
             {
                 this.Invoke((MethodInvoker)delegate
                 {
                     this.Invoke((MethodInvoker)delegate
                     {
                         btn_back.Visible = false;
                         txt_id.Text = "Enter ID";
                         lbl_info.Visible = false;
                     });
                     for (int i = 63; i <= 311; i += 31)
                     {
                         txt_password.Location = new Point(i, 101);
                         this.Refresh();
                         txt_password.Refresh();
                     }
                     for (int i = -185; i <= 63; i += 31)
                     {
                         txt_id.Location = new Point(i, 101);
                         this.Refresh();
                         txt_id.Refresh();
                     }
                 });
                 
             });
            t2.Start();
        }

        private void txt_password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                if (Classes.Cifrado.Encriptar(txt_password.Text) == Classes.Vars.Password)
                {

                    frm_panel panel = new frm_panel();
                    Classes.Vars.form = panel;
                    panel.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Incorrect Password","Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void ID()
        {
            this.Invoke((MethodInvoker)delegate
            {
                for (int i = 63; i >= -185; i-=31)
                {
                    txt_id.Location = new Point(i, 101);
                    this.Refresh();
                    txt_id.Refresh();
                }
            });
        }
    }
}
