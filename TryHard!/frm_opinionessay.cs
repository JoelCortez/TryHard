﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;


namespace TryHard_
{
    public partial class frm_opinionessay : Form
    {
        Classes.Performance performance;

        public frm_opinionessay()
        {
            InitializeComponent();
        }

        private void txt_response_TextChanged(object sender, EventArgs e)
        {
            lbl_quantity.Text = "Words: " + count_words(txt_response.Text).ToString();
        }

        private int count_words(string text)
        {
            MatchCollection words = Regex.Matches(text, @"\S+");
            return words.Count;
        }

        private void frm_opinionessay_Load(object sender, EventArgs e)
        {
            Classes.Vars.txt_response = txt_response;
            Classes.Vars.panel_timer_controller.Visible = true;
            txt_response.Focus();
        }

        private void frm_opinionessay_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            lbl_statement.Text = File.ReadAllText(@"C:\TryHard\Files\Opinion\" + Classes.Vars.Provider + "_statement_" + Classes.Vars.Number_toeic + ".tryhard");
            performance.opinion_essay();
        }
    }
}
