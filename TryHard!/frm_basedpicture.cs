﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_basedpicture : Form
    {
        private Classes.Performance performance;

        public frm_basedpicture()
        {
            InitializeComponent();
        }

        private void frm_basedpicture_Load(object sender, EventArgs e)
        {
            Classes.Vars.panel_timer_controller.Visible = true;
            Classes.Vars.label_number_sentence = lbl_number_sentence;
            Classes.Vars.label_words = lbl_words;
            Classes.Vars.txt_sentences = txt_sentences;
            Classes.Vars.pbx_picture = pbx_picture;
        }

        private void frm_basedpicture_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            //lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\ReadingAloud\" + Classes.Vars.Provider + "_reading_" + 1 + "_" + Classes.Vars.Number_toeic + ".tryhard");
            //Classes.Vars.label_timer.Text = "00:45";
            performance.based();
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            performance.next_sentence();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            performance.back_sentence();
        }
    }
}
