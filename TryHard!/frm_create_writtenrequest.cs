﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_create_writtenrequest : Form
    {
        public frm_create_writtenrequest()
        {
            InitializeComponent();
        }
        Classes.Functions fun = new Classes.Functions();
        Classes.SQLite sql = new Classes.SQLite();
        public static string[] texts;
        public static string[] directions;
        private void frm_create_writtenrequest_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            texts = new string[2];
            directions = new string[2];
            if (Classes.Vars.State_exercise == 1)
            {
                btn_next.Text = "Close";
                btn_save.Text = "Modify";
                Classes.Functions.Path = @"C:\TryHard\Files\Written";
                string path, auxiliar;
                for (int i = 0; i < 2; i++)
                {
                    path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_direction_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".tryhard";
                    directions[i] = File.ReadAllText(path);
                    auxiliar = path.Replace("direction", "email");
                    texts[i] = File.ReadAllText(auxiliar);
                    tbc_direction.Controls["tb_dir_" + (i + 1).ToString()].Controls["txt_email_dir_" + (i + 1).ToString()].Text = directions[i];
                    tbc_email.Controls["tb_email_" + (i + 1).ToString()].Controls["txt_email_" + (i + 1).ToString()].Text = texts[i];
                }
                for (int i = 0; i < 2; i++)
                {
                    fun.DisableControls(tbc_direction.Controls["tb_dir_" + (i + 1).ToString()].Controls["txt_email_dir_" + (i + 1).ToString()], 2);
                    fun.DisableControls(tbc_email.Controls["tb_email_" + (i + 1).ToString()].Controls["txt_email_" + (i + 1).ToString()], 2);                    
                }
            }
            else
            {
                if (Classes.Vars.Part_selection == 11)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
        }

        private void save_email()
        {
            Classes.Functions.Path = @"C:\TryHard\Files\Written";
            fun.CreateFolder();
            string path, auxiliar;
            for (int i = 0; i < 2; i++)
            {
                path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_direction_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".tryhard";
                File.WriteAllText(path, directions[i]);
                auxiliar = path.Replace("direction", "email");
                File.WriteAllText(auxiliar, texts[i]);
            }
            sql.update_parts("written", 1);
            if (sql.exercise_state("written"))
            {
                Classes.Vars.parts[7] = 1;
            }
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            Classes.Vars.Editing = 1;
            if (check_email() == false)
            {
                try
                {
                    save_email();
                    Form frm = new Form();
                    if (btn_next.Text == "Close")
                    {
                        this.Close();
                        return;
                    }
                    else
                    {
                        if (btn_next.Text == "Finish")
                        {
                            if (Classes.Vars.State_exercise == 1)
                            {
                                this.Close();
                                return;
                            }
                            else
                            {
                                frm = new frm_allparts();
                            }
                        }
                        else
                        {
                            frm = new frm_create_opinionessay();
                        }
                    }
                    frm.Show();
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("You should complete all the parts", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }
        private bool check_email()
        {
            bool vf;
            for (int i = 0; i < 2; i++)
            {
                directions[i] = tbc_direction.Controls["tb_dir_" + (i + 1).ToString()].Controls["txt_email_dir_" + (i + 1).ToString()].Text;
                texts[i] = tbc_email.Controls["tb_email_" + (i + 1).ToString()].Controls["txt_email_" + (i + 1).ToString()].Text;
            }            
            if (String.IsNullOrWhiteSpace(texts[0]) || String.IsNullOrWhiteSpace(directions[0]) || String.IsNullOrWhiteSpace(texts[1]) || String.IsNullOrWhiteSpace(directions[1]))
            {
                vf = true;
            }
            else
            {
                vf = false;
            }
            return vf;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (btn_save.Text == "Modify")
            {
                for (int i = 0; i < 2; i++)
                {
                    fun.DisableControls(tbc_direction.Controls["tb_dir_" + (i + 1).ToString()].Controls["txt_email_dir_" + (i + 1).ToString()], 1);
                    fun.DisableControls(tbc_email.Controls["tb_email_" + (i + 1).ToString()].Controls["txt_email_" + (i + 1).ToString()], 1);
                }
                btn_next.Text = "Finish";
                this.Controls.Remove(btn_save);
            }
            else if (btn_save.Text == "Save")
            {
                if (check_email() == false)
                {
                    try
                    {
                        save_email();
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                }
            }
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }
    }
}
