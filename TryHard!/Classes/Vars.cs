﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_.Classes
{
    class Vars
    {
        public static Form form;
        public static Panel panel_container, panel_timer_controller;
        public static Label lbl_text, lbl_introduction, label_timer, label_topic_timer, label_words , label_number_sentence, label_part, label_direction;
        public static PictureBox pbx_picture;
        public static TextBox txt_sentences, txt_response, txt_email;

        protected static string
            privilege,
            name,
            last_names,
            id,
            code,
            password,
            name_file,
            provider,
            last_opened,
            full_name;

        protected static int state, part_selection, number_toeic, state_exercise, number_picture, total_exercises, upload_state, editing, charging;
        protected static bool performing;
        public static int[] parts = new int[9];

        public static string FullName
        {
            set
            {
                full_name = value;
            }
            get
            {
                return full_name;
            }
        }

        public static bool Performing
        {
            set
            {
                performing = value;
            }
            get
            {
                return performing;
            }
        }

        public static int Charging
        {
            set
            {
                charging = value;
            }
            get
            {
                return charging;
            }
        }

        public static int Editing
        {
            set
            {
                editing = value;
            }
            get
            {
                return editing;
            }
        }

        public static string Last_opened
        {
            set
            {
                last_opened = value;
            }
            get
            {
                return last_opened;
            }
        }

        public static int Total_Exercises
        {
            set
            {
                total_exercises = value;
            }
            get
            {
                return total_exercises;
            }
        }

        public static int Upload_state
        {
            set
            {
                upload_state = value;
            }
            get
            {
                return upload_state;
            }
        }

        public static int Number_Picture
        {
            set
            {
                number_picture = value;
            }
            get
            {
                return number_picture;
            }
        }

        public static string Privilege
        {
            get
            {
                return privilege;
            }
            set
            {
                privilege = value;
            }
        }

        public static string Name
        {
            set
            {
                name = value;
            }
            get
            {
                return name;
            }
        }

        public static string Last_Names
        {
            set
            {
                last_names = value;
            }
            get
            {
                return last_names;
            }
        }

        public static string ID
        {
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }

        public static string Password
        {
            set
            {
                password = value;
            }
            get
            {
                return password;
            }
        }
        
        public static string Code
        {
            set
            {
                code = value;
            }
            get
            {
                return code;
            }
        }
        public static int State
        {
            set
            {
                state = value;
            }
            get
            {
                return state;
            }
        }
        public static int Part_selection
        {
            set
            {
                part_selection = value;
            }
            get
            {
                return part_selection;
            }
        }
        public static int Number_toeic
        {
            set
            {
                number_toeic = value;
            }
            get
            {
                return number_toeic;
            }
        }
        public static int State_exercise
        {
            set
            {
                state_exercise = value;
            }
            get
            {
                return state_exercise;
            }
        }
        public static String Name_File
        {
            set
            {
                name_file = value;
            }
            get
            {
                return name_file;
            }
        }
        public static string Provider
        {
            set
            {
                provider = value;
            }
            get
            {
                return provider;
            }
        }
        
        public static Panel panel_control(string tag)
        {
            Panel pnl = new Panel();
            pnl.Size = new System.Drawing.Size(647, 84);
            pnl.BackColor = System.Drawing.Color.White;
            pnl.BorderStyle = BorderStyle.FixedSingle;
            pnl.Tag = tag;
            pnl.Name = "toeic_" + tag;
            return pnl;
        }

        public static Label Label_Toeic( int number)
        {
            Label lbl = new Label();
            lbl.ForeColor = System.Drawing.Color.DeepSkyBlue;
            lbl.Font = new System.Drawing.Font("Cambria", 15, System.Drawing.FontStyle.Bold);
            lbl.BackColor = System.Drawing.Color.Transparent;
            lbl.Location = new System.Drawing.Point(80, 29);
            lbl.Text = "Test " + number.ToString();
            return lbl;
        }

        public static PictureBox icon_test()
        {
            PictureBox pbx = new PictureBox();
            pbx.Size = new System.Drawing.Size(50,50);
            pbx.Location = new System.Drawing.Point(20, 15);
            pbx.BackColor = System.Drawing.Color.White;
            pbx.BackgroundImage = Properties.Resources.toeic_icon;
            pbx.BackgroundImageLayout = ImageLayout.Stretch;
            return pbx;
        }

        public static Button btn_download(string tag)
        {
            Button btn = new Button();
            btn.Text = "Start";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            btn.Size = new System.Drawing.Size(115, 41);
            btn.ForeColor = System.Drawing.Color.White;
            btn.Font = new System.Drawing.Font("Calibri", 12, System.Drawing.FontStyle.Bold);
            btn.FlatStyle = FlatStyle.Flat;
            btn.Cursor = Cursors.Hand;
            btn.BackgroundImage = Properties.Resources.header;
            btn.BackgroundImageLayout = ImageLayout.Stretch;
            btn.Location = new System.Drawing.Point(516, 22);
            btn.Tag = tag;
            btn.Name = "btn_start_" + tag;
            btn.Click += new System.EventHandler(btn_start_event);
            return btn;
        }

        public static Button btn_modify(string tag)
        {
            Button btn = new Button();
            btn.Text = "Modify";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            btn.Size = new System.Drawing.Size(115, 41);
            btn.ForeColor = System.Drawing.Color.White;
            btn.Font = new System.Drawing.Font("Calibri", 12, System.Drawing.FontStyle.Bold);
            btn.FlatStyle = FlatStyle.Flat;
            btn.Cursor = Cursors.Hand;
            btn.BackgroundImage = Properties.Resources.header;
            btn.BackgroundImageLayout = ImageLayout.Stretch;
            //btn.Location = new System.Drawing.Point(390, 22);
            btn.Location = new System.Drawing.Point(516, 22);
            btn.Tag = tag;
            btn.Name = "btn_modify_" + tag;
            btn.Click += new System.EventHandler(btn_start_modifying);
            return btn;
        }

        private static void btn_start_event(object sender, EventArgs e)
        {
            //MessageBox.Show(((Button)sender).Name.ToString());
            Number_toeic = int.Parse(((Button)sender).Tag.ToString());
            frm_select_part frm = new frm_select_part();
            State_exercise = 2;
            frm.Show();
            form.Close();
        }

        private static void btn_start_modifying(object sender, EventArgs e)
        {
            //MessageBox.Show(((Button)sender).Name.ToString());      
            Number_toeic = int.Parse(((Button)sender).Tag.ToString());
            //parts = new int[9];
            for (int i = 0; i < 9; i++)
            {
                parts[i] = 0;
            }
            Total_Exercises = 0;
            Editing = 0;
            form.Close();
            frm_allparts frm = new frm_allparts();
            frm.Show();
        }
    }
}

//this.Load += new System.EventHandler(this.frm_Updownload_Load);