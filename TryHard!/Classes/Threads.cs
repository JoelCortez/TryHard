﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CG.Web.MegaApiClient;
using System.Threading;
using System.Data;
using System.IO;

namespace TryHard_.Classes
{
    class Threads : Functions
    {
        private Mega mega_host = new Mega();
        private Functions fun = new Functions();

        public static ProgressBar progressbar;
        public static Label label;
        public static Button button;
        private int process_upload = 0;
        public string db_;

        public static void SignUp(ProgressBar pb, Label lbl_info,Form frm_base, int privilege)
        {
            try
            {
                lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Searching file"; }));
                pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Connecting to Mega.nz"; }));
                Mega mega = new Mega();
                pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading file"; }));
                INode folder;
                if (privilege == 1)
                {
                    folder = mega.root("Manager");
                }
                else
                {
                    folder = mega.root("User");
                }
                mega.upload(Functions.Path, folder);
                pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Connecting to Database"; }));
                Queries query = new Queries();
                if (privilege == 1)
                {
                    query.query_insert_manager();
                }
                else
                {
                    query.query_insert_user();
                }                
                pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                MessageBox.Show("Sign Up sucessful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                frm_base.Invoke((MethodInvoker)delegate
                {
                    frm_panel panel = new frm_panel();
                    Vars.form = panel;
                    panel.Show();
                    //this.Hide();
                    frm_base.Close();
                });
                //frm_show.Invoke(new MethodInvoker(delegate { frm_show.Show(); }));
                //frm_hide.Invoke(new MethodInvoker(delegate { frm_hide.Close(); }));                
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Process Failed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                MessageBox.Show(ex.Message, "Error");
            }
        }
        

        public void Upload_files(DataTable table_speaking, DataTable table_writing, ProgressBar pb, Label lbl_info, int state)
        {
            //MessageBox.Show(frm_Updownload.process.ToString());
            //MessageBox.Show(table_speaking.Rows.Count.ToString() + " - " + table_speaking.Columns.Count.ToString());
            //lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Connecting to host"; }));
            bar_step(pb, lbl_info, "Connecting to host");
            pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
            if (state == 1)
            {
                //MessageBox.Show("DATA");                
                for (int i = 0; i < table_speaking.Rows.Count; i++)
                {
                    for (int j = 0; j < table_speaking.Columns.Count; j++)
                    {
                        if (table_speaking.Rows[i][j].ToString() != "0")
                        {
                            try
                            {
                                switch (j)
                                {
                                    case 0:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Read aloud exercises"; }));
                                        upload_read_aloud();
                                        process_upload++;
                                        break;
                                    case 1:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Describing a picture exercise"; }));
                                        upload_describe_picture();
                                        process_upload++;
                                        break;
                                    case 2:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Respond To Questions exercises"; }));
                                        upload_respond_questions();
                                        process_upload++;
                                        break;
                                    case 3:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Respond To Questions with information provided exercises"; }));
                                        upload_respond_provided();
                                        process_upload++;
                                        break;
                                    case 4:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Propose a solution exercise"; }));
                                        upload_propose_solution();
                                        process_upload++;
                                        break;
                                    case 5:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Express an opinion exercise"; }));
                                        upload_express_opinion();
                                        process_upload++;
                                        break;
                                }
                                //pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }

                //Writting
                for (int i = 0; i < table_writing.Rows.Count; i++)
                {
                    for (int j = 0; j < table_writing.Columns.Count; j++)
                    {
                        if (table_writing.Rows[i][j].ToString() != "0")
                        {
                            try
                            {
                                switch (j)
                                {
                                    case 0:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Based on a picture exercises"; }));
                                        upload_based_picture();
                                        process_upload++;
                                        break;
                                    case 1:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Wrtitten request exercises"; }));
                                        upload_written();
                                        process_upload++;
                                        break;
                                    case 2:
                                        lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Opinion essay exercise"; }));
                                        upload_opinion_essay();
                                        process_upload++;
                                        break;
                                }
                                //pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
            else if(state == 2)
            {
                //MessageBox.Show("VAR");
                /*frm_Updownload.process = 0;
                for (int i = 0; i < 9; i++)
                {
                    if (Classes.Vars.parts[i].ToString() != "0")
                    {
                        frm_Updownload.process++;
                    }                    
                }
                pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));*/
                /*for (int i = 0; i < 9; i++)
                {
                    MessageBox.Show((i + 1).ToString() + " Thread- " + Vars.parts[i].ToString());
                }*/
                for (int j = 0; j < 6; j++)
                {
                    if (Vars.parts[j].ToString() != "0")
                    {
                        try
                        {
                            switch (j)
                            {
                                case 0:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Read aloud exercises"; }));
                                    upload_read_aloud();
                                    process_upload++;
                                    break;
                                case 1:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Describing a picture exercise"; }));
                                    upload_describe_picture();
                                    process_upload++;
                                    break;
                                case 2:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Respond To Questions exercises"; }));
                                    upload_respond_questions();
                                    process_upload++;
                                    break;
                                case 3:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Respond To Questions with information provided exercises"; }));
                                    upload_respond_provided();
                                    process_upload++;
                                    break;
                                case 4:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Propose a solution exercise"; }));
                                    upload_propose_solution();
                                    process_upload++;
                                    break;
                                case 5:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Express an opinion exercise"; }));
                                    upload_express_opinion();
                                    process_upload++;
                                    break;
                            }

                            //pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }

                //Writting
                for (int j = 6; j < 9; j++)
                {
                    if (Classes.Vars.parts[j].ToString() != "0")
                    {
                        try
                        {
                            switch (j)
                            {
                                case 6:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Based on a picture exercises"; }));
                                    upload_based_picture();
                                    process_upload++;
                                    break;
                                case 7:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Wrtitten request exercises"; }));
                                    upload_written();
                                    process_upload++;
                                    break;
                                case 8:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Uploading Opinion essay exercise"; }));
                                    upload_opinion_essay();
                                    process_upload++;
                                    break;
                            }
                            //pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
            }

            //MessageBox.Show(process_upload.ToString());     
            upload_data();
            if(state == 1)
            {
                if ((frm_Updownload.process - 1) == process_upload)
                {
                    pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                    label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading Process Completed!"; }));
                    button.Invoke(new MethodInvoker(delegate { button.Visible = true; }));
                }
            }
            else if(state == 2)
            {
                if (frm_Updownload.process == process_upload)
                {
                    pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
                    label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading Process Completed!"; }));
                    button.Invoke(new MethodInvoker(delegate { button.Visible = true; }));
                }
            }
        }

        public void Download_files(ProgressBar pb, Label lbl_info, string db, int limit, int initial)
        {
            db_ = db;
            
            Form frm;
            if (Classes.Vars.Part_selection == 3)
            {
                //frm = new frm_create_readaloud(); Downlaod both parts
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.Step = 100 / 20; }));                
                //label = lbl_info;
                //progressbar = pb;
                Download_all_files(11, db, limit, initial);
                return;
            }
            else
            {
                if (Classes.Vars.Part_selection == 1)
                {
                    //frm = new frm_create_basedonpicture(); Speaking
                    progressbar.Invoke(new MethodInvoker(delegate { progressbar.Step = 7; }));
                    //label = lbl_info;
                    //progressbar = pb;
                    Download_all_files(8, db, limit, initial);
                    return;
                }
                else
                {
                    if (Classes.Vars.Part_selection == 2)
                    {
                        //frm = new frm_create_basedonpicture(); Writing
                        progressbar.Invoke(new MethodInvoker(delegate { progressbar.Step = 100 /9; }));
                        //label = lbl_info;
                        //progressbar = pb;
                        Download_all_files(5, db, limit, initial);
                        return;
                    }
                    else
                    {
                        try
                        {
                            bar_step(pb, lbl_info, "Connecting to host");
                            switch (Classes.Vars.Part_selection)
                            {
                                case 4:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Read aloud exercises"; }));
                                    download_read_aloud();
                                    bar_step(pb, lbl_info, "Downloading Process Completed!");
                                    break;
                                case 5:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Describing a picture exercise"; }));
                                    download_describe_picture();
                                    break;
                                case 6:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Respond To Questions exercises"; }));
                                    download_respond_questions();
                                    break;
                                case 7:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Respond To Questions with information provided exercises"; }));
                                    download_respond_provided();
                                    break;
                                case 8:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Propose a solution exercise"; }));
                                    download_propose_solution();
                                    break;
                                case 9:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Express an opinion exercise"; }));
                                    download_express_opinion();
                                    break;
                                case 10:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Based on a picture exercises"; }));
                                    download_based_picture();
                                    break;
                                case 11:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Wrtitten request exercises"; }));
                                    download_written();
                                    break;
                                case 12:
                                    lbl_info.Invoke(new MethodInvoker(delegate { lbl_info.Text = "Downloading Opinion essay exercise"; }));
                                    download_opinion_essay();
                                    break;
                            }
                            if ((frm_Updownload.process - 1) == process_upload)
                            {
                                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Process Completed!"; }));
                                button.Invoke(new MethodInvoker(delegate { button.Visible = true; }));
                            }
                        }
                        catch (Exception ex)
                        {
                            label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Process Failed!"; }));
                            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
                            button.Invoke(new MethodInvoker(delegate { button.Text = "Close"; button.Visible = true; }));
                        }
                    }
                }
            }                   
        }

        public void Download_all_files(int steps, string db, int limit, int initial)
        {
            db_ = db;
            try
            {
                bar_step(progressbar, label, "Connecting to host");
                int process = 1;
                for (int i = initial; i < limit; i++)
                {
                    if (Classes.Vars.parts[i] != 0)
                    {
                        switch (i)
                        {
                            case 0:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Read aloud exercises"; }));
                                download_read_aloud();
                                process++;
                                break;
                            case 1:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Describing a picture exercise"; }));
                                download_describe_picture();
                                process++;
                                break;
                            case 2:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Respond To Questions exercises"; }));
                                download_respond_questions();
                                process++;
                                break;
                            case 3:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Respond To Questions with information provided exercises"; }));
                                download_respond_provided();
                                process++;
                                break;
                            case 4:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Propose a solution exercise"; }));
                                download_propose_solution();
                                process++;
                                break;
                            case 5:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Express an opinion exercise"; }));
                                download_express_opinion();
                                process++;
                                break;
                            case 6:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Based on a picture exercises"; }));
                                download_based_picture();
                                process++;
                                break;
                            case 7:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Wrtitten request exercises"; }));
                                download_written();
                                process++;
                                break;
                            case 8:
                                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Opinion essay exercise"; }));
                                download_opinion_essay();
                                process++;
                                break;
                        }
                    }
                }
                if (Classes.Vars.State != 1)
                {
                    if ((process + 1) == steps)
                    {
                        //MessageBox.Show("Case 1");
                        bar_step(progressbar, label, "Downloading Process Completed!");
                        button.Invoke(new MethodInvoker(delegate { button.Visible = true; }));
                        //label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Process Completed!"; }));
                        //progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
                        //progressbar.Invoke(new MethodInvoker(delegate { progressbar.Step = 1; progressbar.Style = ProgressBarStyle.Marquee; }));
                        //button.Invoke(new MethodInvoker(delegate { button.Visible = true; }));
                        //label.Invoke(new MethodInvoker(delegate { label.Text = "Verifying files ... Please wait"; }));
                    }
                }
                else
                {
                    if (process == steps)
                    {
                        //MessageBox.Show("Case 2");
                        bar_step(progressbar, label, "Downloading Process Completed!");
                        button.Invoke(new MethodInvoker(delegate { button.Visible = true; }));
                        //label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Process Completed!"; }));
                        //progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
                        //progressbar.Invoke(new MethodInvoker(delegate { progressbar.Step = 1; progressbar.Style = ProgressBarStyle.Marquee; }));
                        //label.Invoke(new MethodInvoker(delegate { label.Text = "Verifying files ... Please wait"; }));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bar_step(ProgressBar pb, Label lbl, String str)
        {
            lbl.Invoke(new MethodInvoker(delegate { lbl.Text = str; }));
            pb.Invoke(new MethodInvoker(delegate { pb.PerformStep(); }));
        }

        //Events to upload and download files
        public void upload_data()
        {
            INode folder = mega_host.root("Data");
            label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading Database information"; }));
            //
            string file = Classes.Vars.ID + "_list.db";
            Path = @"C:\TryHard\Data\" + file;
            mega_host.delete_file(file);
            mega_host.upload(Path, folder);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        public void upload_read_aloud()
        {
            INode folder = mega_host.root("Read Aloud");
            for (int a = 1; a < 3; a++)
            {
                if (a == 1)
                {
                    label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading Read aloud exercise 1/2"; }));
                }
                else
                {
                    label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading Read aloud exercise 2/2"; }));
                }
                string file = Classes.Vars.ID + "_reading_" + a + "_" + Classes.Vars.Number_toeic + ".tryhard";
                Path = @"C:\TryHard\Files\ReadingAloud\" + file;
                mega_host.delete_file(file);
                mega_host.upload(Path, folder);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }            
        }

        private void download_read_aloud()
        {
            for (int a = 1; a < 3; a++)
            {
                if (a == 1)
                {
                    label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Read aloud exercise 1/2"; }));
                }
                else
                {
                    label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Read aloud exercise 2/2"; }));
                }
                Path = @"C:\TryHard\Files\ReadingAloud";
                CreateFolder();
                string file = db_ + "_reading_" + a + "_" + Classes.Vars.Number_toeic + ".tryhard";
                Path = @"C:\TryHard\Files\ReadingAloud\" + file;//Classes.Vars.Provider + "_reading_" + a + "_" + Classes.Vars.Number_toeic + ".tryhard";
                fun.delete_file(Path);
                if (mega_host.fileExists(file))
                {                    
                    mega_host.download(Path, file);
                    progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
                }   
            }
        }

        private bool verify_read_aloud()
        {
            //MessageBox.Show("Reading");
            bool exists = true;
            for (int a = 1; a < 3; a++)
            {   
                string file = db_ + "_reading_" + a + "_" + Classes.Vars.Number_toeic + ".tryhard";
                Path = @"C:\TryHard\Files\ReadingAloud\" + file;
                if(!file_exist(Path))
                {
                    exists = false;
                    goto done;
                }
            }
        done:
            return exists;
        }

        private void upload_describe_picture()
        {
            INode folder = mega_host.root("Describe a Picture");
            string file = Vars.ID + "_describing_" + Vars.Number_toeic + ".png";
            Path = @"C:\TryHard\Files\Describe\" + file;
            mega_host.delete_file(file);
            mega_host.upload(Path, folder);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        private void download_describe_picture()
        {
            Path = @"C:\TryHard\Files\Describe";
            CreateFolder();
            string file = db_ + "_describing_" + Classes.Vars.Number_toeic + ".png";
            Path = @"C:\TryHard\Files\Describe\" + file;
            fun.delete_file(Path);
            mega_host.download(Path, file);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        private bool verify_describe_picture()
        {
            string file = db_ + "_describing_" + Classes.Vars.Number_toeic + ".png";
            Path = @"C:\TryHard\Files\Describe\" + file;
            return (!file_exist(Path)) ? false : true;
        }

        private void upload_respond_questions()
        {
            INode folder = mega_host.root("Respond To Question");
            for (int i = 0; i < 4; i++)
            {
                switch(i)
                {
                    case 0:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading respond To Questions exercise 1/3"; }));
                        break;
                   case 1:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading respond To Questions exercise 2/3"; }));
                        break;
                    case 2:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading respond To Questions exercise 3/3"; }));
                        break;
                }
                string file = Vars.ID + "_respond_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                Path = @"C:\TryHard\Files\Respond\" + file;
                mega_host.delete_file(file);
                mega_host.upload(Path, folder);
                Path = Path.Replace(".mp3", ".tryhard");
                file = file.Replace(".mp3", ".tryhard");
                mega_host.delete_file(file);
                mega_host.upload(Path, folder);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }            
        }

        private void download_respond_questions()
        {
            Path = @"C:\TryHard\Files\Respond";
            CreateFolder();
            for (int i = 0; i < 4; i++)
            {
                switch (i)
                {
                    case 0:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading respond To Questions exercise 1/3"; }));
                        break;
                    case 1:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading respond To Questions exercise 2/3"; }));
                        break;
                    case 2:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading respond To Questions exercise 3/3"; }));
                        break;
                }
                string file = db_ + "_respond_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                Path = @"C:\TryHard\Files\Respond\" + file;
                fun.delete_file(Path);
                mega_host.download(Path, file);
                Path = Path.Replace(".mp3", ".tryhard");
                file = file.Replace(".mp3", ".tryhard");
                fun.delete_file(Path);
                mega_host.download(Path, file);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }
        }

        private bool verify_respond_questions()
        {
            bool exists = true;
            for (int i = 0; i < 4; i++)
            {
                string file = db_ + "_respond_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                Path = @"C:\TryHard\Files\Respond\" + file;
                if (!file_exist(Path)) { exists = false; break; }
                Path = Path.Replace(".mp3", ".tryhard");
                if (!file_exist(Path)) { exists = false; break; }
            }
            return exists;
        }

        private void upload_respond_provided()
        {
            INode folder = mega_host.root("Respond Information Provided");
            for (int i = 0; i < 3; i++)
            {
                switch (i)
                {
                    case 0:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading respond To Questions... exercise 1/3"; }));
                        break;
                    case 1:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading respond To Questions... exercise 2/3"; }));
                        break;
                    case 2:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading respond To Questions... exercise 3/3"; }));
                        break;
                }
                string file = Classes.Vars.ID + "_respond_provided_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                Path = @"C:\TryHard\Files\Respond_Provided\" + file;
                mega_host.delete_file(file);
                mega_host.upload(Path, folder);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }
            label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading respond To Questions... information provided"; }));
            string file2 = Classes.Vars.ID + "_info_provided_" + Classes.Vars.Number_toeic + ".png";
            Path = @"C:\TryHard\Files\Respond_Provided\" + file2;
            if (File.Exists(Path))
            {
                string auxiliar;
                mega_host.delete_file(file2);
                auxiliar = file2.Replace(".png", ".tryhard");
                mega_host.delete_file(auxiliar);
                mega_host.upload(Path, folder);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }
            else
            {
                string auxiliar;
                Path = Path.Replace(".png", ".tryhard");
                file2 = file2.Replace(".png", ".tryhard");                                
                if (File.Exists(Path))
                {
                    auxiliar = file2.Replace(".tryhard", ".png");
                    mega_host.delete_file(auxiliar);
                    mega_host.delete_file(file2);
                    mega_host.upload(Path, folder);
                    progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
                }
                else
                    MessageBox.Show("File not found");
            }
        }

        private void download_respond_provided()
        {
            Path = @"C:\TryHard\Files\Respond_Provided";
            CreateFolder();
            for (int i = 0; i < 3; i++)
            {
                switch (i)
                {
                    case 0:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading respond To Questions... exercise 1/3"; }));
                        break;
                    case 1:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading respond To Questions... exercise 2/3"; }));
                        break;
                    case 2:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading respond To Questions... exercise 3/3"; }));
                        break;
                }
                string file = db_ + "_respond_provided_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                Path = @"C:\TryHard\Files\Respond_Provided\" + file;
                fun.delete_file(Path);
                mega_host.download(Path, file);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }
            label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading respond To Questions... information provided"; }));
            string file2 = db_ + "_info_provided_" + Classes.Vars.Number_toeic + ".png";
            Path = @"C:\TryHard\Files\Respond_Provided\" + file2;
            if (mega_host.fileExists(file2))
            {
                fun.delete_file(Path);
                mega_host.download(Path, file2);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }
            else
            {
                Path = Path.Replace(".png", ".tryhard");
                fun.delete_file(Path);
                file2 = file2.Replace(".png", ".tryhard");
                if (mega_host.fileExists(file2))
                {
                    mega_host.download(Path, file2);
                    progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
                }                
            }
        }

        private bool verify_respond_provided()
        {
            bool exists = true;
            for (int i = 0; i < 3; i++)
            {               
                string file = db_ + "_respond_provided_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                Path = @"C:\TryHard\Files\Respond_Provided\" + file;
                if (!file_exist(Path)) { exists = false; goto done; }
            }            
            string file2 = db_ + "_info_provided_" + Classes.Vars.Number_toeic + ".png";
            Path = @"C:\TryHard\Files\Respond_Provided\" + file2;
            if (!file_exist(Path))
            {
                Path = Path.Replace(".png", ".tryhard");
                if (!file_exist(Path))
                {
                    exists = false;
                }
            }            
            done:
            return exists;
        }

        //
        private void upload_express_opinion()
        {
            INode folder = mega_host.root("Express an Opinion");
            string file = Classes.Vars.ID + "_express_" + Classes.Vars.Number_toeic + ".mp3";
            Path = @"C:\TryHard\Files\Express\" + file;
            mega_host.delete_file(file);
            mega_host.upload(Path, folder);            
            Path = Path.Replace(".mp3", ".tryhard");
            file = file.Replace(".mp3", ".tryhard");
            mega_host.delete_file(file);
            mega_host.upload(Path, folder);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        private void download_express_opinion()
        {
            Path = @"C:\TryHard\Files\Express";
            CreateFolder();
            string file = db_ + "_express_" + Classes.Vars.Number_toeic + ".mp3";
            Path = @"C:\TryHard\Files\Express\" + file;
            fun.delete_file(Path);
            mega_host.download(Path, file);
            Path = Path.Replace(".mp3", ".tryhard");
            fun.delete_file(Path);
            file = file.Replace(".mp3", ".tryhard");
            mega_host.download(Path, file);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        private bool verify_express_opinion()
        {
            bool exists = true;
            string file = db_ + "_express_" + Classes.Vars.Number_toeic + ".mp3";
            Path = @"C:\TryHard\Files\Express\" + file;
            if (!file_exist(Path)) { exists = false; goto done; }
            Path = Path.Replace(".mp3", ".tryhard");
            if (!file_exist(Path)) { exists = false;}
        done:
            return exists;
        }

        private void upload_propose_solution()
        {
            INode folder = mega_host.root("Propose a Solution");
            string file = Classes.Vars.ID + "_propose_" + Classes.Vars.Number_toeic + ".mp3";
            Path = @"C:\TryHard\Files\Propose\" + file;
            mega_host.delete_file(file);
            mega_host.upload(Path, folder);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        private void download_propose_solution()
        {
            Path = @"C:\TryHard\Files\Propose";
            CreateFolder();
            string file = db_ + "_propose_" + Classes.Vars.Number_toeic + ".mp3";
            Path = @"C:\TryHard\Files\Propose\" + file;
            fun.delete_file(Path);
            mega_host.download(Path, file);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        private bool verify_propose_solution()
        {            
            string file = db_ + "_propose_" + Classes.Vars.Number_toeic + ".mp3";
            Path = @"C:\TryHard\Files\Propose\" + file;
            return (!file_exist(Path)) ? false : true;
        }

        //Reading
        private void upload_based_picture()
        {
            INode folder = mega_host.root("Based on a Picture");
            string auxiliar = Classes.Vars.ID + "_based_words_" + Classes.Vars.Number_toeic + ".tryhard";
            Path = @"C:\TryHard\Files\Based\" + auxiliar;
            mega_host.delete_file(auxiliar);
            mega_host.upload(Path, folder);
            for (int i = 1; i <= 5; i++)
            {
                switch (i)
                {
                    case 1:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading based on a picture exercise 1/5"; }));
                        break;
                    case 2:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading based on a picture exercise 2/5"; }));
                        break;
                    case 3:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading based on a picture exercise 3/5"; }));
                        break;
                    case 4:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading based on a picture exercise 4/5"; }));
                        break;
                    case 5:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Uploading based on a picture exercise 5/5"; }));
                        break;
                }
                string file = Classes.Vars.ID + "_based_words_picture_" + i.ToString() + "_" + Classes.Vars.Number_toeic + ".png";
                Path = @"C:\TryHard\Files\Based\" + file;
                mega_host.delete_file(file);
                mega_host.upload(Path, folder);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }            
        }

        private void download_based_picture()
        {
            Path = @"C:\TryHard\Files\Based";
            CreateFolder();
            string file = db_ + "_based_words_" + Classes.Vars.Number_toeic + ".tryhard";
            Path = @"C:\TryHard\Files\Based\" + file;
            fun.delete_file(Path);
            mega_host.download(Path, file);
            for (int i = 1; i <= 5; i++)
            {
                switch (i)
                {
                    case 1:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading based on a picture exercise 1/5"; }));
                        break;
                    case 2:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading based on a picture exercise 2/5"; }));
                        break;
                    case 3:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading based on a picture exercise 3/5"; }));
                        break;
                    case 4:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading based on a picture exercise 4/5"; }));
                        break;
                    case 5:
                        label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading based on a picture exercise 5/5"; }));
                        break;
                }
                file = db_ + "_based_words_picture_" + i.ToString() + "_" + Classes.Vars.Number_toeic + ".png";
                Path = @"C:\TryHard\Files\Based\" + file;
                fun.delete_file(Path);
                mega_host.download(Path, file);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }
        }

        private bool verify_based_picture()
        {
            bool exists = true;
            string file = db_ + "_based_words_" + Classes.Vars.Number_toeic + ".tryhard";
            Path = @"C:\TryHard\Files\Based\" + file;
            if (!file_exist(Path)) { exists = false; goto done; }
            for (int i = 1; i <= 5; i++)
            {  
                file = db_ + "_based_words_picture_" + i.ToString() + "_" + Classes.Vars.Number_toeic + ".png";
                Path = @"C:\TryHard\Files\Based\" + file;
                if (!file_exist(Path)) { exists = false; goto done; }
            }
        done:
            return exists;
        }

        private void upload_written()
        {
            INode folder = mega_host.root("Written Request");
            string auxiliar;
            for (int i = 0; i < 2; i++)
            {
                string file = Classes.Vars.ID + "_direction_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".tryhard";
                Path = @"C:\TryHard\Files\Written\" + file;
                mega_host.delete_file(file);
                mega_host.upload(Path, folder);
                auxiliar = Path.Replace("direction", "email");
                file = file.Replace("direction", "email");
                Path = auxiliar;
                mega_host.delete_file(file);
                mega_host.upload(Path, folder);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }            
        }

        private void download_written()
        {
            Path = @"C:\TryHard\Files\Written";
            CreateFolder();
            string auxiliar;
            for (int i = 0; i < 2; i++)
            {
                string file = db_ + "_direction_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".tryhard";
                Path = @"C:\TryHard\Files\Written\" + file;                
                fun.delete_file(Path);
                label.Invoke(new MethodInvoker(delegate { label.Text = "Downloading Wrtitten request exercise " + (i+1).ToString()+ "/2"; }));
                mega_host.download(Path, file);
                auxiliar = Path.Replace("direction", "email");
                file = file.Replace("direction", "email");
                Path = auxiliar;                
                fun.delete_file(Path);
                mega_host.download(Path, file);
                progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
            }
        }

        private bool verify_written()
        {
            bool exists = true;
            for (int i = 0; i < 2; i++)
            {
                string file = db_ + "_direction_" + (i + 1).ToString() + "_" + Vars.Number_toeic + ".tryhard";                
                Path = @"C:\TryHard\Files\Written\" + file;
                //MessageBox.Show(Path);
                if (!file_exist(Path)) { exists = false; goto done; }
                Path = Path.Replace("direction", "email");
                if (!file_exist(Path)) { exists = false; goto done; }
            }
        done:
            return exists;
        }

        private void upload_opinion_essay()
        {
            INode folder = mega_host.root("Opinion Essay");
            string file = Classes.Vars.ID + "_statement_" + Classes.Vars.Number_toeic + ".tryhard";
            Path = @"C:\TryHard\Files\Opinion\" + file;
            mega_host.delete_file(file);
            mega_host.upload(Path, folder);
            path = path.Replace("statement", "directions");
            file = file.Replace("statement", "directions");
            mega_host.delete_file(file);
            mega_host.upload(Path, folder);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }        

        private void download_opinion_essay()
        {
            Path = @"C:\TryHard\Files\Opinion";
            CreateFolder();
            string file = db_ + "_statement_" + Classes.Vars.Number_toeic + ".tryhard";
            Path = @"C:\TryHard\Files\Opinion\" + file;
            fun.delete_file(Path);
            mega_host.download(Path, file);
            path = path.Replace("statement", "directions");
            file = file.Replace("statement", "directions");
            fun.delete_file(Path);
            mega_host.download(Path, file);
            progressbar.Invoke(new MethodInvoker(delegate { progressbar.PerformStep(); }));
        }

        private bool verify_opinion_essay()
        {
            bool exists = true;
            string file = db_ + "_statement_" + Classes.Vars.Number_toeic + ".tryhard";
            Path = @"C:\TryHard\Files\Opinion\" + file;
            if (!file_exist(Path)) { exists = false; goto done; }
            Path = Path.Replace("statement", "directions");
            if (!file_exist(Path)) { exists = false;}
        done:
            return exists;
        }

        public bool verify_files_exist(string db)
        {
            db_ = db;
            bool exists = true;
            for (int i = 0; i < 9; i++)
            {
                //MessageBox.Show((i + 1).ToString() + " - " + Vars.parts[i].ToString());
                //MessageBox.Show(i.ToString() + " - " + Classes.Vars.parts[i].ToString());
                if (Classes.Vars.parts[i] != 0)
                {
                    //MessageBox.Show("Inside");
                    switch (i)
                    {
                        case 0: if (!verify_read_aloud()) { exists = false; goto done; }
                            break;
                        case 1: if (!verify_describe_picture()) { exists = false; goto done; }
                            break;
                        case 2: if (!verify_respond_questions()) { exists = false; goto done; }
                            break;
                        case 3: if (!verify_respond_provided()) { exists = false; goto done; }
                            break;                        
                        case 4: if (!verify_propose_solution()) { exists = false; goto done; }
                            break;
                        case 5: if (!verify_express_opinion()) { exists = false; goto done; }
                            break;
                        case 6: if (!verify_based_picture()) { exists = false; goto done; }
                            break;
                        case 7: if (!verify_written()) { exists = false; goto done; }
                            break;
                        case 8: if (!verify_opinion_essay()) { exists = false; goto done; }
                            break;
                    }
                }
            }
        done:
            //for (int i = 0; i < 9; i++)
            //{
              //  Vars.parts[i] = 0;
            //}
            return exists;
        }
    }
}

//this.Load += new System.EventHandler(this.frm_Updownload_Load);
