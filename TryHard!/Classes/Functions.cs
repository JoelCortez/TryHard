﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CG.Web.MegaApiClient;
using System.Media;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TryHard_.Classes
{
    class Functions
    {
        protected static string path;

        public static string Path
        {
            set
            {
                path = value;
            }
            get
            {
                return path;
            }
        }

        //Create Folder

        public void CreateFolder()
        {
            if (!Directory.Exists(Path))
            {
                DirectoryInfo df = Directory.CreateDirectory(Path);
            }
        }

        public static bool file_exist(string path)
        {
            if(File.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Delete file
        public void delete_file(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        //Create TXT        
        public void CreateTxt(string[] lines)
        {
            File.WriteAllLines(Path, lines);
        }
        //Disable Controls
        public void DisableControls(System.Windows.Forms.Control control, int state)
        {
            if (state == 1)
            {
                control.Enabled = true;
            }
            else if(state==2) 
            {
                control.Enabled = false;
            }
        }

        //Open Form
        public void Open(System.Windows.Forms.Form frm)
        {
            System.Windows.Forms.Form form_open = new System.Windows.Forms.Form();
            form_open = frm;
            form_open.Show();
        }

        public static void play_sound(string path)
        {
            SoundPlayer player = new SoundPlayer();
            player.SoundLocation = path;
            player.Load();
            player.Play();
        }

        //change form in frm_background
        public static void change_form(Form frm, string frm_string)
        {
            //MessageBox.Show(frm_string);
            Vars.panel_container.Controls.Remove(Vars.panel_container.Controls[frm_string]);
            Vars.panel_container.Controls.Add(frm);
            frm.Show();
        }

    }

    public class Mega
    {
        public MegaApiClient mega = new MegaApiClient();
        public IEnumerable<INode> nodos;

        public Mega()
        {
            mega.Login("joelalexander10@outlook.com","teamohermosa");
        }
        
        public INode root(string carpeta)
        {
            nodos = mega.GetNodes();
            INode folder = nodos.Single(n => n.Name == carpeta);
            return folder;
        }

        public void upload(string filename, INode parent)
        {
            mega.UploadFile(filename, parent);
        }
        public bool fileExists(string name)
        {            
            nodos = mega.GetNodes();
            bool existe = mega.GetNodes().Any(n => n.Name == name);
            if (existe == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void download(string path, string filename)
        {
            /*if(fileExists(filename))
            {
                nodos = mega.GetNodes();
                INode file = nodos.Single(n => n.Name == filename);
                //INode file = nodos.Any(n => n.Name == filename);
                mega.DownloadFile(file, path);
            }
            else
            {
                MessageBox.Show("Not Found!");
            }*/
            nodos = mega.GetNodes();
            INode file = nodos.Single(n => n.Name == filename);
            //INode file = nodos.Any(n => n.Name == filename);
            mega.DownloadFile(file, path);
        }
        public void delete_file(string file)
        {            
            if (fileExists(file))
            {
                INode file_delete = root(file);
                mega.Delete(file_delete, false);
            }
        }
        public void play_stream(string file)
        {
            INode file_ = root(file);
            SoundPlayer snd = new SoundPlayer(file);
            //snd.Stream = mega.Download(file_);
            snd.Play();
        }
    }

    public class Record_audio
    {
        [DllImport("winmm.dll")]
        private static extern long mciSendString(string strCommand, string strReturn, int iReturnLength, int handCallback);

        public void record()
        {
            int bits = 16;
            int canales = 2;
            int muestras = 44100;
            int promedio = bits * canales * muestras / 8;
            int alineacion = bits * canales / 8;
            string comando;

            try
            {
                comando = "set capture bitspersample " + bits + " channels " + canales + " alignment " + alineacion + " samplespersec " +
                    muestras + " bytespersec " + promedio + " format tag pcm wait";
                mciSendString("close capture", "", 0, 0);
                mciSendString("open new type waveaudio alias capture", "", 0, 0);
                mciSendString(comando, "", 0, 0);
                mciSendString("record capture", "", 0, 0);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                    
            }
        }

        public void save_recording()
        {
            try
            {
                string path = @"C:\TryHard\buffer";
                if (!Directory.Exists(path))
                {
                    DirectoryInfo df = Directory.CreateDirectory(path);
                }
                string archivo = Functions.Path;
                //string archivo = path + @"\test_microphone"
                mciSendString("stop capture", "", 0, 0);
                mciSendString("save capture " + archivo, "", 0, 0);
                mciSendString("close capture", "", 0, 0);
            }
            catch (Exception sx)
            {
                MessageBox.Show(sx.Message);
            }
        }
    }
}
