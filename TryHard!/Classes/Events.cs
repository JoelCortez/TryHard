﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TryHard_.Classes
{
    class Events : Vars
    {
        Form frm;
        OpenFileDialog ofl = new OpenFileDialog();
        public void Add_event(Control control)
        {
            ContextMenu ctm = new ContextMenu();
            switch (Number_Picture)
            {
                case 0:
                    ctm.MenuItems.Add("Change Words", new EventHandler(change_words_img_1));
                    break;
                case 1:
                    ctm.MenuItems.Add("Change Words", new EventHandler(change_words_img_2));
                    break;
                case 2:
                    ctm.MenuItems.Add("Change Words", new EventHandler(change_words_img_3));
                    break;
                case 3:
                    ctm.MenuItems.Add("Change Words", new EventHandler(change_words_img_4));
                    break;
                case 4:
                    ctm.MenuItems.Add("Change Words", new EventHandler(change_words_img_5));
                    break;
            }
            control.Controls["btn_img_" + (Number_Picture + 1).ToString()].ContextMenu = ctm;
        }

        protected void change_words_img_1(object sender, EventArgs e)
        {
            frm = new frm_addtext_basedonapicture();
            frm_addtext_basedonapicture.position_word = 1;
            frm.ShowDialog();
        }

        protected void change_words_img_2(object sender, EventArgs e)
        {
            frm = new frm_addtext_basedonapicture();
            frm_addtext_basedonapicture.position_word = 2;
            frm.ShowDialog();
        }

        protected void change_words_img_3(object sender, EventArgs e)
        {
            frm = new frm_addtext_basedonapicture();
            frm_addtext_basedonapicture.position_word = 3;
            frm.ShowDialog();
        }

        protected void change_words_img_4(object sender, EventArgs e)
        {
            frm = new frm_addtext_basedonapicture();
            frm_addtext_basedonapicture.position_word = 4;
            frm.ShowDialog();
        }

        protected void change_words_img_5(object sender, EventArgs e)
        {
            frm = new frm_addtext_basedonapicture();
            frm_addtext_basedonapicture.position_word = 5;
            frm.ShowDialog();
        }

        protected void change_image_img_1(object sender, EventArgs e)
        {            
        }
        protected void change_image_img_2(object sender, EventArgs e)
        {
        }
        protected void change_image_img_3(object sender, EventArgs e)
        {
        }
        protected void change_image_img_4(object sender, EventArgs e)
        {
        }
        protected void change_image_img_5(object sender, EventArgs e)
        {
        }


        public void open_dialog(int filename, Control ctrl)
        {
            if(frm_create_basedonpicture.ofl_state == 1)
            {
                if (ofl.ShowDialog() == DialogResult.OK)
                {
                    frm_create_basedonpicture.filenames[filename] = ofl.FileName.ToString();
                    ctrl.Controls["btn_img_" + (filename + 1).ToString()].BackgroundImage.Dispose();
                    ctrl.Controls["btn_img_" + (filename + 1).ToString()].BackgroundImage = Image.FromFile(frm_create_basedonpicture.filenames[filename]);
                    Classes.Vars.Number_Picture = filename;
                    Add_event(ctrl);
                    Form form = new frm_addtext_basedonapicture();
                    frm_addtext_basedonapicture.position_word = filename + 1;
                    form.ShowDialog();
                }
            }
        }
    }

    static class MainEvents
    {
        public static void minimizar(Form frm)
        {
            frm.WindowState = FormWindowState.Minimized;
        }

    }
}
