﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Finisar.SQLite;
using System.Data;
using System.Windows.Forms;

namespace TryHard_.Classes
{
    class SQLite : Functions
    {
        public SQLiteConnection conn;
        protected SQLiteCommand command;
        public DataTable table = new DataTable();        
        public void create_db_toeic()
        {
            if (!File.Exists(@"C:\TryHard\Data\" + Classes.Vars.ID + "_list.db"))
            {
                conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + Classes.Vars.ID + "_list.db;Version=3;New=True;Compress=True;");
                conn.Open();
                command = conn.CreateCommand();
                command.CommandText = "create table list_toeic(number int(11), primary key(number));";
                command.ExecuteNonQuery();
                command.CommandText = "create table toeic_parts(id_number int(11),reading tinyint(1),describe tinyint(1),respond tinyint(1),provided tinyint(1),propose tinyint(1),opinion tinyint(1),based tinyint(1),written tinyint(1),essay tinyint(1), foreign key(id_number) references list_toeic(number));";
                command.ExecuteNonQuery();                
                conn.Close();
            }
        }
                
        public void update_parts(string part, int state)
        {
            conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + Classes.Vars.ID + "_list.db;Version=3;New=False;Compress=True;");
            conn.Open();
            command = conn.CreateCommand();
            command.CommandText = "update toeic_parts set " + part + "=" + state + " where id_number=" + Classes.Vars.Number_toeic + ";";
            command.ExecuteNonQuery();
            conn.Close();
        }
               
        public DataTable check_number_exists()
        {
            DataTable tabla_existe = new DataTable();
            SQLiteDataAdapter da;
            conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + Classes.Vars.ID + "_list.db;Version=3;New=False;Compress=True;");
            conn.Open();
            string query = "select number from list_toeic order by number desc;";
            da = new SQLiteDataAdapter(query, conn);
            da.Fill(tabla_existe);
            conn.Close();
            return tabla_existe;
        }
        public DataTable show_list_toeics(string db)
        {
            DataTable tabla_existe = new DataTable();
            SQLiteDataAdapter da;
            conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + db + "_list.db;Version=3;New=False;Compress=True;");
            conn.Open();
            string query = "select number from list_toeic;";
            da = new SQLiteDataAdapter(query, conn);
            da.Fill(tabla_existe);
            conn.Close();
            return tabla_existe;
        }
        public DataTable show_datas_speaking(string db)
        {
            DataTable tabla_existe = new DataTable();
            SQLiteDataAdapter da;
            conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + db + "_list.db;Version=3;New=False;Compress=True;");
            conn.Open();
            string query = "select reading,describe,respond,provided,propose,opinion from toeic_parts where id_number=" + Classes.Vars.Number_toeic + ";";
            da = new SQLiteDataAdapter(query, conn);
            da.Fill(tabla_existe);
            conn.Close();
            return tabla_existe;
        }
        public DataTable show_datas_writing(string db)
        {
            DataTable tabla_existe = new DataTable();
            SQLiteDataAdapter da;
            conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + db + "_list.db;Version=3;New=False;Compress=True;");
            conn.Open();
            string query = "select based, written, essay from toeic_parts where id_number=" + Classes.Vars.Number_toeic + ";";
            da = new SQLiteDataAdapter(query, conn);
            da.Fill(tabla_existe);
            conn.Close();
            return tabla_existe;
        }
        public void insert_number_toeic()
        {
            int number;
            DataTable existe_numero = new DataTable();
            existe_numero = check_number_exists();
            try
            {
                number = int.Parse(existe_numero.Rows[0][0].ToString()) + 1;
            }
            catch (Exception)
            {
                number = 1;
            }
            conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + Classes.Vars.ID + "_list.db;Version=3;New=False;Compress=True;");
            conn.Open();
            command = conn.CreateCommand();
            command.CommandText = "insert into list_toeic(number)values(" + number + ");";
            command.ExecuteNonQuery();
            command.CommandText = "insert into toeic_parts(id_number,reading,describe,respond,provided,propose,opinion,based,written,essay) values(" + number + ",0,0,0,0,0,0,0,0,0);";
            command.ExecuteNonQuery();
            conn.Close();
        }
        public bool exercise_state(string campo)
        {
            DataTable tabla_existe = new DataTable();
            SQLiteDataAdapter da;
            conn = new SQLiteConnection(@"Data Source = C:\TryHard\Data\" + Classes.Vars.ID + "_list.db;Version=3;New=False;Compress=True;");
            conn.Open();
            string query = "select " + campo + " from toeic_parts where id_number=" + Classes.Vars.Number_toeic + ";";
            da = new SQLiteDataAdapter(query, conn);
            da.Fill(tabla_existe);
            conn.Close();
            if (tabla_existe.Rows[0][0].ToString() != "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    
    }

   
}
