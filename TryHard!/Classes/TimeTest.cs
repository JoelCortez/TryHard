﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TryHard_.Classes
{
    class TimeTest
    {
        public static Label lbl;
        public static Button button, button2;
        public static double duration = 0;
        //private Timer tmr;
        private System.Windows.Forms.Timer tmr;
        private int limit, count = 0;
        private int seconds = 0, minutes = 0;
        private Record_audio record_audio = new Classes.Record_audio();

        public TimeTest()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            button = new Button();
            button2 = new Button();
        }

        public void test_microphone(Label label, Button btn, Button btn_2, int time_out, string starting)
        {
            count = 0;
            lbl = label;
            button = btn;
            button2 = btn_2;
            limit = time_out;
            seconds = limit;
            lbl.Text = "00:" + starting;
            tmr.Tick += new EventHandler(timer_text);
            record_audio.record();
            Functions.Path = @"C:\TryHard\buffer\test_microphone.mp3";
            tmr.Start();
        }

        private void timer_text(object Sender, EventArgs e)
        {
            if (count != limit)
            {
                string text_timer = "";
                seconds--;
                if (seconds == 0)
                {
                    //minutes--;
                    seconds = 0;
                    text_timer = "0" + minutes + ":00";
                }
                else
                {
                    if (seconds < 10)
                    {
                        text_timer = "0" + minutes + ":0" + seconds;
                    }
                    else if (seconds > 9 && seconds < 60)
                    {
                        text_timer = "0" + minutes + ":" + seconds;
                    }
                }
                lbl.Text = text_timer;
                count++;
            }
            else
            {
                tmr.Stop();
                //MessageBox.Show("Good");
                record_audio.save_recording();
                button.Visible = true;
                button2.Visible = true;
            }
        }


        /*
        private void timer_text(object Sender, EventArgs e)        
        {
            if(count != limit)
            {
                string text_timer = "";
                seconds++;
                if (seconds == 60)
                {
                    minutes++;
                    seconds = 0;
                    text_timer = "0" + minutes + ":00";
                }
                else
                {
                    if (seconds < 10)
                    {
                        text_timer = "0" + minutes + ":0" + seconds;
                    }
                    else if (seconds > 9 && seconds < 60)
                    {
                        text_timer = "0" + minutes + ":" + seconds;
                    }
                }
                lbl.Text = text_timer;
                count++;
            }
            else
            {
                record_audio.save_recording();
                tmr.Stop();
            }
        }
        */
        
        public void preparation_direction_time()
        {
            tmr.Tick += new EventHandler(preparation_direction_time_controller);
            tmr.Start();            
        }

        private void preparation_direction_time_controller(object Sender, EventArgs e)
        {
            if (!Classes.Vars.Performing)
            {
                tmr.Stop();
                Performance.wmp.URL = "";
                Performance.wmp.controls.stop();
                return;
            }
            count++;
            if (count == (Math.Round(duration) + 3))
            {
                tmr.Stop();
                //MessageBox.Show("Done3!");
                change_exercise();
            }
            //MessageBox.Show(count + " - " + duration.ToString());
        }

        private void change_exercise()
        {
            string last_opened = "";
            Form frm = new Form();
            switch(Classes.Vars.Part_selection)
            {
                case 4:                    
                    frm = new frm_readaloud();
                    last_opened = "frm_readaloud";
                    break;
                case 5:                    
                    frm = new frm_describepicture();
                    last_opened = "frm_describepicture";
                    break;
                case 6:                    
                    frm = new frm_respondquestions();
                    last_opened = "frm_describepicture";
                    break;
                case 7:                    
                    frm = new frm_infoprovided();
                    last_opened = "frm_respondquestions";
                    break;
                case 8:                    
                    frm = new frm_proposesolution();
                    last_opened = "frm_proposesolution";
                    break;
                case 9:
                    frm = new frm_expressopinion();
                    last_opened = "frm_expressopinion";
                    break;
                case 10:                    
                    frm = new frm_basedpicture();
                    last_opened = "frm_basedpicture";
                    break;
                case 11:
                    frm = new frm_writtenrequest();
                    last_opened = "frm_writtenrequest";
                    break;
                case 12:
                    frm = new frm_opinionessay();
                    last_opened = "frm_opinionessay";
                    break;
            }
            frm.TopLevel = false;
            Classes.Functions.change_form(frm, Vars.Last_opened);
            Vars.Last_opened = last_opened;
        }

        private bool stop_performing()
        {
            bool value = true;
            if (!Vars.Performing)
            {
                Performance.wmp.URL = "";
                tmr.Stop();
                duration = 0;
            }
            return value;
        }
    }
}
