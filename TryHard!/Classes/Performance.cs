﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using WMPLib;
using System.IO;
using System.Drawing;

namespace TryHard_.Classes
{
    class Performance
    {
        public int position = 1, repeating = 0, question = 1;
        TimeTest timer;
        Record_audio record = new Record_audio();
        System.Windows.Forms.Timer tmr;
        private int seconds = 0, minutes = 0, preparation = 0, respond = 0, duration_time;
        public bool performing = false;
        public static WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        private int count = 0;

        private string[] words = new string[10], sentences = new string[5];

        public Performance()
        {
            //wmp
            wmp.settings.volume = 100;
        }

        private void set_part()
        {
            if(Vars.Performing)
            {
                switch (Classes.Vars.Part_selection)
                {
                    case 4:
                        reading(position);
                        break;
                    case 5:
                        describing();
                        break;
                    case 6:
                        question_time();
                        break;
                    case 7:
                        question_time_provided();
                        break;
                    case 8:
                        propose();
                        break;
                    case 9:
                        express_opinion();
                        break;
                    case 10:
                        save_sentences();
                        break;
                    case 11:
                        string path = @"C:\TryHard\Files\Written\" + Vars.ID + "_email_" + (position - 1) + "_" + Vars.Number_toeic + ".tryhard";
                        File.WriteAllText(path, Vars.txt_response.Text);
                        Vars.txt_response.Text = "";
                        written_request();
                        break;
                    case 12:
                        Vars.txt_response.Enabled = false;
                        path = @"C:\TryHard\Files\Opinion\" + Vars.ID + "_opinion_" + Vars.Number_toeic + ".tryhard";
                        File.WriteAllText(path, Vars.txt_response.Text);
                        //MessageBox.Show("Well done");
                        break;
                }
            }
        }

        private void reading_aloud(int state, Label label, Button btn, int time_out, string starting)
        {
            //timer = new TimeTest();
        }

        private void reading(int position)
        {
            repeating++;
            Vars.label_timer.Text = "00:45";
            Vars.lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\ReadingAloud\" + Classes.Vars.Provider + "_reading_" + position + "_" + Classes.Vars.Number_toeic + ".tryhard");
            Functions.Path = @"C:\TryHard\Files\ReadingAloud\" + Vars.ID + "_reading_" + position.ToString() + "_" + Vars.Number_toeic + ".mp3";
            if(repeating == 2)
            {
                preparation_time(45);
            }
            else
            {
                preparation = 0;
                respond_time(45);
                //record.record();
            }
        }

        private void describing()
        {            
            Vars.label_timer.Text = "00:45";
            preparation = 0;
            respond_time(45);
            Classes.Functions.Path = @"C:\TryHard\Files\Describe\" + Vars.ID + "_describing_" + Vars.Number_toeic + ".mp3";
            //record.record();
        }

        private void propose()
        {
            //Vars.label_timer.Text = "00:59";
            preparation = 0;
            respond_time(60);
            Classes.Functions.Path = @"C:\TryHard\Files\Propose\" + Vars.ID + "_propose_" + Classes.Vars.Number_toeic + ".mp3";
        }

        public void propose_problem()
        {
            //Vars.panel_timer_controller.Visible = true;
            //Vars.label_timer.Text = "00:30";
            count = 0;
            wmp.URL = @"C:\TryHard\Files\Propose\" + Vars.Provider + "_propose_" + Classes.Vars.Number_toeic + ".mp3";            
            wmp.controls.play();
            //MessageBox.Show(wmp.URL.ToString());
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(propose_problem_controller);
            tmr.Start();
        }

        private void propose_problem_controller(object Sender, EventArgs e)
        {
            stop_performing();
            TimeTest.duration = wmp.currentMedia.duration;
            //MessageBox.Show(count.ToString() + " - " + TimeTest.duration.ToString());
            if (count == (Math.Round(TimeTest.duration) + 2))
            //if (count == 2)
            {
                tmr.Stop();
                count = 0;
                //prep
                //MessageBox.Show("indication: " + position.ToString());
                //set_part();
                //performing = true;
                //MessageBox.Show("Good");
                //preparation = 3;                
                Vars.panel_timer_controller.Visible = true;                
                preparation_time(30);
                duration_time = 31;
                Vars.label_timer.Text = "00:30";
            }
            count++;
        }

        private void express_opinion()
        {
            preparation = 0;
            respond_time(60);
            Classes.Functions.Path = @"C:\TryHard\Files\Express\" + Vars.ID + "_express_" + Classes.Vars.Number_toeic + ".mp3";
        }

        public void express_statement()
        {
            //Vars.panel_timer_controller.Visible = true;
            //Vars.label_timer.Text = "00:30";
            Vars.lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\Express\" + Vars.Provider + "_express_" + Classes.Vars.Number_toeic + ".tryhard");
            count = 0;
            wmp.URL = @"C:\TryHard\Files\Express\" + Vars.Provider + "_express_" + Classes.Vars.Number_toeic + ".mp3"; ;
            wmp.controls.play();
            //MessageBox.Show(wmp.URL.ToString());
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(express_statement_controller);
            tmr.Start();
        }

        private void express_statement_controller(object Sender, EventArgs e)
        {
            stop_performing();
            TimeTest.duration = wmp.currentMedia.duration;
            //MessageBox.Show(count.ToString() + " - " + TimeTest.duration.ToString());
            if (count == (Math.Round(TimeTest.duration) + 2))
            //if (count == 2)
            {
                tmr.Stop();
                count = 0;
                //prep
                //MessageBox.Show("indication: " + position.ToString());
                //set_part();
                //performing = true;
                //MessageBox.Show("Good");
                //preparation = 3;
                Vars.panel_timer_controller.Visible = true;                
                preparation_time(15);
                duration_time = 16;
                Vars.label_timer.Text = "00:15";
            }
            count++;
        }

        public void preparation_direction_time()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1100;
            tmr.Tick += new EventHandler(preparation_direction_time_controller);            
            tmr.Start();
            wmp.URL = Functions.Path;
            wmp.controls.play();
            timer = new TimeTest();
            timer.preparation_direction_time();
        }
        private void preparation_direction_time_controller(object Sender, EventArgs e)
        {
            stop_performing();
            TimeTest.duration = wmp.currentMedia.duration;
            if (count == 8)
            {
                tmr.Stop();
            }
            count++;
        }


        private void preparation_time_controller(object Sender, EventArgs e)
        {
            stop_performing();
            if (preparation == 3)
            {
                wmp.URL = @"C:\TryHard\res\Beep.mp3";
                wmp.controls.play();
            }
            if (preparation > 5)
            {
                timer_text(1);
            }
            preparation++;
        }

        public void preparation_time(int duration)
        {
            preparation = 0;
            //state : 0 = preparation
            //state : 1 = respond
            count = 0;
            duration_time = duration;
            seconds = duration;
            Vars.label_topic_timer.Text = "PREPARATION TIME";
            //preparation = preparation_d;
            //MessageBox.Show("Hi");
            //Thread.Sleep(1500);
            wmp.URL = @"C:\TryHard\res\PTime.mp3";
            wmp.controls.play();
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(preparation_time_controller);
            tmr.Start();

        }

        public void respond_time(int duration)
        {            
            count = 0;
            duration_time = duration;
            seconds = duration;
            Vars.label_topic_timer.Text = "RESPOND TIME";
            wmp.URL = @"C:\TryHard\res\RTime.mp3";
            wmp.controls.play();
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(respond_time_controller);
            tmr.Start();
        }

        private void respond_time_controller(object Sender, EventArgs e)
        {
            stop_performing();
            if (preparation == 3)
            {
                wmp.URL = @"C:\TryHard\res\Beep.mp3";
                wmp.controls.play();
            }
            if (preparation > 4)
            {
                if(preparation == 5)
                {
                    //MessageBox.Show("To record");
                    record.record();
                }
                if(repeating == 1)
                {
                    if (Vars.Part_selection == 4)
                    {
                        position = 2;
                    }
                    timer_text(1);
                }
                else
                {
                    timer_text(0);
                }
            }
            preparation++;
        }

        public void question_time()
        {
            //performing = false;
            //MessageBox.Show(position.ToString());
            Functions.Path = @"C:\TryHard\Files\Respond\" + Vars.ID + "_respond_" + (position - 1) + "_" + Vars.Number_toeic + ".mp3";
            Vars.label_timer.Text = "00:00";
            Vars.label_topic_timer.Text = "RESPOND TIME";
            count = 0;
            wmp.URL = @"C:\TryHard\Files\Respond\" + Vars.Provider + "_respond_" + position + "_" + Vars.Number_toeic + ".mp3";
            //position++;
            wmp.controls.play();
            if(position == 1 || position == 2 || position == 3)
            {                
                if(position > 1)
                {
                    Vars.lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\Respond\" + Vars.Provider + "_respond_" + position + "_" + Vars.Number_toeic + ".tryhard");
                    Vars.label_timer.Text = "00:15";
                    Vars.panel_timer_controller.Visible = true;
                }
                repeating = 1;
                duration_time = 15;
                seconds = 15;
            }
            if(position == 4)
            {
                Vars.lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\Respond\" + Vars.Provider + "_respond_" + position + "_" + Vars.Number_toeic + ".tryhard");
                Vars.label_timer.Text = "00:30";
                repeating = 0;
                duration_time = 30;
                seconds = 30;
            }            
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(question_time_controller);
            tmr.Start();
        }

        public void question_time_provided()
        {
            //MessageBox.Show("Excelent");
            //MessageBox.Show(position.ToString());
            Functions.Path = @"C:\TryHard\Files\Respond_Provided\" + Vars.ID + "_respond_provided_" + (position - 1) + "_" + Vars.Number_toeic + ".mp3";
            Vars.label_timer.Text = "00:00";
            Vars.label_topic_timer.Text = "RESPOND TIME";
            count = 0;
            wmp.URL = @"C:\TryHard\Files\Respond_Provided\" + Vars.Provider + "_respond_provided_" + (position - 1) + "_" + Vars.Number_toeic + ".mp3";
            //position++;
            wmp.controls.play();
            if (position == 1 || position == 2 || position == 3)
            {
                Vars.label_timer.Text = "00:15";
                Vars.panel_timer_controller.Visible = true;
                repeating = 1;
                duration_time = 15;
                seconds = 15;
            }
            if (position == 4)
            {                
                Vars.label_timer.Text = "00:30";
                repeating = 0;
                duration_time = 30;
                seconds = 30;
            }
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(question_time_controller);
            tmr.Start();
        }

        private void question_time_controller(object Sender, EventArgs e)
        {
            stop_performing();
            count++;
            TimeTest.duration = wmp.currentMedia.duration;
            //MessageBox.Show(count.ToString() + " - " + TimeTest.duration.ToString());
            if (count == (Math.Round(TimeTest.duration) + 2))
            {
                tmr.Stop();
                //MessageBox.Show("Done");
                count = 0;
                if(!performing)
                {
                    wmp.URL = @"C:\TryHard\res\Q" + position + ".mp3";
                    wmp.controls.play();
                    tmr = new System.Windows.Forms.Timer();
                    tmr.Interval = 1000;
                    tmr.Tick += new EventHandler(question_indication_controller);
                    tmr.Start();
                    performing = true;
                    position++;
                }
                else
                {                    
                    //respond_time(15);
                    preparation = 3;
                    tmr = new System.Windows.Forms.Timer();
                    tmr.Interval = 1000;
                    tmr.Tick += new EventHandler(respond_time_controller);
                    tmr.Start();
                    performing = false;
                }
            }            
        }

        private void question_indication_controller(object Sender, EventArgs e)
        {
            stop_performing();
            TimeTest.duration = wmp.currentMedia.duration;
            //MessageBox.Show(count.ToString() + " - " + TimeTest.duration.ToString());
            //if (count == (Math.Round(TimeTest.duration) + 1))
            if (count == 2)
            {
                tmr.Stop();
                count = 0;
                //prep
                //MessageBox.Show("indication: " + position.ToString());
                set_part();
                //performing = true;
            }
            count++;
        }

        public void read_before()
        {
            stop_performing();
            string path = @"C:\TryHard\Files\Respond_Provided\" + Vars.Provider + "_info_provided_" + Vars.Number_toeic + ".png";
            if (File.Exists(path))
            {
                Vars.pbx_picture.Image = Image.FromFile(path);
                Vars.pbx_picture.Visible = true;
            }
            else
            {
                path = path.Replace(".png", ".tryhard");
                Vars.lbl_text.Text = File.ReadAllText(path);
                Vars.lbl_text.Visible = true;
            }
            Vars.panel_timer_controller.Visible = true;
            wmp.URL = @"C:\TryHard\res\4\ReadBefore.mp3";
            wmp.controls.play();
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(read_before_controller);
            tmr.Start();
        }

        private void read_before_controller(object Sender, EventArgs e)
        {
            stop_performing();
            TimeTest.duration = wmp.currentMedia.duration;
            //MessageBox.Show(count.ToString() + " - " + TimeTest.duration.ToString());
            if (count == Math.Round(TimeTest.duration))
            //if (count == 2)
            {
                tmr.Stop();
                count = 0;
                //prep
                //MessageBox.Show("indication: " + position.ToString());
                //set_part();
                //performing = true;
                //MessageBox.Show("Good");
                preparation = 3;
                duration_time = 31;
                seconds = 30;
                Vars.label_timer.Text = "00:30";
                Vars.label_topic_timer.Text = "PREPARATION TIME";
                tmr = new System.Windows.Forms.Timer();
                tmr.Interval = 1000;
                tmr.Tick += new EventHandler(preparation_time_controller);
                tmr.Start();
            }
            count++;
        }

        private void timer_text(int state)//(object Sender, EventArgs e)
        {
            stop_performing();
            if (count <  duration_time)
            {
                string text_timer = "";
                seconds--;
                if (seconds == 0)
                {
                    //minutes--;
                    seconds = 0;
                    text_timer = "0" + minutes + ":00";
                }
                else
                {
                    if (seconds < 10)
                    {
                        text_timer = "0" + minutes + ":0" + seconds;
                    }
                    else if (seconds > 9 && seconds < 60)
                    {
                        text_timer = "0" + minutes + ":" + seconds;
                    }
                }
                Vars.label_timer.Text = text_timer;
                count++;
            }
            else
            {
                tmr.Stop();
                record.save_recording();
                //MessageBox.Show("Good Performance");
                if (state == 1)
                {
                    //MessageBox.Show("Good Performance");                    
                    if(Vars.Part_selection == 6 || Vars.Part_selection == 7)
                    {
                        //MessageBox.Show("Duration: " + wmp.currentMedia.duration.ToString());
                        count = 1;
                        //TimeTest.duration = 3;
                        tmr = new System.Windows.Forms.Timer();
                        tmr.Interval = 1000;
                        tmr.Tick += new EventHandler(question_time_controller);
                        tmr.Start();
                    }
                    else
                    {
                        set_part();
                    }
                }
                else
                {
                    //MessageBox.Show("Good Performance");
                    frm_user_performance frm = new frm_user_performance();
                    frm.Show();
                    Classes.Vars.form.Close();
                    Classes.Vars.form = frm;
                    
                    
                }
            }
        }

        ///Writing
        
        private void save_sentences()
        {
            string path;
            for (int i = 0; i < sentences.Length; i++)
            {
                path = @"C:\TryHard\Files\Based\" + Vars.ID + "_sentence_" + (i + 1).ToString() + "_" + Vars.Number_toeic + ".tryhard";
                File.WriteAllText(path, sentences[i]);
            }
            //MessageBox.Show("Good Perfomrmance");
        }

        public void based()
        {
            Vars.label_timer.Text = "08:00";
            Vars.label_topic_timer.Text = "RESPOND TIME";
            minutes = 7;
            seconds = 60;
            count = 0;
            duration_time = 480;
            //position += 2;
            //question++;
            Vars.pbx_picture.Image = Image.FromFile(@"C:\TryHard\Files\Based\" + Vars.Provider + "_based_words_picture_" + question.ToString() + "_" + Classes.Vars.Number_toeic + ".png");
            words = File.ReadAllLines(@"C:\TryHard\Files\Based\" + Vars.Provider + "_based_words_" + Vars.Number_toeic + ".tryhard");
            Vars.label_number_sentence.Text = "Question " + question.ToString() + " - 5";
            Vars.label_words.Text = words[position - 1] + " / " + words[position];            
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(based_timer_controller);
            tmr.Start();
        }

        public void back_sentence()
        {            
            if (question > 1)
            {
                position -= 2;
                question-= 1;
                //MessageBox.Show("Back In: " + question.ToString());
                Vars.pbx_picture.Image = Image.FromFile(@"C:\TryHard\Files\Based\" + Vars.Provider + "_based_words_picture_" + question.ToString() + "_" + Classes.Vars.Number_toeic + ".png");
                words = File.ReadAllLines(@"C:\TryHard\Files\Based\" + Vars.Provider + "_based_words_" + Vars.Number_toeic + ".tryhard");
                Vars.label_number_sentence.Text = "Question " + question.ToString() + " - 5";
                Vars.label_words.Text = words[position - 1] + " / " + words[position];
                sentences[question] = Vars.txt_sentences.Text;
                Vars.txt_sentences.Text = sentences[question - 1];
            }
            //MessageBox.Show("Back out: " + question.ToString());
        }

        public void next_sentence()
        {            
            if (question < 5)
            {
                position += 2;
                question++;
                //MessageBox.Show("Next In: " + question.ToString());
                Vars.pbx_picture.Image = Image.FromFile(@"C:\TryHard\Files\Based\" + Vars.Provider + "_based_words_picture_" + question.ToString() + "_" + Classes.Vars.Number_toeic + ".png");
                words = File.ReadAllLines(@"C:\TryHard\Files\Based\" + Vars.Provider + "_based_words_" + Vars.Number_toeic + ".tryhard");
                Vars.label_number_sentence.Text = "Question " + question.ToString() + " - 5";
                Vars.label_words.Text = words[position - 1] + " / " + words[position];
                sentences[question - 2] = Vars.txt_sentences.Text;
                Vars.txt_sentences.Text = sentences[question - 1];
            }
            //MessageBox.Show("Next out: " + question.ToString());
        }

        public void written_request()
        {
            if (position < 3)
            {
                count = 0;
                Vars.label_timer.Text = "00:00";
                minutes = 9;
                seconds = 60;
                duration_time = 600;
                //MessageBox.Show(duration_time.ToString());
                string path = @"C:\TryHard\Files\Written\" + Vars.Provider + "_email_" + position + "_" + Vars.Number_toeic + ".tryhard";
                Vars.txt_email.Text = File.ReadAllText(path);
                path = path.Replace("email", "direction");
                Vars.label_direction.Text = File.ReadAllText(path);
                position++;
                tmr = new System.Windows.Forms.Timer();
                tmr.Interval = 1000;
                tmr.Tick += new EventHandler(written_request_controller);
                tmr.Start();
            }
            else
            {
                //MessageBox.Show("Good Performance Written");
                return;
            }
        }

        public void opinion_essay()
        {
            count = 0;
            Vars.label_timer.Text = "00:00";
            Vars.label_topic_timer.Text = "RESPOND TIME";
            minutes = 29;
            seconds = 60;
            duration_time = 1800;
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(opinion_essay_controller);
            tmr.Start();
        }

        private void based_timer_controller(object Sender, EventArgs e)
        {
            stop_performing();
            timer_text_writing(1);
        }

        private void written_request_controller(object Sender, EventArgs e)
        {
            stop_performing();
            timer_text_writing(1);
        }

        private void opinion_essay_controller(object Sender, EventArgs e)
        {
            stop_performing();
            timer_text_writing(1);
        }

        private void timer_text_writing(int state)//(object Sender, EventArgs e)
        {
            stop_performing();
            if (count < duration_time)
            {
                string text_timer = "";
                seconds--;
                if (seconds == 0)
                {
                    if(minutes > 0)
                    {
                        minutes--;
                    }
                    seconds = 0;
                    if(minutes < 10)
                    {
                        text_timer = "0" + minutes + ":00";
                    }
                    else
                    {
                        if(minutes == 0 && seconds == 0)
                        {
                            text_timer = "00:00";
                        }
                        else
                        {
                            text_timer = minutes + ":00";
                        }
                    }
                    seconds = 60;
                }
                else
                {
                    if (seconds < 10)
                    {
                        if(minutes < 10)
                        {
                            text_timer = "0" + minutes + ":0" + seconds;
                        }
                        else
                        {
                            text_timer = minutes + ":0" + seconds;
                        }
                    }
                    else if (seconds > 9 && seconds < 60)
                    {
                        if(minutes > 9 && minutes < 60)
                        {
                            text_timer = minutes + ":" + seconds;
                        }
                        else
                        {
                            text_timer = "0" + minutes + ":" + seconds;
                        }
                    }
                }                
                Vars.label_timer.Text = text_timer;
                count++;
            }
            else
            {
                tmr.Stop();
                set_part();
                //record.save_recording();
                //MessageBox.Show("Good Performance");
                /*if (state == 1)
                {
                    //MessageBox.Show("Good Performance");                    
                    if (Vars.Part_selection == 6 || Vars.Part_selection == 7)
                    {
                        //MessageBox.Show("Duration: " + wmp.currentMedia.duration.ToString());
                        count = 1;
                        //TimeTest.duration = 3;
                        tmr = new System.Windows.Forms.Timer();
                        tmr.Interval = 1000;
                        tmr.Tick += new EventHandler(question_time_controller);
                        tmr.Start();
                    }
                    else
                    {
                        set_part();
                    }
                }*/
            }
        }
        
        private void stop_performing()
        {
            if (!Classes.Vars.Performing)
            {
                tmr.Stop();
                wmp.URL = "";
                wmp.controls.stop();
                duration_time = 0;
                return;
            }
        }        
    }    
}
