﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CG.Web.MegaApiClient;
using MySql.Data.MySqlClient;
using System.Data;

namespace TryHard_.Classes
{
    public class Propiedades
    {
        private string username, password;

        public string Username
        {
            set
            {
                username = value;
            }
            get
            {
                return username;
            }
        }

        public string Password
        {
            set
            {
                password = value;
            }
            get
            {
                return password;
            }
        }       
    }        
    public class ConectarSQL : Propiedades
    {
        //protected string connString = "SERVER=localhost;DATABASE=tryharddb;UID=root;PASSWORD=pruebaservidor;Connection Timeout=30000;";
        //protected string connString = "SERVER=db4free.net;DATABASE=superatedb;UID=jacortez;PASSWORD=heartbeat;Connection Timeout=30000;";
        //protected string connString = "SERVER=127.0.0.1;DATABASE=tryhard;UID=root;PASSWORD=;Connection Timeout=30000;";
        protected string connString = "SERVER=192.168.1.115;DATABASE=tryhard;UID=Usr;PASSWORD=M@n@g3rR00t;Connection Timeout=30000;";
        protected MySqlConnection conn = new MySqlConnection();

        public ConectarSQL()
        {
            conn.ConnectionString = connString;
        }

        public void Conectar()
        {            
            conn.Open();
        }

        public void Desconectar()
        {
            conn.Close();            
        }
    }

    public class Consultas : ConectarSQL
    {
        protected MySqlCommand command;
        private DataTable table = new DataTable();

        public void Query(string query)
        {
            base.Conectar();
            command = new MySqlCommand(query, base.conn);
            command.ExecuteNonQuery();
            base.Desconectar();                       
        }

        public DataTable PullData(string query)
        {
            base.Conectar();
            MySqlDataAdapter da = new MySqlDataAdapter(query, base.conn);
            da.Fill(table);
            base.Desconectar();
            da.Dispose();
            return table;
        }
        
    }
}
