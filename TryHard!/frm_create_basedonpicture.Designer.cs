﻿namespace TryHard_
{
    partial class frm_create_basedonpicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_create_basedonpicture));
            this.lbl_part = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_img_1 = new System.Windows.Forms.Button();
            this.btn_img_2 = new System.Windows.Forms.Button();
            this.btn_img_3 = new System.Windows.Forms.Button();
            this.btn_img_5 = new System.Windows.Forms.Button();
            this.btn_img_4 = new System.Windows.Forms.Button();
            this.lbl_number_toeic = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_part
            // 
            this.lbl_part.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_part.AutoSize = true;
            this.lbl_part.BackColor = System.Drawing.Color.Transparent;
            this.lbl_part.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_part.Location = new System.Drawing.Point(462, 100);
            this.lbl_part.Name = "lbl_part";
            this.lbl_part.Size = new System.Drawing.Size(135, 17);
            this.lbl_part.TabIndex = 92;
            this.lbl_part.Text = "(Based on a Picture)";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(462, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 91;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(698, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 90;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(673, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 89;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 88;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(730, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 87;
            this.pbx_header.TabStop = false;
            // 
            // btn_next
            // 
            this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next.BackColor = System.Drawing.Color.White;
            this.btn_next.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_next.ForeColor = System.Drawing.Color.White;
            this.btn_next.Location = new System.Drawing.Point(589, 381);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(84, 41);
            this.btn_next.TabIndex = 125;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_save.BackColor = System.Drawing.Color.White;
            this.btn_save.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_save.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.Color.White;
            this.btn_save.Location = new System.Drawing.Point(494, 381);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(84, 41);
            this.btn_save.TabIndex = 124;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 11F);
            this.label2.Location = new System.Drawing.Point(36, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 18);
            this.label2.TabIndex = 94;
            this.label2.Text = "Select Images:";
            // 
            // btn_img_1
            // 
            this.btn_img_1.BackColor = System.Drawing.Color.Transparent;
            this.btn_img_1.BackgroundImage = global::TryHard_.Properties.Resources.add_image;
            this.btn_img_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_img_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_img_1.FlatAppearance.BorderSize = 0;
            this.btn_img_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btn_img_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_img_1.Location = new System.Drawing.Point(54, 211);
            this.btn_img_1.Name = "btn_img_1";
            this.btn_img_1.Size = new System.Drawing.Size(116, 112);
            this.btn_img_1.TabIndex = 126;
            this.btn_img_1.UseVisualStyleBackColor = false;
            this.btn_img_1.Click += new System.EventHandler(this.btn_img_1_Click);
            // 
            // btn_img_2
            // 
            this.btn_img_2.BackColor = System.Drawing.Color.Transparent;
            this.btn_img_2.BackgroundImage = global::TryHard_.Properties.Resources.add_image;
            this.btn_img_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_img_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_img_2.FlatAppearance.BorderSize = 0;
            this.btn_img_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btn_img_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_img_2.Location = new System.Drawing.Point(180, 211);
            this.btn_img_2.Name = "btn_img_2";
            this.btn_img_2.Size = new System.Drawing.Size(116, 112);
            this.btn_img_2.TabIndex = 127;
            this.btn_img_2.UseVisualStyleBackColor = false;
            this.btn_img_2.Click += new System.EventHandler(this.btn_img_2_Click);
            // 
            // btn_img_3
            // 
            this.btn_img_3.BackColor = System.Drawing.Color.Transparent;
            this.btn_img_3.BackgroundImage = global::TryHard_.Properties.Resources.add_image;
            this.btn_img_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_img_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_img_3.FlatAppearance.BorderSize = 0;
            this.btn_img_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btn_img_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_img_3.Location = new System.Drawing.Point(308, 211);
            this.btn_img_3.Name = "btn_img_3";
            this.btn_img_3.Size = new System.Drawing.Size(116, 112);
            this.btn_img_3.TabIndex = 128;
            this.btn_img_3.UseVisualStyleBackColor = false;
            this.btn_img_3.Click += new System.EventHandler(this.btn_img_3_Click);
            // 
            // btn_img_5
            // 
            this.btn_img_5.BackColor = System.Drawing.Color.Transparent;
            this.btn_img_5.BackgroundImage = global::TryHard_.Properties.Resources.add_image;
            this.btn_img_5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_img_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_img_5.FlatAppearance.BorderSize = 0;
            this.btn_img_5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btn_img_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_img_5.Location = new System.Drawing.Point(561, 211);
            this.btn_img_5.Name = "btn_img_5";
            this.btn_img_5.Size = new System.Drawing.Size(116, 112);
            this.btn_img_5.TabIndex = 130;
            this.btn_img_5.UseVisualStyleBackColor = false;
            this.btn_img_5.Click += new System.EventHandler(this.btn_img_5_Click);
            // 
            // btn_img_4
            // 
            this.btn_img_4.BackColor = System.Drawing.Color.Transparent;
            this.btn_img_4.BackgroundImage = global::TryHard_.Properties.Resources.add_image;
            this.btn_img_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_img_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_img_4.FlatAppearance.BorderSize = 0;
            this.btn_img_4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btn_img_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_img_4.Location = new System.Drawing.Point(434, 211);
            this.btn_img_4.Name = "btn_img_4";
            this.btn_img_4.Size = new System.Drawing.Size(116, 112);
            this.btn_img_4.TabIndex = 129;
            this.btn_img_4.UseVisualStyleBackColor = false;
            this.btn_img_4.Click += new System.EventHandler(this.btn_img_4_Click);
            // 
            // lbl_number_toeic
            // 
            this.lbl_number_toeic.AutoSize = true;
            this.lbl_number_toeic.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_toeic.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_number_toeic.Location = new System.Drawing.Point(20, 69);
            this.lbl_number_toeic.Name = "lbl_number_toeic";
            this.lbl_number_toeic.Size = new System.Drawing.Size(74, 21);
            this.lbl_number_toeic.TabIndex = 131;
            this.lbl_number_toeic.Text = "TOEIC #";
            // 
            // frm_create_basedonpicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(730, 450);
            this.Controls.Add(this.lbl_number_toeic);
            this.Controls.Add(this.btn_img_5);
            this.Controls.Add(this.btn_img_4);
            this.Controls.Add(this.btn_img_3);
            this.Controls.Add(this.btn_img_2);
            this.Controls.Add(this.btn_img_1);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_part);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_create_basedonpicture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_create_basedonpicture_FormClosing);
            this.Load += new System.EventHandler(this.frm_create_basedonpicture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_part;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_img_1;
        private System.Windows.Forms.Button btn_img_2;
        private System.Windows.Forms.Button btn_img_3;
        private System.Windows.Forms.Button btn_img_5;
        private System.Windows.Forms.Button btn_img_4;
        private System.Windows.Forms.Label lbl_number_toeic;
    }
}