﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_create_describepicture : Form
    {
        public string filename;
        public frm_create_describepicture()
        {
            InitializeComponent();
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofl = new OpenFileDialog();
            ofl.InitialDirectory = @"C:\";
            if (ofl.ShowDialog() == DialogResult.OK)
            {                
                if(!string.IsNullOrEmpty(filename))
                {
                    pbx_picture.Image.Dispose();
                }
                filename = ofl.FileName.ToString();
                pbx_picture.Image = Image.FromFile(filename);                
            }
        }

        private void frm_create_describepicture_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            if (Classes.Vars.State_exercise == 1)
            {
                try
                {
                    filename = @"C:\TryHard\Files\Describe\" + Classes.Vars.ID + "_describing_" + Classes.Vars.Number_toeic + ".png";
                    pbx_picture.Image = Image.FromFile(filename);
                    btn_search.Enabled = false;
                    btn_save.Text = "Modify";
                    btn_next.Text = "Close";                    
                }
                catch (Exception ez)
                {
                    MessageBox.Show(ez.Message);
                }
            }
            else
            {
                if (Classes.Vars.Part_selection == 5)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            //for (int i = 0; i < 9; i++)
            //{
              //  MessageBox.Show((i + 1).ToString() + " Describe- " + Classes.Vars.parts[i].ToString());
            //}
            Classes.Vars.Editing = 1;
            if (!string.IsNullOrEmpty(filename))
            {
                if(btn_next.Text == "Close")
                {
                    pbx_picture.Image.Dispose();
                    this.Close();     
                }
                else
                {                    
                    save_describing();
                    Form frm = new Form();
                    if (btn_next.Text == "Finish")
                    {
                        if(Classes.Vars.State_exercise == 1)
                        {
                            this.Close();
                            return;
                        }
                        else
                        {
                            frm = new frm_allparts();
                        }
                    }
                    else
                    {
                        frm = new frm_create_respondquestions();
                    }
                    frm.Show();
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("You should select a picture!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void save_describing()
        {
            try
            {                
                Classes.Functions.Path = @"C:\TryHard\Files\Describe";
                Classes.Functions fun = new Classes.Functions();
                fun.CreateFolder();
                string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_describing_" + Classes.Vars.Number_toeic + ".png";
                File.Copy(filename, path, true);
                Classes.SQLite sql = new Classes.SQLite();
                sql.update_parts("describe", 1);
                if (sql.exercise_state("describe"))
                {
                    Classes.Vars.parts[1] = 1;
                }
            }
            catch (Exception ff)
            {
                MessageBox.Show(ff.Message);
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if(btn_save.Text == "Modify")
            {                
                btn_next.Text = "Finish";
                this.Controls.Remove(btn_save);
                btn_search.Enabled = true;
                filename = "";                
                pbx_picture.Image.Dispose();
            }
            else
            {
                if (!string.IsNullOrEmpty(filename))
                {
                    save_describing();
                    Form frm = new frm_allparts();
                    frm.Show();
                    this.Close();
                }
                else
                {
                    if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                }
            }
    
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }
    }
}
