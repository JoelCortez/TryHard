﻿namespace TryHard_
{
    partial class frm_create_respondquestions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_create_respondquestions));
            this.lbl_part = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_search_introduction = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_intoduction = new System.Windows.Forms.TextBox();
            this.btn_play_introduction = new System.Windows.Forms.Button();
            this.btn_stop_introduction = new System.Windows.Forms.Button();
            this.btn_stop1 = new System.Windows.Forms.Button();
            this.btn_play1 = new System.Windows.Forms.Button();
            this.txt_question1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_search1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_stop2 = new System.Windows.Forms.Button();
            this.btn_play2 = new System.Windows.Forms.Button();
            this.txt_question2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_search2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_stop3 = new System.Windows.Forms.Button();
            this.btn_play3 = new System.Windows.Forms.Button();
            this.txt_question3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_search3 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.lbl_number_toeic = new System.Windows.Forms.Label();
            this.btn_record_introduction = new System.Windows.Forms.Button();
            this.lbl_done_1 = new System.Windows.Forms.Label();
            this.btn_record_1 = new System.Windows.Forms.Button();
            this.lbl_done_2 = new System.Windows.Forms.Label();
            this.btn_record_2 = new System.Windows.Forms.Button();
            this.lbl_done_3 = new System.Windows.Forms.Label();
            this.btn_record_3 = new System.Windows.Forms.Button();
            this.lbl_done_4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_part
            // 
            this.lbl_part.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_part.AutoSize = true;
            this.lbl_part.BackColor = System.Drawing.Color.Transparent;
            this.lbl_part.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_part.Location = new System.Drawing.Point(462, 100);
            this.lbl_part.Name = "lbl_part";
            this.lbl_part.Size = new System.Drawing.Size(158, 17);
            this.lbl_part.TabIndex = 47;
            this.lbl_part.Text = "(Respond To Questions)";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(462, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 46;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(698, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 45;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(673, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 44;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 43;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(730, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 42;
            this.pbx_header.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 11F);
            this.label1.Location = new System.Drawing.Point(28, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 18);
            this.label1.TabIndex = 48;
            this.label1.Text = "Introduction (Audio):";
            // 
            // btn_search_introduction
            // 
            this.btn_search_introduction.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search_introduction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search_introduction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search_introduction.FlatAppearance.BorderSize = 0;
            this.btn_search_introduction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search_introduction.Location = new System.Drawing.Point(177, 142);
            this.btn_search_introduction.Name = "btn_search_introduction";
            this.btn_search_introduction.Size = new System.Drawing.Size(20, 20);
            this.btn_search_introduction.TabIndex = 49;
            this.btn_search_introduction.UseVisualStyleBackColor = true;
            this.btn_search_introduction.Click += new System.EventHandler(this.btn_search_introduction_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 11F);
            this.label2.Location = new System.Drawing.Point(249, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 18);
            this.label2.TabIndex = 50;
            this.label2.Text = "Introduction:";
            // 
            // txt_intoduction
            // 
            this.txt_intoduction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_intoduction.BackColor = System.Drawing.Color.White;
            this.txt_intoduction.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_intoduction.ForeColor = System.Drawing.Color.DimGray;
            this.txt_intoduction.Location = new System.Drawing.Point(343, 142);
            this.txt_intoduction.Multiline = true;
            this.txt_intoduction.Name = "txt_intoduction";
            this.txt_intoduction.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_intoduction.Size = new System.Drawing.Size(360, 59);
            this.txt_intoduction.TabIndex = 51;
            this.txt_intoduction.UseSystemPasswordChar = true;
            // 
            // btn_play_introduction
            // 
            this.btn_play_introduction.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play_introduction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play_introduction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play_introduction.FlatAppearance.BorderSize = 0;
            this.btn_play_introduction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play_introduction.Location = new System.Drawing.Point(58, 168);
            this.btn_play_introduction.Name = "btn_play_introduction";
            this.btn_play_introduction.Size = new System.Drawing.Size(20, 20);
            this.btn_play_introduction.TabIndex = 52;
            this.btn_play_introduction.UseVisualStyleBackColor = true;
            this.btn_play_introduction.Click += new System.EventHandler(this.btn_play_introduction_Click);
            // 
            // btn_stop_introduction
            // 
            this.btn_stop_introduction.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop_introduction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop_introduction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop_introduction.FlatAppearance.BorderSize = 0;
            this.btn_stop_introduction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop_introduction.Location = new System.Drawing.Point(85, 168);
            this.btn_stop_introduction.Name = "btn_stop_introduction";
            this.btn_stop_introduction.Size = new System.Drawing.Size(20, 20);
            this.btn_stop_introduction.TabIndex = 53;
            this.btn_stop_introduction.UseVisualStyleBackColor = true;
            this.btn_stop_introduction.Click += new System.EventHandler(this.btn_stop_introduction_Click);
            // 
            // btn_stop1
            // 
            this.btn_stop1.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop1.FlatAppearance.BorderSize = 0;
            this.btn_stop1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop1.Location = new System.Drawing.Point(85, 241);
            this.btn_stop1.Name = "btn_stop1";
            this.btn_stop1.Size = new System.Drawing.Size(20, 20);
            this.btn_stop1.TabIndex = 59;
            this.btn_stop1.UseVisualStyleBackColor = true;
            this.btn_stop1.Click += new System.EventHandler(this.btn_stop1_Click);
            // 
            // btn_play1
            // 
            this.btn_play1.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play1.FlatAppearance.BorderSize = 0;
            this.btn_play1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play1.Location = new System.Drawing.Point(58, 241);
            this.btn_play1.Name = "btn_play1";
            this.btn_play1.Size = new System.Drawing.Size(20, 20);
            this.btn_play1.TabIndex = 58;
            this.btn_play1.UseVisualStyleBackColor = true;
            this.btn_play1.Click += new System.EventHandler(this.btn_play1_Click);
            // 
            // txt_question1
            // 
            this.txt_question1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_question1.BackColor = System.Drawing.Color.White;
            this.txt_question1.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_question1.ForeColor = System.Drawing.Color.DimGray;
            this.txt_question1.Location = new System.Drawing.Point(343, 215);
            this.txt_question1.Multiline = true;
            this.txt_question1.Name = "txt_question1";
            this.txt_question1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_question1.Size = new System.Drawing.Size(360, 46);
            this.txt_question1.TabIndex = 57;
            this.txt_question1.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Calibri", 11F);
            this.label3.Location = new System.Drawing.Point(249, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 18);
            this.label3.TabIndex = 56;
            this.label3.Text = "Question 1:";
            // 
            // btn_search1
            // 
            this.btn_search1.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search1.FlatAppearance.BorderSize = 0;
            this.btn_search1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search1.Location = new System.Drawing.Point(177, 215);
            this.btn_search1.Name = "btn_search1";
            this.btn_search1.Size = new System.Drawing.Size(20, 20);
            this.btn_search1.TabIndex = 55;
            this.btn_search1.UseVisualStyleBackColor = true;
            this.btn_search1.Click += new System.EventHandler(this.btn_search1_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Calibri", 11F);
            this.label4.Location = new System.Drawing.Point(28, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 18);
            this.label4.TabIndex = 54;
            this.label4.Text = "Please select Audio 1:";
            // 
            // btn_stop2
            // 
            this.btn_stop2.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop2.FlatAppearance.BorderSize = 0;
            this.btn_stop2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop2.Location = new System.Drawing.Point(85, 300);
            this.btn_stop2.Name = "btn_stop2";
            this.btn_stop2.Size = new System.Drawing.Size(20, 20);
            this.btn_stop2.TabIndex = 65;
            this.btn_stop2.UseVisualStyleBackColor = true;
            this.btn_stop2.Click += new System.EventHandler(this.btn_stop2_Click);
            // 
            // btn_play2
            // 
            this.btn_play2.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play2.FlatAppearance.BorderSize = 0;
            this.btn_play2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play2.Location = new System.Drawing.Point(58, 300);
            this.btn_play2.Name = "btn_play2";
            this.btn_play2.Size = new System.Drawing.Size(20, 20);
            this.btn_play2.TabIndex = 64;
            this.btn_play2.UseVisualStyleBackColor = true;
            this.btn_play2.Click += new System.EventHandler(this.btn_play2_Click);
            // 
            // txt_question2
            // 
            this.txt_question2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_question2.BackColor = System.Drawing.Color.White;
            this.txt_question2.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_question2.ForeColor = System.Drawing.Color.DimGray;
            this.txt_question2.Location = new System.Drawing.Point(343, 274);
            this.txt_question2.Multiline = true;
            this.txt_question2.Name = "txt_question2";
            this.txt_question2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_question2.Size = new System.Drawing.Size(360, 46);
            this.txt_question2.TabIndex = 63;
            this.txt_question2.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Calibri", 11F);
            this.label5.Location = new System.Drawing.Point(249, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 18);
            this.label5.TabIndex = 62;
            this.label5.Text = "Question 2:";
            // 
            // btn_search2
            // 
            this.btn_search2.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search2.FlatAppearance.BorderSize = 0;
            this.btn_search2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search2.Location = new System.Drawing.Point(177, 274);
            this.btn_search2.Name = "btn_search2";
            this.btn_search2.Size = new System.Drawing.Size(20, 20);
            this.btn_search2.TabIndex = 61;
            this.btn_search2.UseVisualStyleBackColor = true;
            this.btn_search2.Click += new System.EventHandler(this.btn_search2_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Calibri", 11F);
            this.label6.Location = new System.Drawing.Point(28, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 18);
            this.label6.TabIndex = 60;
            this.label6.Text = "Please select Audio 2:";
            // 
            // btn_stop3
            // 
            this.btn_stop3.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop3.FlatAppearance.BorderSize = 0;
            this.btn_stop3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop3.Location = new System.Drawing.Point(85, 359);
            this.btn_stop3.Name = "btn_stop3";
            this.btn_stop3.Size = new System.Drawing.Size(20, 20);
            this.btn_stop3.TabIndex = 71;
            this.btn_stop3.UseVisualStyleBackColor = true;
            this.btn_stop3.Click += new System.EventHandler(this.btn_stop3_Click);
            // 
            // btn_play3
            // 
            this.btn_play3.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play3.FlatAppearance.BorderSize = 0;
            this.btn_play3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play3.Location = new System.Drawing.Point(58, 359);
            this.btn_play3.Name = "btn_play3";
            this.btn_play3.Size = new System.Drawing.Size(20, 20);
            this.btn_play3.TabIndex = 70;
            this.btn_play3.UseVisualStyleBackColor = true;
            this.btn_play3.Click += new System.EventHandler(this.btn_play3_Click);
            // 
            // txt_question3
            // 
            this.txt_question3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_question3.BackColor = System.Drawing.Color.White;
            this.txt_question3.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_question3.ForeColor = System.Drawing.Color.DimGray;
            this.txt_question3.Location = new System.Drawing.Point(343, 333);
            this.txt_question3.Multiline = true;
            this.txt_question3.Name = "txt_question3";
            this.txt_question3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_question3.Size = new System.Drawing.Size(360, 46);
            this.txt_question3.TabIndex = 69;
            this.txt_question3.UseSystemPasswordChar = true;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 11F);
            this.label7.Location = new System.Drawing.Point(249, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 18);
            this.label7.TabIndex = 68;
            this.label7.Text = "Question 3:";
            // 
            // btn_search3
            // 
            this.btn_search3.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search3.FlatAppearance.BorderSize = 0;
            this.btn_search3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search3.Location = new System.Drawing.Point(177, 333);
            this.btn_search3.Name = "btn_search3";
            this.btn_search3.Size = new System.Drawing.Size(20, 20);
            this.btn_search3.TabIndex = 67;
            this.btn_search3.UseVisualStyleBackColor = true;
            this.btn_search3.Click += new System.EventHandler(this.btn_search3_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Calibri", 11F);
            this.label8.Location = new System.Drawing.Point(28, 333);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 18);
            this.label8.TabIndex = 66;
            this.label8.Text = "Please select Audio 3:";
            // 
            // btn_next
            // 
            this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next.BackColor = System.Drawing.Color.White;
            this.btn_next.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_next.ForeColor = System.Drawing.Color.White;
            this.btn_next.Location = new System.Drawing.Point(542, 390);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(84, 41);
            this.btn_next.TabIndex = 73;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_save.BackColor = System.Drawing.Color.White;
            this.btn_save.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_save.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.Color.White;
            this.btn_save.Location = new System.Drawing.Point(447, 390);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(84, 41);
            this.btn_save.TabIndex = 72;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // lbl_number_toeic
            // 
            this.lbl_number_toeic.AutoSize = true;
            this.lbl_number_toeic.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_toeic.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_number_toeic.Location = new System.Drawing.Point(20, 69);
            this.lbl_number_toeic.Name = "lbl_number_toeic";
            this.lbl_number_toeic.Size = new System.Drawing.Size(74, 21);
            this.lbl_number_toeic.TabIndex = 74;
            this.lbl_number_toeic.Text = "TOEIC #";
            // 
            // btn_record_introduction
            // 
            this.btn_record_introduction.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record_introduction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record_introduction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record_introduction.FlatAppearance.BorderSize = 0;
            this.btn_record_introduction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record_introduction.Location = new System.Drawing.Point(32, 168);
            this.btn_record_introduction.Name = "btn_record_introduction";
            this.btn_record_introduction.Size = new System.Drawing.Size(20, 21);
            this.btn_record_introduction.TabIndex = 124;
            this.btn_record_introduction.UseVisualStyleBackColor = true;
            this.btn_record_introduction.Click += new System.EventHandler(this.btn_record_introduction_Click);
            // 
            // lbl_done_1
            // 
            this.lbl_done_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_1.AutoSize = true;
            this.lbl_done_1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_1.Location = new System.Drawing.Point(110, 170);
            this.lbl_done_1.Name = "lbl_done_1";
            this.lbl_done_1.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_1.TabIndex = 123;
            this.lbl_done_1.Text = "Done!";
            this.lbl_done_1.Visible = false;
            // 
            // btn_record_1
            // 
            this.btn_record_1.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record_1.FlatAppearance.BorderSize = 0;
            this.btn_record_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record_1.Location = new System.Drawing.Point(32, 241);
            this.btn_record_1.Name = "btn_record_1";
            this.btn_record_1.Size = new System.Drawing.Size(20, 21);
            this.btn_record_1.TabIndex = 126;
            this.btn_record_1.UseVisualStyleBackColor = true;
            this.btn_record_1.Click += new System.EventHandler(this.btn_record_1_Click);
            // 
            // lbl_done_2
            // 
            this.lbl_done_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_2.AutoSize = true;
            this.lbl_done_2.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_2.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_2.Location = new System.Drawing.Point(110, 243);
            this.lbl_done_2.Name = "lbl_done_2";
            this.lbl_done_2.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_2.TabIndex = 125;
            this.lbl_done_2.Text = "Done!";
            this.lbl_done_2.Visible = false;
            // 
            // btn_record_2
            // 
            this.btn_record_2.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record_2.FlatAppearance.BorderSize = 0;
            this.btn_record_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record_2.Location = new System.Drawing.Point(32, 300);
            this.btn_record_2.Name = "btn_record_2";
            this.btn_record_2.Size = new System.Drawing.Size(20, 21);
            this.btn_record_2.TabIndex = 128;
            this.btn_record_2.UseVisualStyleBackColor = true;
            this.btn_record_2.Click += new System.EventHandler(this.btn_record_2_Click);
            // 
            // lbl_done_3
            // 
            this.lbl_done_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_3.AutoSize = true;
            this.lbl_done_3.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_3.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_3.Location = new System.Drawing.Point(110, 302);
            this.lbl_done_3.Name = "lbl_done_3";
            this.lbl_done_3.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_3.TabIndex = 127;
            this.lbl_done_3.Text = "Done!";
            this.lbl_done_3.Visible = false;
            // 
            // btn_record_3
            // 
            this.btn_record_3.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record_3.FlatAppearance.BorderSize = 0;
            this.btn_record_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record_3.Location = new System.Drawing.Point(32, 359);
            this.btn_record_3.Name = "btn_record_3";
            this.btn_record_3.Size = new System.Drawing.Size(20, 21);
            this.btn_record_3.TabIndex = 130;
            this.btn_record_3.UseVisualStyleBackColor = true;
            this.btn_record_3.Click += new System.EventHandler(this.btn_record_3_Click);
            // 
            // lbl_done_4
            // 
            this.lbl_done_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_4.AutoSize = true;
            this.lbl_done_4.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_4.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_4.Location = new System.Drawing.Point(110, 361);
            this.lbl_done_4.Name = "lbl_done_4";
            this.lbl_done_4.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_4.TabIndex = 129;
            this.lbl_done_4.Text = "Done!";
            this.lbl_done_4.Visible = false;
            // 
            // frm_create_respondquestions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(730, 450);
            this.Controls.Add(this.btn_record_3);
            this.Controls.Add(this.lbl_done_4);
            this.Controls.Add(this.btn_record_2);
            this.Controls.Add(this.lbl_done_3);
            this.Controls.Add(this.btn_record_1);
            this.Controls.Add(this.lbl_done_2);
            this.Controls.Add(this.btn_record_introduction);
            this.Controls.Add(this.lbl_done_1);
            this.Controls.Add(this.lbl_number_toeic);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_stop3);
            this.Controls.Add(this.btn_play3);
            this.Controls.Add(this.txt_question3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_search3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btn_stop2);
            this.Controls.Add(this.btn_play2);
            this.Controls.Add(this.txt_question2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_search2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_stop1);
            this.Controls.Add(this.btn_play1);
            this.Controls.Add(this.txt_question1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_search1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_stop_introduction);
            this.Controls.Add(this.btn_play_introduction);
            this.Controls.Add(this.txt_intoduction);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_search_introduction);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_part);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_create_respondquestions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_create_respondquestions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_part;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_search_introduction;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_intoduction;
        private System.Windows.Forms.Button btn_play_introduction;
        private System.Windows.Forms.Button btn_stop_introduction;
        private System.Windows.Forms.Button btn_stop1;
        private System.Windows.Forms.Button btn_play1;
        private System.Windows.Forms.TextBox txt_question1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_search1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_stop2;
        private System.Windows.Forms.Button btn_play2;
        private System.Windows.Forms.TextBox txt_question2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_search2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_stop3;
        private System.Windows.Forms.Button btn_play3;
        private System.Windows.Forms.TextBox txt_question3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_search3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label lbl_number_toeic;
        private System.Windows.Forms.Button btn_record_introduction;
        private System.Windows.Forms.Label lbl_done_1;
        private System.Windows.Forms.Button btn_record_1;
        private System.Windows.Forms.Label lbl_done_2;
        private System.Windows.Forms.Button btn_record_2;
        private System.Windows.Forms.Label lbl_done_3;
        private System.Windows.Forms.Button btn_record_3;
        private System.Windows.Forms.Label lbl_done_4;
    }
}