﻿namespace TryHard_
{
    partial class frm_select_part
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_select_part));
            this.lbl_welcome = new System.Windows.Forms.Label();
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.btn_continue = new System.Windows.Forms.Button();
            this.lbl_name = new System.Windows.Forms.Label();
            this.cb_speaking = new System.Windows.Forms.CheckBox();
            this.cb_writing = new System.Windows.Forms.CheckBox();
            this.cb_both = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cb_individual_speaking = new System.Windows.Forms.CheckBox();
            this.cbo_options_speaking = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo_options_writing = new System.Windows.Forms.ComboBox();
            this.cb_individual_writing = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_welcome
            // 
            this.lbl_welcome.AutoSize = true;
            this.lbl_welcome.BackColor = System.Drawing.Color.Transparent;
            this.lbl_welcome.Font = new System.Drawing.Font("Cambria", 20F, System.Drawing.FontStyle.Bold);
            this.lbl_welcome.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbl_welcome.Location = new System.Drawing.Point(24, 112);
            this.lbl_welcome.Name = "lbl_welcome";
            this.lbl_welcome.Size = new System.Drawing.Size(154, 32);
            this.lbl_welcome.TabIndex = 15;
            this.lbl_welcome.Text = "Select part:";
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(552, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 14;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(527, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 13;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 12;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(584, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 11;
            this.pbx_header.TabStop = false;
            // 
            // btn_continue
            // 
            this.btn_continue.BackColor = System.Drawing.Color.White;
            this.btn_continue.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_continue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_continue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_continue.Enabled = false;
            this.btn_continue.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_continue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_continue.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_continue.ForeColor = System.Drawing.Color.White;
            this.btn_continue.Location = new System.Drawing.Point(231, 302);
            this.btn_continue.Name = "btn_continue";
            this.btn_continue.Size = new System.Drawing.Size(124, 41);
            this.btn_continue.TabIndex = 16;
            this.btn_continue.Text = "Continue";
            this.btn_continue.UseVisualStyleBackColor = false;
            this.btn_continue.Click += new System.EventHandler(this.btn_continue_Click);
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl_name.Location = new System.Drawing.Point(316, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(204, 19);
            this.lbl_name.TabIndex = 19;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // cb_speaking
            // 
            this.cb_speaking.AutoSize = true;
            this.cb_speaking.BackColor = System.Drawing.Color.White;
            this.cb_speaking.Font = new System.Drawing.Font("Calibri", 14F);
            this.cb_speaking.Location = new System.Drawing.Point(40, 161);
            this.cb_speaking.Name = "cb_speaking";
            this.cb_speaking.Size = new System.Drawing.Size(98, 27);
            this.cb_speaking.TabIndex = 23;
            this.cb_speaking.Text = "Speaking";
            this.cb_speaking.UseVisualStyleBackColor = false;
            this.cb_speaking.CheckedChanged += new System.EventHandler(this.cb_speaking_CheckedChanged);
            // 
            // cb_writing
            // 
            this.cb_writing.AutoSize = true;
            this.cb_writing.BackColor = System.Drawing.Color.White;
            this.cb_writing.Font = new System.Drawing.Font("Calibri", 14F);
            this.cb_writing.Location = new System.Drawing.Point(40, 200);
            this.cb_writing.Name = "cb_writing";
            this.cb_writing.Size = new System.Drawing.Size(86, 27);
            this.cb_writing.TabIndex = 24;
            this.cb_writing.Text = "Writing";
            this.cb_writing.UseVisualStyleBackColor = false;
            this.cb_writing.CheckedChanged += new System.EventHandler(this.cb_writing_CheckedChanged);
            // 
            // cb_both
            // 
            this.cb_both.AutoSize = true;
            this.cb_both.BackColor = System.Drawing.Color.White;
            this.cb_both.Font = new System.Drawing.Font("Calibri", 14F);
            this.cb_both.Location = new System.Drawing.Point(40, 237);
            this.cb_both.Name = "cb_both";
            this.cb_both.Size = new System.Drawing.Size(192, 27);
            this.cb_both.TabIndex = 25;
            this.cb_both.Text = "Speaking and Writing";
            this.cb_both.UseVisualStyleBackColor = false;
            this.cb_both.CheckedChanged += new System.EventHandler(this.cb_both_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DodgerBlue;
            this.pictureBox1.Image = global::TryHard_.Properties.Resources.header;
            this.pictureBox1.Location = new System.Drawing.Point(117, 282);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(350, 1);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // cb_individual_speaking
            // 
            this.cb_individual_speaking.AutoSize = true;
            this.cb_individual_speaking.BackColor = System.Drawing.Color.White;
            this.cb_individual_speaking.Font = new System.Drawing.Font("Calibri", 28F);
            this.cb_individual_speaking.Location = new System.Drawing.Point(301, 166);
            this.cb_individual_speaking.Name = "cb_individual_speaking";
            this.cb_individual_speaking.Size = new System.Drawing.Size(15, 14);
            this.cb_individual_speaking.TabIndex = 28;
            this.cb_individual_speaking.UseVisualStyleBackColor = false;
            this.cb_individual_speaking.CheckedChanged += new System.EventHandler(this.cb_individual_CheckedChanged);
            // 
            // cbo_options_speaking
            // 
            this.cbo_options_speaking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_options_speaking.Enabled = false;
            this.cbo_options_speaking.Font = new System.Drawing.Font("Cambria", 11F);
            this.cbo_options_speaking.ForeColor = System.Drawing.Color.DimGray;
            this.cbo_options_speaking.FormattingEnabled = true;
            this.cbo_options_speaking.Items.AddRange(new object[] {
            "Reading Aloud",
            "Describing a Picture",
            "Respond To Questions",
            "Respond with information provided",
            "Propose a solution",
            "Express an opinion"});
            this.cbo_options_speaking.Location = new System.Drawing.Point(322, 161);
            this.cbo_options_speaking.Name = "cbo_options_speaking";
            this.cbo_options_speaking.Size = new System.Drawing.Size(241, 25);
            this.cbo_options_speaking.TabIndex = 29;
            this.cbo_options_speaking.SelectedIndexChanged += new System.EventHandler(this.cbo_options_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Cambria", 20F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(272, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 32);
            this.label1.TabIndex = 30;
            this.label1.Text = "Select section:";
            // 
            // cbo_options_writing
            // 
            this.cbo_options_writing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_options_writing.Enabled = false;
            this.cbo_options_writing.Font = new System.Drawing.Font("Cambria", 11F);
            this.cbo_options_writing.ForeColor = System.Drawing.Color.DimGray;
            this.cbo_options_writing.FormattingEnabled = true;
            this.cbo_options_writing.Items.AddRange(new object[] {
            "Based on a Picture",
            "Written Request",
            "Opinion Essay"});
            this.cbo_options_writing.Location = new System.Drawing.Point(322, 202);
            this.cbo_options_writing.Name = "cbo_options_writing";
            this.cbo_options_writing.Size = new System.Drawing.Size(145, 25);
            this.cbo_options_writing.TabIndex = 32;
            this.cbo_options_writing.SelectedIndexChanged += new System.EventHandler(this.cbo_options_writing_SelectedIndexChanged);
            // 
            // cb_individual_writing
            // 
            this.cb_individual_writing.AutoSize = true;
            this.cb_individual_writing.BackColor = System.Drawing.Color.White;
            this.cb_individual_writing.Font = new System.Drawing.Font("Calibri", 28F);
            this.cb_individual_writing.Location = new System.Drawing.Point(301, 207);
            this.cb_individual_writing.Name = "cb_individual_writing";
            this.cb_individual_writing.Size = new System.Drawing.Size(15, 14);
            this.cb_individual_writing.TabIndex = 31;
            this.cb_individual_writing.UseVisualStyleBackColor = false;
            this.cb_individual_writing.CheckedChanged += new System.EventHandler(this.cbo_individual_writing_CheckedChanged);
            // 
            // frm_select_part
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.cbo_options_writing);
            this.Controls.Add(this.cb_individual_writing);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbo_options_speaking);
            this.Controls.Add(this.cb_individual_speaking);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cb_both);
            this.Controls.Add(this.cb_writing);
            this.Controls.Add(this.cb_speaking);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.btn_continue);
            this.Controls.Add(this.lbl_welcome);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_select_part";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_select_part_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_welcome;
        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Button btn_continue;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.CheckBox cb_speaking;
        private System.Windows.Forms.CheckBox cb_writing;
        private System.Windows.Forms.CheckBox cb_both;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox cb_individual_speaking;
        private System.Windows.Forms.ComboBox cbo_options_speaking;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbo_options_writing;
        private System.Windows.Forms.CheckBox cb_individual_writing;
    }
}