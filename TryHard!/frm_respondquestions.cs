﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_respondquestions : Form
    {
        private Classes.Performance performance;

        public frm_respondquestions()
        {
            InitializeComponent();
        }

        private void frm_respondquestions_Load(object sender, EventArgs e)
        {
            //Classes.Vars.lbl_introduction = lbl_introduction;
            //Classes.Vars.label_timer = lbl
            Classes.Vars.lbl_text = lbl_text;
        }

        private void frm_respondquestions_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            //lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\ReadingAloud\" + Classes.Vars.Provider + "_reading_" + 1 + "_" + Classes.Vars.Number_toeic + ".tryhard");
            //Classes.Vars.label_timer.Text = "00:45";
            performance.question_time();
            lbl_introduction.Text = File.ReadAllText(@"C:\TryHard\Files\Respond\" + Classes.Vars.Provider + "_respond_" + 1 + "_" + Classes.Vars.Number_toeic + ".tryhard");
        }
    }    
}
