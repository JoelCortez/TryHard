﻿namespace TryHard_
{
    partial class frm_respondquestions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_introduction = new System.Windows.Forms.Label();
            this.lbl_text = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_introduction
            // 
            this.lbl_introduction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_introduction.BackColor = System.Drawing.Color.Transparent;
            this.lbl_introduction.Font = new System.Drawing.Font("Calibri", 15F);
            this.lbl_introduction.Location = new System.Drawing.Point(0, 0);
            this.lbl_introduction.Name = "lbl_introduction";
            this.lbl_introduction.Size = new System.Drawing.Size(700, 120);
            this.lbl_introduction.TabIndex = 32;
            this.lbl_introduction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_text
            // 
            this.lbl_text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_text.BackColor = System.Drawing.Color.Transparent;
            this.lbl_text.Font = new System.Drawing.Font("Calibri", 15F);
            this.lbl_text.Location = new System.Drawing.Point(0, 244);
            this.lbl_text.Name = "lbl_text";
            this.lbl_text.Size = new System.Drawing.Size(700, 120);
            this.lbl_text.TabIndex = 33;
            this.lbl_text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frm_respondquestions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(700, 400);
            this.Controls.Add(this.lbl_text);
            this.Controls.Add(this.lbl_introduction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_respondquestions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_respondquestions_Load);
            this.Shown += new System.EventHandler(this.frm_respondquestions_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_introduction;
        private System.Windows.Forms.Label lbl_text;
    }
}