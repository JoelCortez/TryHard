﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using CG.Web.MegaApiClient;

namespace TryHard_
{
    public partial class frm_signup_user : Form
    {
        Classes.Functions fun = new Classes.Functions();
        Thread t;
        public frm_signup_user()
        {
            InitializeComponent();
        }

        private void btn_accept_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt_id.Text) && !string.IsNullOrWhiteSpace(txt_password.Text))
            {
                try
                {                    
                    Form frm = this.Owner;
                    fun.DisableControls(txt_id, 2);
                    fun.DisableControls(txt_password, 2);
                    fun.DisableControls(btn_accept, 2);
                    Classes.Vars.ID = Classes.Cifrado.Encriptar(txt_id.Text);
                    Classes.Vars.Password = Classes.Cifrado.Encriptar(txt_password.Text);                
                    Classes.Functions.Path = @"C:\TryHard\Names";
                    fun.CreateFolder();
                    Classes.Functions.Path = Classes.Functions.Path + @"\" +Classes.Vars.ID+".tryhard";
                    string[] lines = { Classes.Vars.Privilege, Classes.Vars.Name, Classes.Vars.Last_Names,Classes.Vars.Password, Classes.Vars.Code.ToString(), Classes.Vars.Provider};
                    fun.CreateTxt(lines);                    
                    pnl_barshow.Visible = true;
                    t = new Thread(() =>
                     {
                         int state;
                         if (Classes.Cifrado.Desencriptar(Classes.Vars.Privilege.ToString()) == "Manager")
                         {
                             state = 1;
                         }
                         else
                         {
                             state = 2;
                         }
                         Classes.Threads.SignUp(pb_progress, lbl_info, this, state);                        
                         //this.Invoke((MethodInvoker)delegate { this.Close(); });
                     });
                    t.Start();
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Please try a different ID Number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    pnl_barshow.Visible = false;
                    fun.DisableControls(txt_id, 1);
                    fun.DisableControls(txt_password, 1);
                    fun.DisableControls(btn_accept, 1);
                    txt_id.Focus();
                }
            }
            else
                MessageBox.Show("Please fill the blank spaces", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
        }

        private void lbl_abort_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show(Classes.Cifrado.Encriptar("Section"));
        }

        private void MostrarPanel()
        {
            frm_panel panel = new frm_panel();
            panel.Invoke((MethodInvoker)delegate { panel.Show(); });
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            frm_login login = new frm_login();
            login.Show();
            this.Close();
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }
    }
}
