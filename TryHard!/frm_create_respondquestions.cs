﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using WMPLib;

namespace TryHard_
{
    public partial class frm_create_respondquestions : Form
    {
        public static string [] filenames, texts;
        OpenFileDialog ofl = new OpenFileDialog();
        WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        Classes.Functions fun = new Classes.Functions();

        public frm_create_respondquestions()
        {
            InitializeComponent();
        }

        private void frm_create_respondquestions_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            Classes.Vars.Editing = 1;
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
            filenames = new string[4];
            texts = new string[4];
            if(Classes.Vars.State_exercise == 1)
            {
                try
                {
                    for( int i = 1; i <=4; i++)
                    {
                        string path = @"C:\TryHard\Files\Respond\" + Classes.Vars.ID + "_respond_" + i + "_" + Classes.Vars.Number_toeic + ".mp3";
                        filenames[i - 1] = path;
                        path = path.Replace(".mp3", ".tryhard");
                        texts[i - 1] = File.ReadAllText(path);                        
                    }
                    txt_intoduction.Text = texts[0];
                    txt_question1.Text = texts[1];                    
                    txt_question2.Text = texts[2];
                    txt_question3.Text = texts[3];
                    btn_save.Text = "Modify";
                    btn_next.Text = "Close";
                    fun.DisableControls(btn_search_introduction, 2);
                    fun.DisableControls(btn_search1, 2);
                    fun.DisableControls(btn_search2, 2);
                    fun.DisableControls(btn_search3, 2);
                    txt_intoduction.ReadOnly = true;
                    txt_question1.ReadOnly = true;
                    txt_question2.ReadOnly = true;
                    txt_question3.ReadOnly = true;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }                
            }
            else
            {
                if (Classes.Vars.Part_selection == 6)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
        }

        private void btn_search_introduction_Click(object sender, EventArgs e)
        {
            open_dialog(0);
        }

        private void btn_search1_Click(object sender, EventArgs e)
        {
            open_dialog(1);
        }

        private void btn_search2_Click(object sender, EventArgs e)
        {
            open_dialog(2);
        }

        private void btn_search3_Click(object sender, EventArgs e)
        {
            open_dialog(3);
        }

        private void btn_play_introduction_Click(object sender, EventArgs e)
        {
            play_audio(filenames[0]);
        }

        private void btn_stop_introduction_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_play1_Click(object sender, EventArgs e)
        {
            play_audio(filenames[1]);
        }

        private void btn_play2_Click(object sender, EventArgs e)
        {
            play_audio(filenames[2]);
        }

        private void btn_play3_Click(object sender, EventArgs e)
        {
            play_audio(filenames[3]);
        }

        private void btn_stop1_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_stop2_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_stop3_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }
        private void play_audio(string source)
        {
            wmp.settings.volume = 100;
            try
            {
                if (!string.IsNullOrEmpty(source))
                {
                    //MessageBox.Show(source);
                    wmp.URL = source;
                    wmp.controls.play();
                }
                else
                {
                    MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if(btn_save.Text == "Modify")
            {
                fun.DisableControls(btn_search_introduction, 1);
                fun.DisableControls(btn_search1, 1);
                fun.DisableControls(btn_search2, 1);
                fun.DisableControls(btn_search3, 1);
                txt_intoduction.ReadOnly = false;
                txt_question1.ReadOnly = false;
                txt_question2.ReadOnly = false;
                txt_question3.ReadOnly = false;
                this.Controls.Remove(btn_save);                
                btn_next.Text = "Finish";
            }
            else
            {
                texts[0] = txt_intoduction.Text;
                texts[1] = txt_question1.Text;
                texts[2] = txt_question2.Text;
                texts[3] = txt_question3.Text;

                if (!string.IsNullOrEmpty(texts[0]) && !string.IsNullOrEmpty(texts[1]) && !string.IsNullOrEmpty(texts[2]) && !string.IsNullOrEmpty(texts[3]))
                {
                    if (!string.IsNullOrEmpty(filenames[0]) && !string.IsNullOrEmpty(filenames[1]) && !string.IsNullOrEmpty(filenames[2]) && !string.IsNullOrEmpty(filenames[3]))
                    {
                        Classes.Functions.Path = @"C:\TryHard\Files\Respond";
                        fun.CreateFolder();

                        try
                        {
                            save_respond();
                            Form frm = new frm_allparts();
                            frm.Show();
                            this.Close();
                        }
                        catch (Exception es)
                        {

                            MessageBox.Show(es.Message);
                        }


                    }
                    else
                    {
                        if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        {
                            Form frm = new frm_allparts();
                            frm.Show();
                            this.Close();
                        }
                    }

                }
                else
                {
                    if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                }
            }
        }

        private void open_dialog(int i)
        {
            ofl.Filter = "Mp3|*.mp3|Wav|*.wav";
            ofl.Title = "Select an audio";
            if (ofl.ShowDialog() == DialogResult.OK)
            {
                filenames[i] = ofl.FileName.ToString();
                this.Controls["lbl_done_" + (i + 1)].Visible = true;
            }
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
            wmp.URL = "";

            if(btn_next.Text == "Close")
            {
                this.Close();
                return;
            }
            else
            {
                texts[0] = txt_intoduction.Text;
                texts[1] = txt_question1.Text;
                texts[2] = txt_question2.Text;
                texts[3] = txt_question3.Text;

                if (!string.IsNullOrEmpty(texts[0]) && !string.IsNullOrEmpty(texts[1]) && !string.IsNullOrEmpty(texts[2]) && !string.IsNullOrEmpty(texts[3]))
                {
                    if (!string.IsNullOrEmpty(filenames[0]) && !string.IsNullOrEmpty(filenames[1]) && !string.IsNullOrEmpty(filenames[2]) && !string.IsNullOrEmpty(filenames[3]))
                    {
                        Classes.Functions.Path = @"C:\TryHard\Files\Respond";
                        fun.CreateFolder();
                        try
                        {
                            save_respond();
                            Form frm = new Form();
                            if (btn_next.Text == "Finish")
                            {
                                if (Classes.Vars.State_exercise == 1)
                                {
                                    this.Close();
                                    return;
                                }
                                else
                                {
                                    frm = new frm_allparts();
                                }
                            }
                            else
                            {
                                frm = new frm_create_infoprovided();
                            }
                            frm.Show();
                            this.Close();
                        }
                        catch (Exception es)
                        {

                            MessageBox.Show(es.Message);
                        }


                    }
                    else
                    {
                        MessageBox.Show("Please, select all the audios required", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                }
                else
                {
                    MessageBox.Show("Please, fill the blank spaces", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            
        }

        private void btn_record_introduction_Click(object sender, EventArgs e)
        {
            add_exercise(1, 1);
        }

        private void save_respond()
        {
            string path, text;
            for (int i = 0; i < 4; i++)
            {
                path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_respond_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                if(filenames[i] != path)
                {
                    File.Copy(filenames[i], path, true);
                }
                text = texts[i];
                path = path.Replace(".mp3", ".tryhard");
                //MessageBox.Show(text);
                File.WriteAllText(path, text);
            }
            Classes.SQLite sql = new Classes.SQLite();
            sql.update_parts("respond", 1);
            if (sql.exercise_state("respond"))
            {
                Classes.Vars.parts[2] = 1;
            }
        }

        private void btn_record_1_Click(object sender, EventArgs e)
        {
            add_exercise(1, 2);
        }

        private void btn_record_2_Click(object sender, EventArgs e)
        {
            add_exercise(1, 3);
        }

        private void btn_record_3_Click(object sender, EventArgs e)
        {
            add_exercise(1, 4);
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }

        private void add_exercise(int exercise, int n_exercise)
        {
            Form frm = new frm_record();
            frm.Owner = this;
            frm_record.exercise = exercise;
            frm_record.number_exercise = n_exercise;
            frm.ShowDialog();
        }
    }
}
