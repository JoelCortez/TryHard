﻿namespace TryHard_
{
    partial class frm_create_writtenrequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_create_writtenrequest));
            this.lbl_part = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.tbc_email = new System.Windows.Forms.TabControl();
            this.tb_email_1 = new System.Windows.Forms.TabPage();
            this.txt_email_1 = new System.Windows.Forms.TextBox();
            this.tb_email_2 = new System.Windows.Forms.TabPage();
            this.txt_email_2 = new System.Windows.Forms.TextBox();
            this.tbc_direction = new System.Windows.Forms.TabControl();
            this.tb_dir_1 = new System.Windows.Forms.TabPage();
            this.txt_email_dir_1 = new System.Windows.Forms.TextBox();
            this.tb_dir_2 = new System.Windows.Forms.TabPage();
            this.txt_email_dir_2 = new System.Windows.Forms.TextBox();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.lbl_number_toeic = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.tbc_email.SuspendLayout();
            this.tb_email_1.SuspendLayout();
            this.tb_email_2.SuspendLayout();
            this.tbc_direction.SuspendLayout();
            this.tb_dir_1.SuspendLayout();
            this.tb_dir_2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_part
            // 
            this.lbl_part.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_part.AutoSize = true;
            this.lbl_part.BackColor = System.Drawing.Color.Transparent;
            this.lbl_part.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_part.Location = new System.Drawing.Point(462, 100);
            this.lbl_part.Name = "lbl_part";
            this.lbl_part.Size = new System.Drawing.Size(122, 17);
            this.lbl_part.TabIndex = 99;
            this.lbl_part.Text = "(Written Request)";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(462, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 98;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(698, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 97;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(673, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 96;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 95;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(730, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 94;
            this.pbx_header.TabStop = false;
            // 
            // tbc_email
            // 
            this.tbc_email.Controls.Add(this.tb_email_1);
            this.tbc_email.Controls.Add(this.tb_email_2);
            this.tbc_email.Font = new System.Drawing.Font("Calibri", 10.5F);
            this.tbc_email.HotTrack = true;
            this.tbc_email.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbc_email.Location = new System.Drawing.Point(32, 217);
            this.tbc_email.Name = "tbc_email";
            this.tbc_email.SelectedIndex = 0;
            this.tbc_email.Size = new System.Drawing.Size(650, 174);
            this.tbc_email.TabIndex = 105;
            // 
            // tb_email_1
            // 
            this.tb_email_1.Controls.Add(this.txt_email_1);
            this.tb_email_1.Location = new System.Drawing.Point(4, 26);
            this.tb_email_1.Name = "tb_email_1";
            this.tb_email_1.Padding = new System.Windows.Forms.Padding(3);
            this.tb_email_1.Size = new System.Drawing.Size(642, 144);
            this.tb_email_1.TabIndex = 0;
            this.tb_email_1.Text = "Email 1";
            this.tb_email_1.UseVisualStyleBackColor = true;
            // 
            // txt_email_1
            // 
            this.txt_email_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_email_1.BackColor = System.Drawing.Color.White;
            this.txt_email_1.Font = new System.Drawing.Font("Cambria", 10F);
            this.txt_email_1.ForeColor = System.Drawing.Color.DimGray;
            this.txt_email_1.Location = new System.Drawing.Point(3, 5);
            this.txt_email_1.Multiline = true;
            this.txt_email_1.Name = "txt_email_1";
            this.txt_email_1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_email_1.Size = new System.Drawing.Size(631, 133);
            this.txt_email_1.TabIndex = 104;
            this.txt_email_1.UseSystemPasswordChar = true;
            // 
            // tb_email_2
            // 
            this.tb_email_2.Controls.Add(this.txt_email_2);
            this.tb_email_2.Location = new System.Drawing.Point(4, 26);
            this.tb_email_2.Name = "tb_email_2";
            this.tb_email_2.Padding = new System.Windows.Forms.Padding(3);
            this.tb_email_2.Size = new System.Drawing.Size(642, 144);
            this.tb_email_2.TabIndex = 1;
            this.tb_email_2.Text = "Email 2";
            this.tb_email_2.UseVisualStyleBackColor = true;
            // 
            // txt_email_2
            // 
            this.txt_email_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_email_2.BackColor = System.Drawing.Color.White;
            this.txt_email_2.Font = new System.Drawing.Font("Cambria", 10F);
            this.txt_email_2.ForeColor = System.Drawing.Color.DimGray;
            this.txt_email_2.Location = new System.Drawing.Point(3, 5);
            this.txt_email_2.Multiline = true;
            this.txt_email_2.Name = "txt_email_2";
            this.txt_email_2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_email_2.Size = new System.Drawing.Size(631, 133);
            this.txt_email_2.TabIndex = 105;
            this.txt_email_2.UseSystemPasswordChar = true;
            // 
            // tbc_direction
            // 
            this.tbc_direction.Controls.Add(this.tb_dir_1);
            this.tbc_direction.Controls.Add(this.tb_dir_2);
            this.tbc_direction.Font = new System.Drawing.Font("Calibri", 10.5F);
            this.tbc_direction.HotTrack = true;
            this.tbc_direction.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbc_direction.Location = new System.Drawing.Point(32, 100);
            this.tbc_direction.Name = "tbc_direction";
            this.tbc_direction.SelectedIndex = 0;
            this.tbc_direction.Size = new System.Drawing.Size(424, 111);
            this.tbc_direction.TabIndex = 2;
            // 
            // tb_dir_1
            // 
            this.tb_dir_1.Controls.Add(this.txt_email_dir_1);
            this.tb_dir_1.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_dir_1.Location = new System.Drawing.Point(4, 26);
            this.tb_dir_1.Name = "tb_dir_1";
            this.tb_dir_1.Padding = new System.Windows.Forms.Padding(3);
            this.tb_dir_1.Size = new System.Drawing.Size(416, 81);
            this.tb_dir_1.TabIndex = 0;
            this.tb_dir_1.Text = "Email 1 Directions";
            this.tb_dir_1.UseVisualStyleBackColor = true;
            // 
            // txt_email_dir_1
            // 
            this.txt_email_dir_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_email_dir_1.BackColor = System.Drawing.Color.White;
            this.txt_email_dir_1.Font = new System.Drawing.Font("Cambria", 10F);
            this.txt_email_dir_1.ForeColor = System.Drawing.Color.DimGray;
            this.txt_email_dir_1.Location = new System.Drawing.Point(3, 5);
            this.txt_email_dir_1.Multiline = true;
            this.txt_email_dir_1.Name = "txt_email_dir_1";
            this.txt_email_dir_1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_email_dir_1.Size = new System.Drawing.Size(405, 70);
            this.txt_email_dir_1.TabIndex = 104;
            this.txt_email_dir_1.UseSystemPasswordChar = true;
            // 
            // tb_dir_2
            // 
            this.tb_dir_2.Controls.Add(this.txt_email_dir_2);
            this.tb_dir_2.Font = new System.Drawing.Font("Cambria", 9.75F);
            this.tb_dir_2.Location = new System.Drawing.Point(4, 26);
            this.tb_dir_2.Name = "tb_dir_2";
            this.tb_dir_2.Padding = new System.Windows.Forms.Padding(3);
            this.tb_dir_2.Size = new System.Drawing.Size(416, 81);
            this.tb_dir_2.TabIndex = 1;
            this.tb_dir_2.Text = "Email 2 Directions";
            this.tb_dir_2.UseVisualStyleBackColor = true;
            // 
            // txt_email_dir_2
            // 
            this.txt_email_dir_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_email_dir_2.BackColor = System.Drawing.Color.White;
            this.txt_email_dir_2.Font = new System.Drawing.Font("Cambria", 10F);
            this.txt_email_dir_2.ForeColor = System.Drawing.Color.DimGray;
            this.txt_email_dir_2.Location = new System.Drawing.Point(3, 5);
            this.txt_email_dir_2.Multiline = true;
            this.txt_email_dir_2.Name = "txt_email_dir_2";
            this.txt_email_dir_2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_email_dir_2.Size = new System.Drawing.Size(405, 70);
            this.txt_email_dir_2.TabIndex = 105;
            this.txt_email_dir_2.UseSystemPasswordChar = true;
            // 
            // btn_next
            // 
            this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next.BackColor = System.Drawing.Color.White;
            this.btn_next.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_next.ForeColor = System.Drawing.Color.White;
            this.btn_next.Location = new System.Drawing.Point(598, 397);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(84, 41);
            this.btn_next.TabIndex = 108;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_save.BackColor = System.Drawing.Color.White;
            this.btn_save.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_save.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.Color.White;
            this.btn_save.Location = new System.Drawing.Point(503, 397);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(84, 41);
            this.btn_save.TabIndex = 107;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // lbl_number_toeic
            // 
            this.lbl_number_toeic.AutoSize = true;
            this.lbl_number_toeic.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_toeic.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_number_toeic.Location = new System.Drawing.Point(20, 69);
            this.lbl_number_toeic.Name = "lbl_number_toeic";
            this.lbl_number_toeic.Size = new System.Drawing.Size(74, 21);
            this.lbl_number_toeic.TabIndex = 109;
            this.lbl_number_toeic.Text = "TOEIC #";
            // 
            // frm_create_writtenrequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(730, 450);
            this.Controls.Add(this.lbl_number_toeic);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.tbc_direction);
            this.Controls.Add(this.tbc_email);
            this.Controls.Add(this.lbl_part);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_create_writtenrequest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_create_writtenrequest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.tbc_email.ResumeLayout(false);
            this.tb_email_1.ResumeLayout(false);
            this.tb_email_1.PerformLayout();
            this.tb_email_2.ResumeLayout(false);
            this.tb_email_2.PerformLayout();
            this.tbc_direction.ResumeLayout(false);
            this.tb_dir_1.ResumeLayout(false);
            this.tb_dir_1.PerformLayout();
            this.tb_dir_2.ResumeLayout(false);
            this.tb_dir_2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_part;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.TabControl tbc_email;
        private System.Windows.Forms.TabPage tb_email_1;
        private System.Windows.Forms.TextBox txt_email_1;
        private System.Windows.Forms.TabPage tb_email_2;
        private System.Windows.Forms.TextBox txt_email_2;
        private System.Windows.Forms.TabControl tbc_direction;
        private System.Windows.Forms.TabPage tb_dir_1;
        private System.Windows.Forms.TextBox txt_email_dir_1;
        private System.Windows.Forms.TabPage tb_dir_2;
        private System.Windows.Forms.TextBox txt_email_dir_2;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label lbl_number_toeic;
    }
}