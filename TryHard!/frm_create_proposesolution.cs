﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using System.IO;

namespace TryHard_
{
    public partial class frm_create_proposesolution : Form
    {
        WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        OpenFileDialog ofl = new OpenFileDialog();
        public static string filename;

        public frm_create_proposesolution()
        {
            InitializeComponent();
        }

        private void frm_create_proposesolution_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            if (Classes.Vars.State_exercise == 1)
            {
                try
                {
                    string path = @"C:\TryHard\Files\Propose\" + Classes.Vars.ID + "_propose_" + Classes.Vars.Number_toeic + ".mp3";
                    btn_search.Enabled = false;
                    filename = path;
                    btn_save.Text = "Modify";
                    btn_next.Text = "Close";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                if (Classes.Vars.Part_selection == 8)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
            wmp.URL = "";
            ofl.Filter = "Mp3|*.mp3|Wav|*.wav";
            if (ofl.ShowDialog() == DialogResult.OK)
            {
                filename = ofl.FileName.ToString();
                if(Classes.Vars.State_exercise == 1)
                {
                    lbl_done.Text = "Changed!";
                }
                else
                {
                    lbl_done.Text = "Done!";
                }
                lbl_done.Visible = true;
            }
        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            wmp.settings.volume = 100;
            if (!string.IsNullOrEmpty(filename))
            {
                wmp.URL = filename;
                wmp.controls.play();
            }
            else
            {
                MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            Classes.Vars.Editing = 1;
            wmp.controls.stop();
            wmp.URL = "";
            try
            {
                if (!string.IsNullOrEmpty(filename))
                {
                    save_propose();
                    Form frm = new Form();
                    if(btn_next.Text == "Close")
                    {
                        this.Close();
                        return;
                    }
                    else
                    {
                        if (btn_next.Text == "Finish")
                        {
                            if (Classes.Vars.State_exercise == 1)
                            {
                                this.Close();
                                return;
                            }
                            else
                            {
                                frm = new frm_allparts();
                            }
                        }
                        else
                        {
                            frm = new frm_create_expressopinion();
                        }
                    }
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message);
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if(btn_save.Text == "Modify")
                {
                    btn_next.Text = "Finish";
                    btn_search.Enabled = true;
                    this.Controls.Remove(btn_save);
                }
                else
                {
                    if (!string.IsNullOrEmpty(filename))
                    {
                        save_propose();
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                    else
                    {
                        if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        {
                            Form frm = new frm_allparts();
                            frm.Show();
                            this.Close();
                        }
                    }
                }
            }
            catch (Exception)
            {
                                
            }           
        }
        private void save_propose()
        {
            Classes.Functions.Path = @"C:\TryHard\Files\Propose";
            Classes.Functions fun = new Classes.Functions();
            fun.CreateFolder();
            string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_propose_" + Classes.Vars.Number_toeic + ".mp3";
            if(filename != path)
            {
                File.Copy(filename, path, true);
            }
            Classes.SQLite sql = new Classes.SQLite();
            sql.update_parts("propose", 1);
            if (sql.exercise_state("propose"))
            {
                Classes.Vars.parts[4] = 1;
            }
        }

        private void btn_record_Click(object sender, EventArgs e)
        {
            Form frm = new frm_record();
            frm.Owner = this;
            frm_record.exercise = 3;
            frm.ShowDialog();
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }
    }
}
