﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CG.Web.MegaApiClient;
using System.Threading;

namespace TryHard_
{
    public partial class frm_create_section : Form
    {
        Classes.Mega host = new Classes.Mega();
        Classes.Queries query = new Classes.Queries();
        public frm_create_section()
        {
            InitializeComponent();
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_accept_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt_code.Text) && !string.IsNullOrWhiteSpace(txt_section.Text))
            {
                this.Enabled = false;
                Classes.Functions fun = new Classes.Functions();
                Classes.Vars.Code = Classes.Cifrado.Encriptar(txt_code.Text);
                Classes.Functions.Path = @"C:\TryHard\Section";
                fun.CreateFolder();
                Classes.Functions.Path = Classes.Functions.Path + @"\" + Classes.Vars.Code + ".tryhard";
                string[] lineas = { txt_section.Text, Classes.Vars.ID};
                fun.DisableControls(txt_section, 2);
                fun.DisableControls(txt_code, 2);
                fun.DisableControls(btn_accept, 2);
                Thread t2 = new Thread(() =>
                {
                    try
                    {
                        this.Invoke(new MethodInvoker(delegate {
                            //this.Enabled = false;
                        }));
                        query.insert_section();
                        MessageBox.Show("Section created", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Invoke(new MethodInvoker(delegate {
                            this.Enabled = false;
                            this.Close();
                        }));
                    }
                    catch (Exception me)
                    {
                        MessageBox.Show(me.Message);
                        query.Desconectar();
                        this.Invoke((MethodInvoker)delegate
                        {
                            fun.DisableControls(txt_section, 1);
                            fun.DisableControls(txt_code, 1);
                            fun.DisableControls(btn_accept, 1);
                            txt_code.Focus();
                        });
                    }
                });
                Thread t = new Thread(() =>
                {
                    try
                    {
                        fun.CreateTxt(lineas);
                        INode folder = host.root("Section");
                        host.upload(Classes.Functions.Path, folder);                        
                        t2.Start();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Please try a different Code Number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        MessageBox.Show(ex.Message);
                        this.Invoke((MethodInvoker)delegate
                        {
                            fun.DisableControls(txt_section, 1);
                            fun.DisableControls(txt_code, 1);
                            fun.DisableControls(btn_accept, 1);
                            txt_code.Focus();
                        });
                    }                    
                });
                t.Start();
            }
        }

        private void txt_code_TextChanged(object sender, EventArgs e)
        {
            if (txt_code.TextLength != 5)
                            
                btn_accept.Enabled = false;            
            else
                btn_accept.Enabled = true;
        }
    }
}
