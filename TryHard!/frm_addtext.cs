﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_addtext : Form
    {
        public frm_addtext()
        {
            InitializeComponent();
        }

        PictureBox pbx = new PictureBox();

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if(this.Controls.Contains(pbx))
            {
                this.Controls.Remove(pbx);
                pbx.Image.Dispose();
                //this.Controls.Remove(this.Controls["pbx_picture"]);
            }
            this.Close();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt_informationP.Text))
            {
                Form form = this.Owner;                
                frm_create_infoprovided.texts = txt_informationP.Text;
                frm_create_infoprovided.text_image = 2;
                if(Classes.Vars.State_exercise == 1)
                {
                    form.Controls["lbl_done_info"].Text = "Text - Changed!";
                }
                else
                {
                    form.Controls["lbl_done_info"].Text = "Text - Done!";
                }                   
                form.Controls["lbl_done_info"].Visible = true;
                this.Close();
            }
        }

        private void frm_addtext_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Load");
            if (Classes.Vars.State_exercise == 1)
            {
                if(frm_create_infoprovided.show != 1)
                {
                    this.Controls.Remove(btn_save);
                    try
                    {
                        if (frm_create_infoprovided.text_image == 1)
                        {
                            string path = "";
                            /*if(frm_create_infoprovided.filenames[3] != "")
                            {
                                path = @"C:\TryHard\Files\Respond_Provided\" + Classes.Vars.ID + "_info_provided_" + Classes.Vars.Number_toeic + ".png";
                            }
                            else
                            {
                                path = frm_create_infoprovided.filenames[3];
                            }*/
                            try
                            {
                                path = frm_create_infoprovided.filenames[3];
                            }
                            catch (Exception)
                            {

                                //throw;
                            }
                            this.Controls.Remove(txt_informationP);                            
                            pbx.Name = "pbx_picture";
                            pbx.BackColor = Color.White;
                            pbx.Size = new Size(340, 210);
                            pbx.Location = new Point(24, 90);
                            pbx.SizeMode = PictureBoxSizeMode.StretchImage;
                            pbx.Image = Image.FromFile(path);
                            this.Controls.Add(pbx);
                        }
                        else if (frm_create_infoprovided.text_image == 2)
                        {                            
                            txt_informationP.Text = frm_create_infoprovided.texts;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }                
            }
            else
            {
                if (!string.IsNullOrEmpty(frm_create_infoprovided.texts))
                {
                    txt_informationP.Text = frm_create_infoprovided.texts;
                }
            }
        }

        private void frm_addtext_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.Controls.Contains(pbx))
            {
                pbx.Image.Dispose();
            }
        }

        private void frm_addtext_Shown(object sender, EventArgs e)
        {
            //MessageBox.Show("Shown");
        }
    }
}
