﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_addtext_basedonapicture : Form
    {
        public static int position_word = 0;
        public frm_addtext_basedonapicture()
        {
            InitializeComponent();
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_accept_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txt_word_1.Text) && !String.IsNullOrEmpty(txt_word_2.Text))
            {
                string word_1 = txt_word_1.Text;
                string word_2 = txt_word_2.Text;
                switch (position_word)
                {
                    case 1:
                        frm_create_basedonpicture.words[0, 0] = word_1;
                        frm_create_basedonpicture.words[0, 1] = word_2;
                        break;
                    case 2:
                        frm_create_basedonpicture.words[1, 0] = word_1;
                        frm_create_basedonpicture.words[1, 1] = word_2;
                        break;
                    case 3:
                        frm_create_basedonpicture.words[2, 0] = word_1;
                        frm_create_basedonpicture.words[2, 1] = word_2;
                        break;
                    case 4:
                        frm_create_basedonpicture.words[3, 0] = word_1;
                        frm_create_basedonpicture.words[3, 1] = word_2;
                        break;
                    case 5:
                        frm_create_basedonpicture.words[4, 0] = word_1;
                        frm_create_basedonpicture.words[4, 1] = word_2;
                        break;
                    default: MessageBox.Show("");
                        break;
                }
                this.Close();
            }
            else
            {

            }
            
        }

        private void frm_addtext_basedonapicture_Load(object sender, EventArgs e)
        {
            if (frm_create_basedonpicture.ofl_state == 0)
            {
                btn_accept.Enabled = false;
            }            
            switch (position_word)
            {
                case 1:
                    txt_word_1.Text = frm_create_basedonpicture.words[0, 0];
                    txt_word_2.Text = frm_create_basedonpicture.words[0, 1];
                    break;
                case 2:
                    txt_word_1.Text = frm_create_basedonpicture.words[1, 0];
                    txt_word_2.Text = frm_create_basedonpicture.words[1, 1];
                    break;
                case 3:
                    txt_word_1.Text = frm_create_basedonpicture.words[2, 0];
                    txt_word_2.Text = frm_create_basedonpicture.words[2, 1];
                    break;
                case 4:
                    txt_word_1.Text = frm_create_basedonpicture.words[3, 0];
                    txt_word_2.Text = frm_create_basedonpicture.words[3, 1];
                    break;
                case 5:
                    txt_word_1.Text = frm_create_basedonpicture.words[4, 0];
                    txt_word_2.Text = frm_create_basedonpicture.words[4, 1];
                    break;
                default:
                    MessageBox.Show("");
                    break;
            }
        }
    }
}
