﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_background : Form
    {
        public frm_background()
        {
            InitializeComponent();
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btn_mostrar_Click(object sender, EventArgs e)
        {
            //frm_expressopinion form = Application.OpenForms.OfType<frm_expressopinion>().FirstOrDefault();
            //frm_expressopinion form1 = form ?? new frm_expressopinion();
            //Form form = new frm_readaloud();
            //form.TopLevel = false;
            //this.pnl_container.Controls.Add(form);
            //form.Show();
            //change();

        }            
        
        private void frm_background_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            pnl_container.Size = new Size(700, 400);
            pnl_container.Location = new Point((this.Width / 2) - (pnl_container.Width / 2), (this.Height / 2) - (pnl_container.Height / 2));
            this.MinimumSize = new Size(this.Width, this.Height);
            pnl_time_controller.Size = new Size(270, 100);
            pnl_time_controller.Location = new Point((this.Width / 2) - (pnl_time_controller.Width / 2), pnl_container.Location.Y + 410);
            //MessageBox.Show(pnl_container.Location.X.ToString() +  " - " + pnl_container.Location.Y.ToString());
            Classes.Vars.panel_container = pnl_container;
            Classes.Vars.panel_timer_controller = pnl_time_controller;
            Classes.Vars.label_timer = lbl_timer;
            Classes.Vars.label_topic_timer = lbl_timer_topic;
            Classes.Vars.label_part = lbl_part;
            Classes.Vars.Performing = true;
            //Classes.Vars.Part_selection = 5;
        }

        private void frm_background_Resize(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized && this.WindowState != FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void frm_background_Shown(object sender, EventArgs e)
        {
            if(Classes.Vars.Part_selection > 9)
            {
                Form frm = new frm_directions();
                frm.TopLevel = false;
                this.pnl_container.Controls.Add(frm);
                frm.Show();
                Classes.Vars.Last_opened = "frm_directions";
            }
            else
            {
                Form frm = new frm_microphone_verification();
                frm.TopLevel = false;
                this.pnl_container.Controls.Add(frm);
                frm.Show();
                Classes.Vars.Last_opened = "frm_microphone_verification";
            }
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            //Classes.Performance pr = new Classes.Performance()
            //Dispose(false);
            //Classes.Destructor ds = new Classes.Destructor();
            Classes.Vars.Charging = 1;
            Classes.Vars.Performing = false;
            frm_panel frm = new frm_panel();
            Classes.Vars.form = frm;
            frm.Show();
            this.Close();
        }
    }
}
