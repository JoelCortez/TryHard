﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using WMPLib;

namespace TryHard_
{
    public partial class frm_create_infoprovided : Form
    {
        public static string[] filenames;
        public static string texts;
        OpenFileDialog ofl = new OpenFileDialog();
        public static int text_image = 0;
        public static int show = 0;
        WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        Classes.Functions fun = new Classes.Functions();

        public frm_create_infoprovided()
        {
            InitializeComponent();
        }

        private void frm_create_infoprovided_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
            filenames = new string[4];            
            if(Classes.Vars.State_exercise == 1)
            {
                string path = @"C:\TryHard\Files\Respond_Provided\" + Classes.Vars.ID + "_info_provided_" + Classes.Vars.Number_toeic + ".png";
                if(File.Exists(path))
                {
                    text_image = 1;
                    filenames[3] = path;
                }
                else
                {
                    path = path.Replace(".png", ".tryhard");
                    if (File.Exists(path))
                    {
                        text_image = 2;
                        filenames[3] = path;
                        texts = File.ReadAllText(path);
                    }
                }
                btn_check.Visible = true;
                fun.DisableControls(btn_search_image, 2);
                fun.DisableControls(btn_insert_text, 2);
                fun.DisableControls(btn_search1, 2);
                fun.DisableControls(btn_search2, 2);
                fun.DisableControls(btn_search3, 2);
                btn_save.Text = "Modify";
                btn_next.Text = "Close";
                for(int i = 0; i < 3; i++)
                {
                    //path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_respond_provided_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                    path = @"C:\TryHard\Files\Respond_Provided\" + Classes.Vars.ID + "_respond_provided_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                    //MessageBox.Show(path);
                    filenames[i] = path;
                }
            }
            else
            {
                if (Classes.Vars.Part_selection == 7)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            Classes.Vars.Editing = 1;
            wmp.controls.stop();
            wmp.URL = "";
            if (!string.IsNullOrEmpty(filenames[0]) && !string.IsNullOrEmpty(filenames[1]) && !string.IsNullOrEmpty(filenames[2]))
            {
                if (text_image != 0)
                {
                    Classes.Functions.Path = @"C:\TryHard\Files\Respond_Provided";
                    //MessageBox.Show(Classes.Functions.Path);
                    fun.CreateFolder();
                    try
                    {                        
                        save_respond_provided();
                        Form frm = new Form();
                        if(btn_next.Text == "Close")
                        {
                            this.Close();
                            return;
                        }
                        else
                        {
                            if (btn_next.Text == "Finish")
                            {
                                if (Classes.Vars.State_exercise == 1)
                                {
                                    this.Close();
                                    return;
                                }
                                else
                                {
                                    frm = new frm_allparts();
                                }
                            }
                            else
                            {
                                frm = new frm_create_proposesolution();
                            }
                        }
                        frm.Show();
                        this.Close();
                    }
                    catch (Exception es)
                    {

                        MessageBox.Show(es.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Please, select a type of information provided", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


            }
            else
            {
                MessageBox.Show("Please, select all the audios required", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void btn_search_image_Click(object sender, EventArgs e)
        {
            ofl = new OpenFileDialog();
            ofl.Filter = "PNG|*.png|JPG|*.jpg";
            if (ofl.ShowDialog() == DialogResult.OK)
            {
                filenames[3] = ofl.FileName.ToString();
                text_image = 1;
                texts = "";
                if(Classes.Vars.State_exercise == 1)
                {
                    lbl_done_info.Text = "Image - Changed!";
                }
                else
                {
                    lbl_done_info.Text = "Image - Done!";
                }
                lbl_done_info.Visible = true;
            }
        }

        private void btn_search1_Click(object sender, EventArgs e)
        {
            show_dialog(0,1);
        }

        private void btn_search2_Click(object sender, EventArgs e)
        {
            show_dialog(1,2);
        }

        private void btn_search3_Click(object sender, EventArgs e)
        {
            show_dialog(2,3);
        }

        private void show_dialog(int i, int label)
        {
            ofl.Filter = "Mp3|*.mp3|Wav|*.wav";
            ofl.Title = "Select an audio";
            if (ofl.ShowDialog() == DialogResult.OK)
            {
                filenames[i] = ofl.FileName.ToString();
                if(Classes.Vars.State_exercise == 1)
                {
                    lbl_done_1.Text = "Changed!";
                    lbl_done_2.Text = "Changed!";
                    lbl_done_3.Text = "Changed!";
                }
                switch (label)
                {
                    case 1: lbl_done_1.Visible = true;
                        break;
                    case 2: lbl_done_2.Visible = true;
                        break;
                    case 3: lbl_done_3.Visible = true;
                        break;
                }
            }
        }
                
        private void btn_play1_Click(object sender, EventArgs e)
        {
            play_audio(filenames[0]);
        }

        private void btn_play2_Click(object sender, EventArgs e)
        {
            play_audio(filenames[1]);
        }

        private void btn_play3_Click(object sender, EventArgs e)
        {
            play_audio(filenames[2]);
        }

        private void btn_stop1_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_stop2_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_stop3_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_insert_text_Click(object sender, EventArgs e)
        {
            show = 1;
            frm_addtext frm = new frm_addtext();
            frm.Owner = this;
            frm.ShowDialog();
        }

        private void play_audio(string source)
        {
            wmp.settings.volume = 100;
            try
            {
                if (!string.IsNullOrEmpty(source))
                {
                    //MessageBox.Show(source);
                    wmp.URL = source;
                    wmp.controls.play();
                }
                else
                {
                    MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void save_respond_provided()
        {
            string path;
            for (int i = 0; i < 3; i++)
            {
                path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_respond_provided_" + (i + 1) + "_" + Classes.Vars.Number_toeic + ".mp3";
                //MessageBox.Show(path);
                if (filenames[i] != path)
                {
                    File.Copy(filenames[i], path, true);
                }
            }

            Classes.SQLite sql = new Classes.SQLite();
            string temp;
            if (text_image == 1)
            {
                string path2 = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_info_provided_" + Classes.Vars.Number_toeic + ".png";
                temp = path2.Replace(".png", ".tryhard");
                if(File.Exists(temp))
                {
                    File.Delete(temp);
                }
                if (filenames[3] != path2)
                {
                    File.Copy(filenames[3], path2, true);
                }                
                sql.update_parts("provided", 1);
                if (sql.exercise_state("provided"))
                {
                    Classes.Vars.parts[3] = 1;
                }
            }
            else
            {
                if (text_image == 2)
                {
                    string path2 = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_info_provided_" + Classes.Vars.Number_toeic + ".tryhard";
                    temp = path2.Replace(".tryhard", ".png");
                    if(File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                    File.WriteAllText(path2, texts);
                    sql.update_parts("provided", 2);
                    if (sql.exercise_state("provided"))
                    {
                        Classes.Vars.parts[3] = 1;
                    }
                }
            }           
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if(btn_save.Text == "Modify")
            {
                fun.DisableControls(btn_search_image, 1);
                fun.DisableControls(btn_insert_text, 1);
                fun.DisableControls(btn_search1, 1);
                fun.DisableControls(btn_search2, 1);
                fun.DisableControls(btn_search3, 1);
                this.Controls.Remove(btn_save);
                btn_next.Text = "Finish";
            }
            else
            {
                if (!string.IsNullOrEmpty(filenames[0]) && !string.IsNullOrEmpty(filenames[1]) && !string.IsNullOrEmpty(filenames[2]))
                {
                    if (text_image != 0)
                    {
                        Classes.Functions.Path = @"C:\TryHard\Files\Respond_Provided";
                        fun.CreateFolder();
                        try
                        {
                            save_respond_provided();
                            Form frm = new frm_allparts();
                            frm.Show();
                            this.Close();
                        }
                        catch (Exception es)
                        {

                            MessageBox.Show(es.Message);
                        }
                    }
                    else
                    {
                        if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        {
                            Form frm = new frm_allparts();
                            frm.Show();
                            this.Close();
                        }
                    }


                }
                else
                {
                    if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                }
            }
        }

        private void btn_check_Click(object sender, EventArgs e)
        {
            show = 0;
            frm_addtext frm = new frm_addtext();
            frm.Owner = this;
            frm.ShowDialog();
        }
        //Metodo mostrar ventana para grabar
        private void add_exercise(int exercise, int n_exercise)
        {
            Form frm = new frm_record();
            frm.Owner = this;
            frm_record.exercise = exercise;
            frm_record.number_exercise = n_exercise;
            frm.ShowDialog();
        }

        private void btn_record_1_Click(object sender, EventArgs e)
        {
            add_exercise(2, 1);
        }

        private void btn_record_2_Click(object sender, EventArgs e)
        {
            add_exercise(2, 2);
        }

        private void btn_record_3_Click(object sender, EventArgs e)
        {
            add_exercise(2, 3);
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }
    }
}

