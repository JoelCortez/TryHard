﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_proposesolution : Form
    {
        private Classes.Performance performance;

        public frm_proposesolution()
        {
            InitializeComponent();
        }

        private void frm_proposesolution_Load(object sender, EventArgs e)
        {
        }

        private void frm_proposesolution_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            ///lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\ReadingAloud\" + Classes.Vars.Provider + "_reading_" + 1 + "_" + Classes.Vars.Number_toeic + ".tryhard");
            ///Classes.Vars.label_timer.Text = "00:45";
            performance.propose_problem();
        }
    }
}
