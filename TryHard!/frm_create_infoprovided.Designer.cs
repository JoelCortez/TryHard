﻿namespace TryHard_
{
    partial class frm_create_infoprovided
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_create_infoprovided));
            this.btn_stop3 = new System.Windows.Forms.Button();
            this.btn_play3 = new System.Windows.Forms.Button();
            this.btn_search3 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_stop2 = new System.Windows.Forms.Button();
            this.btn_play2 = new System.Windows.Forms.Button();
            this.btn_search2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_stop1 = new System.Windows.Forms.Button();
            this.btn_play1 = new System.Windows.Forms.Button();
            this.btn_search1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_part = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_search_image = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_insert_text = new System.Windows.Forms.Button();
            this.lbl_number_toeic = new System.Windows.Forms.Label();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.lbl_done_1 = new System.Windows.Forms.Label();
            this.lbl_done_2 = new System.Windows.Forms.Label();
            this.lbl_done_3 = new System.Windows.Forms.Label();
            this.lbl_done_info = new System.Windows.Forms.Label();
            this.btn_check = new System.Windows.Forms.Button();
            this.btn_record_1 = new System.Windows.Forms.Button();
            this.btn_record_2 = new System.Windows.Forms.Button();
            this.btn_record_3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_stop3
            // 
            this.btn_stop3.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop3.FlatAppearance.BorderSize = 0;
            this.btn_stop3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop3.Location = new System.Drawing.Point(542, 266);
            this.btn_stop3.Name = "btn_stop3";
            this.btn_stop3.Size = new System.Drawing.Size(20, 20);
            this.btn_stop3.TabIndex = 102;
            this.btn_stop3.UseVisualStyleBackColor = true;
            this.btn_stop3.Click += new System.EventHandler(this.btn_stop3_Click);
            // 
            // btn_play3
            // 
            this.btn_play3.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play3.FlatAppearance.BorderSize = 0;
            this.btn_play3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play3.Location = new System.Drawing.Point(515, 266);
            this.btn_play3.Name = "btn_play3";
            this.btn_play3.Size = new System.Drawing.Size(20, 20);
            this.btn_play3.TabIndex = 101;
            this.btn_play3.UseVisualStyleBackColor = true;
            this.btn_play3.Click += new System.EventHandler(this.btn_play3_Click);
            // 
            // btn_search3
            // 
            this.btn_search3.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search3.FlatAppearance.BorderSize = 0;
            this.btn_search3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search3.Location = new System.Drawing.Point(635, 239);
            this.btn_search3.Name = "btn_search3";
            this.btn_search3.Size = new System.Drawing.Size(20, 20);
            this.btn_search3.TabIndex = 98;
            this.btn_search3.UseVisualStyleBackColor = true;
            this.btn_search3.Click += new System.EventHandler(this.btn_search3_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Calibri", 11F);
            this.label8.Location = new System.Drawing.Point(486, 239);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 18);
            this.label8.TabIndex = 97;
            this.label8.Text = "Please select Audio 3:";
            // 
            // btn_stop2
            // 
            this.btn_stop2.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop2.FlatAppearance.BorderSize = 0;
            this.btn_stop2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop2.Location = new System.Drawing.Point(320, 266);
            this.btn_stop2.Name = "btn_stop2";
            this.btn_stop2.Size = new System.Drawing.Size(20, 20);
            this.btn_stop2.TabIndex = 96;
            this.btn_stop2.UseVisualStyleBackColor = true;
            this.btn_stop2.Click += new System.EventHandler(this.btn_stop2_Click);
            // 
            // btn_play2
            // 
            this.btn_play2.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play2.FlatAppearance.BorderSize = 0;
            this.btn_play2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play2.Location = new System.Drawing.Point(293, 266);
            this.btn_play2.Name = "btn_play2";
            this.btn_play2.Size = new System.Drawing.Size(20, 20);
            this.btn_play2.TabIndex = 95;
            this.btn_play2.UseVisualStyleBackColor = true;
            this.btn_play2.Click += new System.EventHandler(this.btn_play2_Click);
            // 
            // btn_search2
            // 
            this.btn_search2.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search2.FlatAppearance.BorderSize = 0;
            this.btn_search2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search2.Location = new System.Drawing.Point(413, 239);
            this.btn_search2.Name = "btn_search2";
            this.btn_search2.Size = new System.Drawing.Size(20, 20);
            this.btn_search2.TabIndex = 92;
            this.btn_search2.UseVisualStyleBackColor = true;
            this.btn_search2.Click += new System.EventHandler(this.btn_search2_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Calibri", 11F);
            this.label6.Location = new System.Drawing.Point(264, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 18);
            this.label6.TabIndex = 91;
            this.label6.Text = "Please select Audio 2:";
            // 
            // btn_stop1
            // 
            this.btn_stop1.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop1.FlatAppearance.BorderSize = 0;
            this.btn_stop1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop1.Location = new System.Drawing.Point(100, 266);
            this.btn_stop1.Name = "btn_stop1";
            this.btn_stop1.Size = new System.Drawing.Size(20, 20);
            this.btn_stop1.TabIndex = 90;
            this.btn_stop1.UseVisualStyleBackColor = true;
            this.btn_stop1.Click += new System.EventHandler(this.btn_stop1_Click);
            // 
            // btn_play1
            // 
            this.btn_play1.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play1.FlatAppearance.BorderSize = 0;
            this.btn_play1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play1.Location = new System.Drawing.Point(73, 266);
            this.btn_play1.Name = "btn_play1";
            this.btn_play1.Size = new System.Drawing.Size(20, 20);
            this.btn_play1.TabIndex = 89;
            this.btn_play1.UseVisualStyleBackColor = true;
            this.btn_play1.Click += new System.EventHandler(this.btn_play1_Click);
            // 
            // btn_search1
            // 
            this.btn_search1.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search1.FlatAppearance.BorderSize = 0;
            this.btn_search1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search1.Location = new System.Drawing.Point(193, 239);
            this.btn_search1.Name = "btn_search1";
            this.btn_search1.Size = new System.Drawing.Size(20, 20);
            this.btn_search1.TabIndex = 86;
            this.btn_search1.UseVisualStyleBackColor = true;
            this.btn_search1.Click += new System.EventHandler(this.btn_search1_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Calibri", 11F);
            this.label4.Location = new System.Drawing.Point(44, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 18);
            this.label4.TabIndex = 85;
            this.label4.Text = "Please select Audio 1:";
            // 
            // lbl_part
            // 
            this.lbl_part.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_part.AutoSize = true;
            this.lbl_part.BackColor = System.Drawing.Color.Transparent;
            this.lbl_part.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_part.Location = new System.Drawing.Point(462, 100);
            this.lbl_part.Name = "lbl_part";
            this.lbl_part.Size = new System.Drawing.Size(244, 17);
            this.lbl_part.TabIndex = 78;
            this.lbl_part.Text = "(Respond with information provided)";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(462, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 77;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(698, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 76;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(673, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 75;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 74;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(730, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 73;
            this.pbx_header.TabStop = false;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Calibri", 11F);
            this.label9.Location = new System.Drawing.Point(43, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 18);
            this.label9.TabIndex = 104;
            this.label9.Text = "Information provided:";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Calibri", 11F);
            this.label10.Location = new System.Drawing.Point(189, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 18);
            this.label10.TabIndex = 105;
            this.label10.Text = "Select Image:";
            // 
            // btn_search_image
            // 
            this.btn_search_image.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search_image.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search_image.FlatAppearance.BorderSize = 0;
            this.btn_search_image.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search_image.Location = new System.Drawing.Point(281, 172);
            this.btn_search_image.Name = "btn_search_image";
            this.btn_search_image.Size = new System.Drawing.Size(20, 20);
            this.btn_search_image.TabIndex = 106;
            this.btn_search_image.UseVisualStyleBackColor = true;
            this.btn_search_image.Click += new System.EventHandler(this.btn_search_image_Click);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Calibri", 11F);
            this.label11.Location = new System.Drawing.Point(303, 174);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 18);
            this.label11.TabIndex = 107;
            this.label11.Text = "/";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Calibri", 11F);
            this.label12.Location = new System.Drawing.Point(314, 174);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 18);
            this.label12.TabIndex = 108;
            this.label12.Text = "Text:";
            // 
            // btn_insert_text
            // 
            this.btn_insert_text.BackgroundImage = global::TryHard_.Properties.Resources.insert;
            this.btn_insert_text.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_insert_text.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_insert_text.FlatAppearance.BorderSize = 0;
            this.btn_insert_text.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_insert_text.Location = new System.Drawing.Point(352, 173);
            this.btn_insert_text.Name = "btn_insert_text";
            this.btn_insert_text.Size = new System.Drawing.Size(20, 20);
            this.btn_insert_text.TabIndex = 109;
            this.btn_insert_text.UseVisualStyleBackColor = true;
            this.btn_insert_text.Click += new System.EventHandler(this.btn_insert_text_Click);
            // 
            // lbl_number_toeic
            // 
            this.lbl_number_toeic.AutoSize = true;
            this.lbl_number_toeic.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_toeic.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_number_toeic.Location = new System.Drawing.Point(20, 69);
            this.lbl_number_toeic.Name = "lbl_number_toeic";
            this.lbl_number_toeic.Size = new System.Drawing.Size(74, 21);
            this.lbl_number_toeic.TabIndex = 110;
            this.lbl_number_toeic.Text = "TOEIC #";
            // 
            // btn_next
            // 
            this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next.BackColor = System.Drawing.Color.White;
            this.btn_next.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_next.ForeColor = System.Drawing.Color.White;
            this.btn_next.Location = new System.Drawing.Point(542, 390);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(84, 41);
            this.btn_next.TabIndex = 112;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_save.BackColor = System.Drawing.Color.White;
            this.btn_save.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_save.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.Color.White;
            this.btn_save.Location = new System.Drawing.Point(447, 390);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(84, 41);
            this.btn_save.TabIndex = 111;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // lbl_done_1
            // 
            this.lbl_done_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_1.AutoSize = true;
            this.lbl_done_1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_1.Location = new System.Drawing.Point(129, 268);
            this.lbl_done_1.Name = "lbl_done_1";
            this.lbl_done_1.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_1.TabIndex = 117;
            this.lbl_done_1.Text = "Done!";
            this.lbl_done_1.Visible = false;
            // 
            // lbl_done_2
            // 
            this.lbl_done_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_2.AutoSize = true;
            this.lbl_done_2.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_2.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_2.Location = new System.Drawing.Point(351, 268);
            this.lbl_done_2.Name = "lbl_done_2";
            this.lbl_done_2.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_2.TabIndex = 118;
            this.lbl_done_2.Text = "Done!";
            this.lbl_done_2.Visible = false;
            // 
            // lbl_done_3
            // 
            this.lbl_done_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_3.AutoSize = true;
            this.lbl_done_3.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_3.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_3.Location = new System.Drawing.Point(572, 267);
            this.lbl_done_3.Name = "lbl_done_3";
            this.lbl_done_3.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_3.TabIndex = 119;
            this.lbl_done_3.Text = "Done!";
            this.lbl_done_3.Visible = false;
            // 
            // lbl_done_info
            // 
            this.lbl_done_info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done_info.AutoSize = true;
            this.lbl_done_info.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done_info.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done_info.Location = new System.Drawing.Point(43, 198);
            this.lbl_done_info.Name = "lbl_done_info";
            this.lbl_done_info.Size = new System.Drawing.Size(43, 17);
            this.lbl_done_info.TabIndex = 120;
            this.lbl_done_info.Text = "Done!";
            this.lbl_done_info.Visible = false;
            // 
            // btn_check
            // 
            this.btn_check.BackgroundImage = global::TryHard_.Properties.Resources.check;
            this.btn_check.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_check.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_check.FlatAppearance.BorderSize = 0;
            this.btn_check.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_check.Location = new System.Drawing.Point(386, 174);
            this.btn_check.Name = "btn_check";
            this.btn_check.Size = new System.Drawing.Size(19, 19);
            this.btn_check.TabIndex = 121;
            this.btn_check.UseVisualStyleBackColor = true;
            this.btn_check.Visible = false;
            this.btn_check.Click += new System.EventHandler(this.btn_check_Click);
            // 
            // btn_record_1
            // 
            this.btn_record_1.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record_1.FlatAppearance.BorderSize = 0;
            this.btn_record_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record_1.Location = new System.Drawing.Point(50, 266);
            this.btn_record_1.Name = "btn_record_1";
            this.btn_record_1.Size = new System.Drawing.Size(21, 22);
            this.btn_record_1.TabIndex = 122;
            this.btn_record_1.UseVisualStyleBackColor = true;
            this.btn_record_1.Click += new System.EventHandler(this.btn_record_1_Click);
            // 
            // btn_record_2
            // 
            this.btn_record_2.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record_2.FlatAppearance.BorderSize = 0;
            this.btn_record_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record_2.Location = new System.Drawing.Point(270, 266);
            this.btn_record_2.Name = "btn_record_2";
            this.btn_record_2.Size = new System.Drawing.Size(21, 22);
            this.btn_record_2.TabIndex = 123;
            this.btn_record_2.UseVisualStyleBackColor = true;
            this.btn_record_2.Click += new System.EventHandler(this.btn_record_2_Click);
            // 
            // btn_record_3
            // 
            this.btn_record_3.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record_3.FlatAppearance.BorderSize = 0;
            this.btn_record_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record_3.Location = new System.Drawing.Point(492, 266);
            this.btn_record_3.Name = "btn_record_3";
            this.btn_record_3.Size = new System.Drawing.Size(21, 22);
            this.btn_record_3.TabIndex = 124;
            this.btn_record_3.UseVisualStyleBackColor = true;
            this.btn_record_3.Click += new System.EventHandler(this.btn_record_3_Click);
            // 
            // frm_create_infoprovided
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(730, 450);
            this.Controls.Add(this.btn_record_3);
            this.Controls.Add(this.btn_record_2);
            this.Controls.Add(this.btn_record_1);
            this.Controls.Add(this.btn_check);
            this.Controls.Add(this.lbl_done_info);
            this.Controls.Add(this.lbl_done_3);
            this.Controls.Add(this.lbl_done_2);
            this.Controls.Add(this.lbl_done_1);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.lbl_number_toeic);
            this.Controls.Add(this.btn_insert_text);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btn_search_image);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btn_stop3);
            this.Controls.Add(this.btn_play3);
            this.Controls.Add(this.btn_search3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btn_stop2);
            this.Controls.Add(this.btn_play2);
            this.Controls.Add(this.btn_search2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_stop1);
            this.Controls.Add(this.btn_play1);
            this.Controls.Add(this.btn_search1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_part);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_create_infoprovided";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_create_infoprovided_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_stop3;
        private System.Windows.Forms.Button btn_play3;
        private System.Windows.Forms.Button btn_search3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_stop2;
        private System.Windows.Forms.Button btn_play2;
        private System.Windows.Forms.Button btn_search2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_stop1;
        private System.Windows.Forms.Button btn_play1;
        private System.Windows.Forms.Button btn_search1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_part;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_search_image;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_insert_text;
        private System.Windows.Forms.Label lbl_number_toeic;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label lbl_done_1;
        private System.Windows.Forms.Label lbl_done_2;
        private System.Windows.Forms.Label lbl_done_3;
        private System.Windows.Forms.Label lbl_done_info;
        private System.Windows.Forms.Button btn_check;
        private System.Windows.Forms.Button btn_record_1;
        private System.Windows.Forms.Button btn_record_2;
        private System.Windows.Forms.Button btn_record_3;
    }
}