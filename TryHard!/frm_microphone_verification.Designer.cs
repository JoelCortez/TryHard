﻿namespace TryHard_
{
    partial class frm_microphone_verification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_microphone_verification));
            this.lbl_timer = new System.Windows.Forms.Label();
            this.lbl_id = new System.Windows.Forms.Label();
            this.btn_continue = new System.Windows.Forms.Button();
            this.btn_record = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_play = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_timer
            // 
            this.lbl_timer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_timer.AutoSize = true;
            this.lbl_timer.BackColor = System.Drawing.Color.Transparent;
            this.lbl_timer.Font = new System.Drawing.Font("Calibri", 26F);
            this.lbl_timer.Location = new System.Drawing.Point(301, 265);
            this.lbl_timer.Name = "lbl_timer";
            this.lbl_timer.Size = new System.Drawing.Size(99, 42);
            this.lbl_timer.TabIndex = 126;
            this.lbl_timer.Text = "00:00";
            // 
            // lbl_id
            // 
            this.lbl_id.BackColor = System.Drawing.Color.Transparent;
            this.lbl_id.Font = new System.Drawing.Font("Calibri", 15F);
            this.lbl_id.Location = new System.Drawing.Point(38, 32);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(650, 203);
            this.lbl_id.TabIndex = 130;
            this.lbl_id.Text = resources.GetString("lbl_id.Text");
            // 
            // btn_continue
            // 
            this.btn_continue.BackColor = System.Drawing.Color.White;
            this.btn_continue.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_continue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_continue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_continue.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_continue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_continue.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_continue.ForeColor = System.Drawing.Color.White;
            this.btn_continue.Location = new System.Drawing.Point(537, 338);
            this.btn_continue.Name = "btn_continue";
            this.btn_continue.Size = new System.Drawing.Size(124, 41);
            this.btn_continue.TabIndex = 165;
            this.btn_continue.Text = "Continue";
            this.btn_continue.UseVisualStyleBackColor = false;
            this.btn_continue.Click += new System.EventHandler(this.btn_continue_Click);
            // 
            // btn_record
            // 
            this.btn_record.BackColor = System.Drawing.Color.White;
            this.btn_record.BackgroundImage = global::TryHard_.Properties.Resources.record2;
            this.btn_record.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record.FlatAppearance.BorderSize = 0;
            this.btn_record.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record.Location = new System.Drawing.Point(249, 265);
            this.btn_record.Name = "btn_record";
            this.btn_record.Size = new System.Drawing.Size(40, 40);
            this.btn_record.TabIndex = 129;
            this.btn_record.UseVisualStyleBackColor = false;
            this.btn_record.Click += new System.EventHandler(this.btn_record_Click);
            // 
            // btn_stop
            // 
            this.btn_stop.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop.FlatAppearance.BorderSize = 0;
            this.btn_stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop.Location = new System.Drawing.Point(406, 271);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(30, 30);
            this.btn_stop.TabIndex = 128;
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Visible = false;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_play
            // 
            this.btn_play.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play.FlatAppearance.BorderSize = 0;
            this.btn_play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play.Location = new System.Drawing.Point(406, 271);
            this.btn_play.Name = "btn_play";
            this.btn_play.Size = new System.Drawing.Size(30, 30);
            this.btn_play.TabIndex = 127;
            this.btn_play.UseVisualStyleBackColor = true;
            this.btn_play.Visible = false;
            this.btn_play.Click += new System.EventHandler(this.btn_play_Click);
            // 
            // frm_microphone_verification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(700, 400);
            this.Controls.Add(this.btn_continue);
            this.Controls.Add(this.lbl_id);
            this.Controls.Add(this.btn_record);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.btn_play);
            this.Controls.Add(this.lbl_timer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_microphone_verification";
            this.Text = "frm_microphone_verification";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_record;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btn_play;
        private System.Windows.Forms.Label lbl_timer;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Button btn_continue;
    }
}