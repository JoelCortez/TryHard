﻿namespace TryHard_
{
    partial class frm_basedpicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_words = new System.Windows.Forms.Label();
            this.lbl_number_sentence = new System.Windows.Forms.Label();
            this.txt_sentences = new System.Windows.Forms.TextBox();
            this.pbx_picture = new System.Windows.Forms.PictureBox();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_back = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_picture)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_words
            // 
            this.lbl_words.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_words.BackColor = System.Drawing.Color.Transparent;
            this.lbl_words.Font = new System.Drawing.Font("Calibri", 14F);
            this.lbl_words.Location = new System.Drawing.Point(0, 247);
            this.lbl_words.Name = "lbl_words";
            this.lbl_words.Size = new System.Drawing.Size(700, 26);
            this.lbl_words.TabIndex = 32;
            this.lbl_words.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_number_sentence
            // 
            this.lbl_number_sentence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_number_sentence.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_sentence.Font = new System.Drawing.Font("Calibri", 14F);
            this.lbl_number_sentence.Location = new System.Drawing.Point(0, 0);
            this.lbl_number_sentence.Name = "lbl_number_sentence";
            this.lbl_number_sentence.Size = new System.Drawing.Size(700, 26);
            this.lbl_number_sentence.TabIndex = 33;
            this.lbl_number_sentence.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_sentences
            // 
            this.txt_sentences.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_sentences.BackColor = System.Drawing.Color.White;
            this.txt_sentences.Font = new System.Drawing.Font("Cambria", 13F);
            this.txt_sentences.ForeColor = System.Drawing.Color.DimGray;
            this.txt_sentences.Location = new System.Drawing.Point(25, 279);
            this.txt_sentences.Multiline = true;
            this.txt_sentences.Name = "txt_sentences";
            this.txt_sentences.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_sentences.Size = new System.Drawing.Size(650, 63);
            this.txt_sentences.TabIndex = 52;
            this.txt_sentences.UseSystemPasswordChar = true;
            // 
            // pbx_picture
            // 
            this.pbx_picture.Location = new System.Drawing.Point(195, 45);
            this.pbx_picture.Name = "pbx_picture";
            this.pbx_picture.Size = new System.Drawing.Size(311, 198);
            this.pbx_picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_picture.TabIndex = 76;
            this.pbx_picture.TabStop = false;
            // 
            // btn_next
            // 
            this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next.BackColor = System.Drawing.Color.White;
            this.btn_next.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_next.ForeColor = System.Drawing.Color.White;
            this.btn_next.Location = new System.Drawing.Point(594, 351);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(84, 41);
            this.btn_next.TabIndex = 75;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_back
            // 
            this.btn_back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_back.BackColor = System.Drawing.Color.White;
            this.btn_back.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_back.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_back.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_back.ForeColor = System.Drawing.Color.White;
            this.btn_back.Location = new System.Drawing.Point(502, 351);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(84, 41);
            this.btn_back.TabIndex = 74;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = false;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // frm_basedpicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(700, 400);
            this.Controls.Add(this.pbx_picture);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.txt_sentences);
            this.Controls.Add(this.lbl_number_sentence);
            this.Controls.Add(this.lbl_words);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_basedpicture";
            this.Load += new System.EventHandler(this.frm_basedpicture_Load);
            this.Shown += new System.EventHandler(this.frm_basedpicture_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_picture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_words;
        private System.Windows.Forms.Label lbl_number_sentence;
        private System.Windows.Forms.TextBox txt_sentences;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.PictureBox pbx_picture;
    }
}