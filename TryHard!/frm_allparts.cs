﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_allparts : Form
    {
        Form frm;
        Classes.SQLite sql = new Classes.SQLite();
        private int total_1, total_2, steps;  

        public frm_allparts()
        {
            InitializeComponent();
        }

        private void frm_allparts_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(Classes.Vars.Editing.ToString());
            lbl_name.Text = Classes.Vars.FullName;
            steps = 1;
            //btn_1.MouseDoubleClick
            //pgb_verify_files.PerformStep();
            start_loading();
            Thread t = new Thread(() =>
            {
                Classes.Threads thr = new Classes.Threads();
                if (Classes.Vars.Editing == 0)
                {
                    //MessageBox.Show(Classes.Vars.Editing.ToString());
                    if (thr.verify_files_exist(Classes.Vars.ID))
                    {
                        //MessageBox.Show("Hey");
                        for (int i = 0; i < 9; i++)
                        {
                            Classes.Vars.parts[i] = 0;
                        }
                        //MessageBox.Show("Completed");
                        this.Invoke(new MethodInvoker(delegate { pnl_verify_files.Visible = false; }));
                    }
                    else
                    {
                        /*for (int i = 0; i < 9; i++)
                        {
                            MessageBox.Show((i + 1).ToString() + " False- " + Classes.Vars.parts[i].ToString());
                        }*/
                        //MessageBox.Show("Incompleted");
                        this.Invoke(new MethodInvoker(delegate {
                            pgb_verify_files.Visible = false;
                            lbl_information.Visible = false;
                            lbl_warning.Visible = true;
                            btn_continue.Visible = true;
                        }));
                    }
                }
                else
                {
                    this.Invoke(new MethodInvoker(delegate { pnl_verify_files.Visible = false; }));
                }

            });
            if (Classes.Vars.Editing == 0)
            {
                //MessageBox.Show("To verify");
                t.Start();
            }
            else
            {
                /*for (int i = 0; i < 9; i++)
                {
                    MessageBox.Show((i + 1).ToString() + " All- " + Classes.Vars.parts[i].ToString());
                }*/
                pnl_verify_files.Visible = false;
            }
        }

        private void start_loading()
        {
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
            DataTable table = sql.show_datas_speaking(Classes.Vars.ID);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                for(int j = 0; j < table.Columns.Count; j++)
                {                 
                    if (table.Rows[i][j].ToString() != "0")
                    {
                        this.Controls["btn_" + (j + 1)].BackgroundImage = Properties.Resources.correct;
                        this.Controls["btn_" + (j + 1)].Tag = 1;
                        this.Controls["lbl_" + (j + 1)].Tag = 1;
                        if(Classes.Vars.Editing == 0)
                        {
                            Classes.Vars.parts[j] = 1;
                        }
                    }                    
                }
            }
            table = sql.show_datas_writing(Classes.Vars.ID);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (table.Rows[i][j].ToString() != "0")
                    {
                        this.Controls["btn_" + (j + 7)].BackgroundImage = Properties.Resources.correct;
                        this.Controls["btn_" + (j + 7)].Tag = 1;
                        this.Controls["lbl_" + (j + 7)].Tag = 1;
                        if (Classes.Vars.Editing == 0)
                        {
                            Classes.Vars.parts[j + 6] = 1;
                        }                        
                    }
                }
            }
            Classes.Vars.State_exercise = 1;
            Classes.Vars.Part_selection = 0;
            add_double_clic_events();
        }


        private void add_double_clic_events()
        {
            for (int i = 1; i < 10; i++)
            {
                if (this.Controls["lbl_" + i.ToString()].Tag.ToString() != "1")
                {
                    this.Controls["lbl_" + i.ToString()].Cursor = Cursors.Hand;
                    this.Controls["lbl_" + i.ToString()].MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(double_click);
                }
            }
        }

        private int add_steps_progress()
        {
            int pgb_step = 0;
            for (int i = 0; i < 9; i++)
            {
                if (Classes.Vars.parts[i] != 0)
                {
                    switch (i)
                    {
                        case 0:
                            pgb_step += 2;
                            break;
                        case 2:
                            pgb_step += 3;
                            break;
                        case 3:
                            pgb_step += 3;
                            break;
                        case 6:
                            pgb_step += 5;
                            break;
                        case 7:
                            pgb_step += 2;
                            break;
                        default:
                            pgb_step++;
                            break;
                    }
                    steps += 1;
                }
            }
            if (Classes.Vars.State != 1)
            {
                steps += 1;
            }
            this.Invoke(new MethodInvoker(delegate { pgb_verify_files.Step = 100 / pgb_step; }));//(steps + 1); }));
            return steps;
            
        }

        private void double_click(object sender, EventArgs e)
        {
            //MessageBox.Show(((Label)sender).Name.ToString());
            Classes.Vars.State_exercise = 2;
            int part = int.Parse(((Label)sender).Name.ToString().Replace("lbl_", ""));
            assign_part(part);
        }

        private void assign_part(int state)
        {
            frm = new Form();
            switch (state)
                {
                    case 1:
                        Classes.Vars.Part_selection = 4;
                    frm = new frm_create_readaloud();
                break;
                    case 2:
                        Classes.Vars.Part_selection = 5;
                    frm = new frm_create_describepicture();
                break;
                    case 3:
                        Classes.Vars.Part_selection = 6;
                    frm = new frm_create_respondquestions();
                break;
                    case 4:
                        Classes.Vars.Part_selection = 7;
                    frm = new frm_create_infoprovided();
                break;
                    case 5:
                        Classes.Vars.Part_selection = 8;
                    frm = new frm_create_proposesolution();
                break;
                    case 6:
                        Classes.Vars.Part_selection = 9;
                    frm = new frm_create_expressopinion();
                break;
                    case 7:
                        Classes.Vars.Part_selection = 10;
                    frm = new frm_create_basedonpicture();
                break;
                    case 8:
                        Classes.Vars.Part_selection = 11;
                    frm = new frm_create_writtenrequest();
                break;
                    case 9:
                        Classes.Vars.Part_selection = 12;
                    frm = new frm_create_opinionessay();
                break;
                }
            frm.Show();
            this.Close();
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            if(btn_1.Tag.ToString() == "1")
            {                
                frm = new frm_create_readaloud();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            if (btn_2.Tag.ToString() == "1")
            {
                frm = new frm_create_describepicture();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            if (btn_3.Tag.ToString() == "1")
            {
                frm = new frm_create_respondquestions();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            if (btn_4.Tag.ToString() == "1")
            {
                frm = new frm_create_infoprovided();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            if (btn_5.Tag.ToString() == "1")
            {
                frm = new frm_create_proposesolution();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            if (btn_6.Tag.ToString() == "1")
            {
                frm = new frm_create_expressopinion();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            if (btn_7.Tag.ToString() == "1")
            {
                frm = new frm_create_basedonpicture();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            if (btn_8.Tag.ToString() == "1")
            {
                frm = new frm_create_writtenrequest();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            if (btn_9.Tag.ToString() == "1")
            {
                frm = new frm_create_opinionessay();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("This exercise has not been done!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            Form frm = new frm_panel();
            Classes.Vars.form = frm;
            frm.Show();
            this.Close();
        }

        private void btn_upload_Click(object sender, EventArgs e)
        {
            check_numbers();
            if (total_1 > total_2)
            {
                if (MessageBox.Show("Press yes if you want to upload all the test; press No if just want to upload the last changes", "TryHard!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frm_Updownload.state = 1;
                }
                else
                {
                    frm_Updownload.state = 2;
                }                   
            }
            else
            {
                frm_Updownload.state = 1;
            }
            Classes.Vars.State_exercise = 0;
            Form frm = new frm_Updownload();
            frm.Show();
            this.Close();
        }

        DataTable table_speaking, table_writing;

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void btn_continue_Click(object sender, EventArgs e)
        {
            btn_continue.Enabled = false;
            lbl_information.Visible = true;
            pgb_verify_files.Visible = true;
            pgb_verify_files.Style = ProgressBarStyle.Continuous;
            Thread t = new Thread(()=> 
            {   
                Classes.Threads.label = lbl_information;
                Classes.Threads.progressbar = pgb_verify_files;
                Classes.Threads thr = new Classes.Threads();
                thr.Download_all_files(add_steps_progress(), Classes.Vars.ID, 9, 0);
                if(Classes.Vars.Editing == 0)
                {
                    if (thr.verify_files_exist(Classes.Vars.ID))
                    {
                        for (int i = 0; i < 9; i++)
                        {
                            Classes.Vars.parts[i] = 0;
                        }
                        //MessageBox.Show("Completed");
                        this.Invoke(new MethodInvoker(delegate { pnl_verify_files.Visible = false; }));
                    }
                    else
                    {
                        //MessageBox.Show("Incompleted");
                        this.Invoke(new MethodInvoker(delegate {
                            pgb_verify_files.Visible = false;
                            lbl_information.Visible = false;
                            lbl_warning.Visible = true;
                            btn_continue.Enabled = true;
                            btn_continue.Visible = true;
                        }));
                    }
                }
                else
                {
                    this.Invoke(new MethodInvoker(delegate { pnl_verify_files.Visible = false; }));
                }

            });
            t.Start();
            
        }

        private void check_numbers()
        {
            total_1 = 0;
            total_2 = 0;
            table_speaking = sql.show_datas_speaking(Classes.Vars.ID);
            for (int i = 0; i < table_speaking.Rows.Count; i++)
            {
                for (int j = 0; j < table_speaking.Columns.Count; j++)
                {
                    //MessageBox.Show(Classes.Vars.parts[j].ToString());
                    if (table_speaking.Rows[i][j].ToString() != "0" && Classes.Vars.parts[j].ToString() != "0")
                    {
                        //MessageBox.Show("Los dos");
                        total_1 += 1;
                    }
                    else if (Classes.Vars.parts[j].ToString() != "0")
                    {
                        //MessageBox.Show("Solo cambio");
                        total_2 += 1;
                    }

                }
            }
            table_writing = sql.show_datas_writing(Classes.Vars.ID);
            for (int i = 0; i < table_writing.Rows.Count; i++)
            {
                for (int j = 0; j < table_writing.Columns.Count; j++)
                {
                    //MessageBox.Show(Classes.Vars.parts[j + 6].ToString());
                    if (table_writing.Rows[i][j].ToString() != "0" && Classes.Vars.parts[j + 6].ToString() != "0")
                    {
                        total_1 += 1;
                    }
                    else if (Classes.Vars.parts[j + 6].ToString() != "0")
                    {
                        total_2 += 1;
                    }


                }
            }
        }
    }
}
