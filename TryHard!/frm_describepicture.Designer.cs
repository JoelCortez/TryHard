﻿namespace TryHard_
{
    partial class frm_describepicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbx_picture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_picture)).BeginInit();
            this.SuspendLayout();
            // 
            // pbx_picture
            // 
            this.pbx_picture.ErrorImage = null;
            this.pbx_picture.Location = new System.Drawing.Point(41, 37);
            this.pbx_picture.Name = "pbx_picture";
            this.pbx_picture.Size = new System.Drawing.Size(619, 326);
            this.pbx_picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_picture.TabIndex = 0;
            this.pbx_picture.TabStop = false;
            // 
            // frm_describepicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(700, 400);
            this.Controls.Add(this.pbx_picture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_describepicture";
            this.Load += new System.EventHandler(this.frm_describepicture_Load);
            this.Shown += new System.EventHandler(this.frm_describepicture_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_picture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbx_picture;
    }
}