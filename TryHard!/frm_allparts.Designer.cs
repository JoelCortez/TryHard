﻿namespace TryHard_
{
    partial class frm_allparts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_allparts));
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.lbl_6 = new System.Windows.Forms.Label();
            this.btn_6 = new System.Windows.Forms.Button();
            this.lbl_5 = new System.Windows.Forms.Label();
            this.btn_5 = new System.Windows.Forms.Button();
            this.lbl_4 = new System.Windows.Forms.Label();
            this.btn_4 = new System.Windows.Forms.Button();
            this.lbl_3 = new System.Windows.Forms.Label();
            this.btn_3 = new System.Windows.Forms.Button();
            this.lbl_2 = new System.Windows.Forms.Label();
            this.btn_2 = new System.Windows.Forms.Button();
            this.lbl_1 = new System.Windows.Forms.Label();
            this.btn_1 = new System.Windows.Forms.Button();
            this.lbl_9 = new System.Windows.Forms.Label();
            this.btn_9 = new System.Windows.Forms.Button();
            this.lbl_8 = new System.Windows.Forms.Label();
            this.btn_8 = new System.Windows.Forms.Button();
            this.lbl_7 = new System.Windows.Forms.Label();
            this.btn_7 = new System.Windows.Forms.Button();
            this.lbl_welcome = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_number_toeic = new System.Windows.Forms.Label();
            this.btn_upload = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.pgb_verify_files = new System.Windows.Forms.ProgressBar();
            this.lbl_warning = new System.Windows.Forms.Label();
            this.btn_continue = new System.Windows.Forms.Button();
            this.lbl_information = new System.Windows.Forms.Label();
            this.pnl_verify_files = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.pnl_verify_files.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(698, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 126;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(673, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 125;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 124;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(730, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 123;
            this.pbx_header.TabStop = false;
            // 
            // lbl_6
            // 
            this.lbl_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_6.AutoSize = true;
            this.lbl_6.BackColor = System.Drawing.Color.Transparent;
            this.lbl_6.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_6.ForeColor = System.Drawing.Color.Navy;
            this.lbl_6.Location = new System.Drawing.Point(517, 217);
            this.lbl_6.Name = "lbl_6";
            this.lbl_6.Size = new System.Drawing.Size(127, 16);
            this.lbl_6.TabIndex = 148;
            this.lbl_6.Tag = "0";
            this.lbl_6.Text = "Express an Opinion";
            // 
            // btn_6
            // 
            this.btn_6.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_6.FlatAppearance.BorderSize = 0;
            this.btn_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_6.Location = new System.Drawing.Point(486, 213);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(25, 25);
            this.btn_6.TabIndex = 147;
            this.btn_6.Tag = "0";
            this.btn_6.UseVisualStyleBackColor = true;
            this.btn_6.Click += new System.EventHandler(this.btn_6_Click);
            // 
            // lbl_5
            // 
            this.lbl_5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_5.AutoSize = true;
            this.lbl_5.BackColor = System.Drawing.Color.Transparent;
            this.lbl_5.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_5.ForeColor = System.Drawing.Color.Navy;
            this.lbl_5.Location = new System.Drawing.Point(307, 217);
            this.lbl_5.Name = "lbl_5";
            this.lbl_5.Size = new System.Drawing.Size(125, 16);
            this.lbl_5.TabIndex = 146;
            this.lbl_5.Tag = "0";
            this.lbl_5.Text = "Propose a Solution";
            // 
            // btn_5
            // 
            this.btn_5.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_5.FlatAppearance.BorderSize = 0;
            this.btn_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_5.Location = new System.Drawing.Point(276, 213);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(25, 25);
            this.btn_5.TabIndex = 145;
            this.btn_5.Tag = "0";
            this.btn_5.UseVisualStyleBackColor = true;
            this.btn_5.Click += new System.EventHandler(this.btn_5_Click);
            // 
            // lbl_4
            // 
            this.lbl_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_4.BackColor = System.Drawing.Color.Transparent;
            this.lbl_4.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_4.ForeColor = System.Drawing.Color.Navy;
            this.lbl_4.Location = new System.Drawing.Point(92, 217);
            this.lbl_4.Name = "lbl_4";
            this.lbl_4.Size = new System.Drawing.Size(160, 52);
            this.lbl_4.TabIndex = 144;
            this.lbl_4.Tag = "0";
            this.lbl_4.Text = "Respond with Information Provided";
            // 
            // btn_4
            // 
            this.btn_4.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_4.FlatAppearance.BorderSize = 0;
            this.btn_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_4.Location = new System.Drawing.Point(61, 213);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(25, 25);
            this.btn_4.TabIndex = 143;
            this.btn_4.Tag = "0";
            this.btn_4.UseVisualStyleBackColor = true;
            this.btn_4.Click += new System.EventHandler(this.btn_4_Click);
            // 
            // lbl_3
            // 
            this.lbl_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_3.AutoSize = true;
            this.lbl_3.BackColor = System.Drawing.Color.Transparent;
            this.lbl_3.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_3.ForeColor = System.Drawing.Color.Navy;
            this.lbl_3.Location = new System.Drawing.Point(517, 155);
            this.lbl_3.Name = "lbl_3";
            this.lbl_3.Size = new System.Drawing.Size(146, 16);
            this.lbl_3.TabIndex = 142;
            this.lbl_3.Tag = "0";
            this.lbl_3.Text = "Respond To Questions";
            // 
            // btn_3
            // 
            this.btn_3.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_3.FlatAppearance.BorderSize = 0;
            this.btn_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_3.Location = new System.Drawing.Point(486, 151);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(25, 25);
            this.btn_3.TabIndex = 141;
            this.btn_3.Tag = "0";
            this.btn_3.UseVisualStyleBackColor = true;
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
            // 
            // lbl_2
            // 
            this.lbl_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_2.AutoSize = true;
            this.lbl_2.BackColor = System.Drawing.Color.Transparent;
            this.lbl_2.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_2.ForeColor = System.Drawing.Color.Navy;
            this.lbl_2.Location = new System.Drawing.Point(307, 155);
            this.lbl_2.Name = "lbl_2";
            this.lbl_2.Size = new System.Drawing.Size(122, 16);
            this.lbl_2.TabIndex = 140;
            this.lbl_2.Tag = "0";
            this.lbl_2.Text = "Describe a Picture";
            // 
            // btn_2
            // 
            this.btn_2.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_2.FlatAppearance.BorderSize = 0;
            this.btn_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_2.Location = new System.Drawing.Point(276, 151);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(25, 25);
            this.btn_2.TabIndex = 139;
            this.btn_2.Tag = "0";
            this.btn_2.UseVisualStyleBackColor = true;
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
            // 
            // lbl_1
            // 
            this.lbl_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_1.AutoSize = true;
            this.lbl_1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_1.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_1.ForeColor = System.Drawing.Color.Navy;
            this.lbl_1.Location = new System.Drawing.Point(92, 155);
            this.lbl_1.Name = "lbl_1";
            this.lbl_1.Size = new System.Drawing.Size(122, 16);
            this.lbl_1.TabIndex = 138;
            this.lbl_1.Tag = "0";
            this.lbl_1.Text = "Reading Aloud 1-2";
            // 
            // btn_1
            // 
            this.btn_1.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_1.FlatAppearance.BorderSize = 0;
            this.btn_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_1.Location = new System.Drawing.Point(61, 151);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(25, 25);
            this.btn_1.TabIndex = 137;
            this.btn_1.Tag = "0";
            this.btn_1.UseVisualStyleBackColor = true;
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
            // 
            // lbl_9
            // 
            this.lbl_9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_9.AutoSize = true;
            this.lbl_9.BackColor = System.Drawing.Color.Transparent;
            this.lbl_9.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_9.ForeColor = System.Drawing.Color.Navy;
            this.lbl_9.Location = new System.Drawing.Point(517, 318);
            this.lbl_9.Name = "lbl_9";
            this.lbl_9.Size = new System.Drawing.Size(95, 16);
            this.lbl_9.TabIndex = 155;
            this.lbl_9.Tag = "0";
            this.lbl_9.Text = "Opinion Essay";
            // 
            // btn_9
            // 
            this.btn_9.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_9.FlatAppearance.BorderSize = 0;
            this.btn_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_9.Location = new System.Drawing.Point(486, 314);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(25, 25);
            this.btn_9.TabIndex = 154;
            this.btn_9.Tag = "0";
            this.btn_9.UseVisualStyleBackColor = true;
            this.btn_9.Click += new System.EventHandler(this.btn_9_Click);
            // 
            // lbl_8
            // 
            this.lbl_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_8.AutoSize = true;
            this.lbl_8.BackColor = System.Drawing.Color.Transparent;
            this.lbl_8.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_8.ForeColor = System.Drawing.Color.Navy;
            this.lbl_8.Location = new System.Drawing.Point(307, 318);
            this.lbl_8.Name = "lbl_8";
            this.lbl_8.Size = new System.Drawing.Size(109, 16);
            this.lbl_8.TabIndex = 153;
            this.lbl_8.Tag = "0";
            this.lbl_8.Text = "Written Request";
            // 
            // btn_8
            // 
            this.btn_8.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_8.FlatAppearance.BorderSize = 0;
            this.btn_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_8.Location = new System.Drawing.Point(276, 314);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(25, 25);
            this.btn_8.TabIndex = 152;
            this.btn_8.Tag = "0";
            this.btn_8.UseVisualStyleBackColor = true;
            this.btn_8.Click += new System.EventHandler(this.btn_8_Click);
            // 
            // lbl_7
            // 
            this.lbl_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_7.AutoSize = true;
            this.lbl_7.BackColor = System.Drawing.Color.Transparent;
            this.lbl_7.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_7.ForeColor = System.Drawing.Color.Navy;
            this.lbl_7.Location = new System.Drawing.Point(92, 318);
            this.lbl_7.Name = "lbl_7";
            this.lbl_7.Size = new System.Drawing.Size(123, 16);
            this.lbl_7.TabIndex = 151;
            this.lbl_7.Tag = "0";
            this.lbl_7.Text = "Based on a Picture";
            // 
            // btn_7
            // 
            this.btn_7.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.btn_7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_7.FlatAppearance.BorderSize = 0;
            this.btn_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_7.Location = new System.Drawing.Point(61, 314);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(25, 25);
            this.btn_7.TabIndex = 150;
            this.btn_7.Tag = "0";
            this.btn_7.UseVisualStyleBackColor = true;
            this.btn_7.Click += new System.EventHandler(this.btn_7_Click);
            // 
            // lbl_welcome
            // 
            this.lbl_welcome.AutoSize = true;
            this.lbl_welcome.BackColor = System.Drawing.Color.Transparent;
            this.lbl_welcome.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.lbl_welcome.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbl_welcome.Location = new System.Drawing.Point(57, 108);
            this.lbl_welcome.Name = "lbl_welcome";
            this.lbl_welcome.Size = new System.Drawing.Size(87, 21);
            this.lbl_welcome.TabIndex = 156;
            this.lbl_welcome.Text = "Speaking";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label10.Location = new System.Drawing.Point(57, 275);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 21);
            this.label10.TabIndex = 157;
            this.label10.Text = "Writing";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(462, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 158;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // lbl_number_toeic
            // 
            this.lbl_number_toeic.AutoSize = true;
            this.lbl_number_toeic.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_toeic.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_number_toeic.Location = new System.Drawing.Point(20, 69);
            this.lbl_number_toeic.Name = "lbl_number_toeic";
            this.lbl_number_toeic.Size = new System.Drawing.Size(74, 21);
            this.lbl_number_toeic.TabIndex = 159;
            this.lbl_number_toeic.Text = "TOEIC #";
            // 
            // btn_upload
            // 
            this.btn_upload.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_upload.BackgroundImage")));
            this.btn_upload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_upload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_upload.FlatAppearance.BorderSize = 0;
            this.btn_upload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_upload.Location = new System.Drawing.Point(670, 346);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(30, 30);
            this.btn_upload.TabIndex = 160;
            this.btn_upload.Tag = "0";
            this.btn_upload.UseVisualStyleBackColor = true;
            this.btn_upload.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(619, 357);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 16);
            this.label11.TabIndex = 161;
            this.label11.Text = "Upload";
            // 
            // pgb_verify_files
            // 
            this.pgb_verify_files.Location = new System.Drawing.Point(94, 211);
            this.pgb_verify_files.MarqueeAnimationSpeed = 1;
            this.pgb_verify_files.Name = "pgb_verify_files";
            this.pgb_verify_files.Size = new System.Drawing.Size(473, 31);
            this.pgb_verify_files.Step = 1;
            this.pgb_verify_files.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pgb_verify_files.TabIndex = 0;
            // 
            // lbl_warning
            // 
            this.lbl_warning.AutoSize = true;
            this.lbl_warning.BackColor = System.Drawing.Color.Transparent;
            this.lbl_warning.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_warning.Location = new System.Drawing.Point(68, 48);
            this.lbl_warning.Name = "lbl_warning";
            this.lbl_warning.Size = new System.Drawing.Size(515, 21);
            this.lbl_warning.TabIndex = 163;
            this.lbl_warning.Text = "To modify and see the exercises, you should download them first.";
            this.lbl_warning.Visible = false;
            // 
            // btn_continue
            // 
            this.btn_continue.BackColor = System.Drawing.Color.White;
            this.btn_continue.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_continue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_continue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_continue.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_continue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_continue.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_continue.ForeColor = System.Drawing.Color.White;
            this.btn_continue.Location = new System.Drawing.Point(268, 102);
            this.btn_continue.Name = "btn_continue";
            this.btn_continue.Size = new System.Drawing.Size(124, 41);
            this.btn_continue.TabIndex = 164;
            this.btn_continue.Text = "Continue";
            this.btn_continue.UseVisualStyleBackColor = false;
            this.btn_continue.Visible = false;
            this.btn_continue.Click += new System.EventHandler(this.btn_continue_Click);
            // 
            // lbl_information
            // 
            this.lbl_information.AutoSize = true;
            this.lbl_information.BackColor = System.Drawing.Color.Transparent;
            this.lbl_information.Font = new System.Drawing.Font("Cambria", 12F);
            this.lbl_information.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbl_information.Location = new System.Drawing.Point(95, 179);
            this.lbl_information.Name = "lbl_information";
            this.lbl_information.Size = new System.Drawing.Size(197, 19);
            this.lbl_information.TabIndex = 165;
            this.lbl_information.Text = "Verifying files ... Please wait";
            // 
            // pnl_verify_files
            // 
            this.pnl_verify_files.Controls.Add(this.lbl_information);
            this.pnl_verify_files.Controls.Add(this.btn_continue);
            this.pnl_verify_files.Controls.Add(this.lbl_warning);
            this.pnl_verify_files.Controls.Add(this.pgb_verify_files);
            this.pnl_verify_files.Location = new System.Drawing.Point(40, 107);
            this.pnl_verify_files.Name = "pnl_verify_files";
            this.pnl_verify_files.Size = new System.Drawing.Size(660, 275);
            this.pnl_verify_files.TabIndex = 162;
            // 
            // frm_allparts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(730, 394);
            this.Controls.Add(this.pnl_verify_files);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btn_upload);
            this.Controls.Add(this.lbl_number_toeic);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbl_welcome);
            this.Controls.Add(this.lbl_9);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.lbl_8);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.lbl_7);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.lbl_6);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.lbl_5);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.lbl_4);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.lbl_3);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.lbl_2);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.lbl_1);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(730, 394);
            this.MinimumSize = new System.Drawing.Size(730, 394);
            this.Name = "frm_allparts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_allparts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.pnl_verify_files.ResumeLayout(false);
            this.pnl_verify_files.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Label lbl_6;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Label lbl_5;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Label lbl_4;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Label lbl_3;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Label lbl_2;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Label lbl_1;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Label lbl_9;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Label lbl_8;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Label lbl_7;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Label lbl_welcome;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_number_toeic;
        private System.Windows.Forms.Button btn_upload;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar pgb_verify_files;
        private System.Windows.Forms.Label lbl_warning;
        private System.Windows.Forms.Button btn_continue;
        private System.Windows.Forms.Label lbl_information;
        private System.Windows.Forms.Panel pnl_verify_files;
    }
}