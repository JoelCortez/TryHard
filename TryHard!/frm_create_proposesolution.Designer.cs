﻿namespace TryHard_
{
    partial class frm_create_proposesolution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_create_proposesolution));
            this.lbl_part = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_play = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.lbl_number_toeic = new System.Windows.Forms.Label();
            this.lbl_done = new System.Windows.Forms.Label();
            this.btn_record = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_part
            // 
            this.lbl_part.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_part.AutoSize = true;
            this.lbl_part.BackColor = System.Drawing.Color.Transparent;
            this.lbl_part.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_part.Location = new System.Drawing.Point(462, 100);
            this.lbl_part.Name = "lbl_part";
            this.lbl_part.Size = new System.Drawing.Size(137, 17);
            this.lbl_part.TabIndex = 78;
            this.lbl_part.Text = "(Propose a Solution)";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(462, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 77;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(698, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 76;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(673, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 75;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 74;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(730, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 73;
            this.pbx_header.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 11F);
            this.label2.Location = new System.Drawing.Point(46, 232);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 18);
            this.label2.TabIndex = 80;
            this.label2.Text = "Select Audio:";
            // 
            // btn_stop
            // 
            this.btn_stop.BackgroundImage = global::TryHard_.Properties.Resources.stop;
            this.btn_stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_stop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop.FlatAppearance.BorderSize = 0;
            this.btn_stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop.Location = new System.Drawing.Point(100, 262);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(20, 20);
            this.btn_stop.TabIndex = 83;
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_play
            // 
            this.btn_play.BackgroundImage = global::TryHard_.Properties.Resources.play;
            this.btn_play.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_play.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_play.FlatAppearance.BorderSize = 0;
            this.btn_play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play.Location = new System.Drawing.Point(73, 262);
            this.btn_play.Name = "btn_play";
            this.btn_play.Size = new System.Drawing.Size(20, 20);
            this.btn_play.TabIndex = 82;
            this.btn_play.UseVisualStyleBackColor = true;
            this.btn_play.Click += new System.EventHandler(this.btn_play_Click);
            // 
            // btn_search
            // 
            this.btn_search.BackgroundImage = global::TryHard_.Properties.Resources.search;
            this.btn_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_search.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search.FlatAppearance.BorderSize = 0;
            this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search.Location = new System.Drawing.Point(138, 232);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(20, 20);
            this.btn_search.TabIndex = 81;
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // btn_next
            // 
            this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next.BackColor = System.Drawing.Color.White;
            this.btn_next.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_next.ForeColor = System.Drawing.Color.White;
            this.btn_next.Location = new System.Drawing.Point(542, 390);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(84, 41);
            this.btn_next.TabIndex = 114;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_save.BackColor = System.Drawing.Color.White;
            this.btn_save.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_save.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.Color.White;
            this.btn_save.Location = new System.Drawing.Point(447, 390);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(84, 41);
            this.btn_save.TabIndex = 113;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // lbl_number_toeic
            // 
            this.lbl_number_toeic.AutoSize = true;
            this.lbl_number_toeic.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_toeic.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_number_toeic.Location = new System.Drawing.Point(20, 69);
            this.lbl_number_toeic.Name = "lbl_number_toeic";
            this.lbl_number_toeic.Size = new System.Drawing.Size(74, 21);
            this.lbl_number_toeic.TabIndex = 115;
            this.lbl_number_toeic.Text = "TOEIC #";
            // 
            // lbl_done
            // 
            this.lbl_done.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_done.AutoSize = true;
            this.lbl_done.BackColor = System.Drawing.Color.Transparent;
            this.lbl_done.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbl_done.Location = new System.Drawing.Point(46, 303);
            this.lbl_done.Name = "lbl_done";
            this.lbl_done.Size = new System.Drawing.Size(43, 17);
            this.lbl_done.TabIndex = 116;
            this.lbl_done.Text = "Done!";
            this.lbl_done.Visible = false;
            // 
            // btn_record
            // 
            this.btn_record.BackgroundImage = global::TryHard_.Properties.Resources.record;
            this.btn_record.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_record.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_record.FlatAppearance.BorderSize = 0;
            this.btn_record.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_record.Location = new System.Drawing.Point(46, 260);
            this.btn_record.Name = "btn_record";
            this.btn_record.Size = new System.Drawing.Size(21, 22);
            this.btn_record.TabIndex = 123;
            this.btn_record.UseVisualStyleBackColor = true;
            this.btn_record.Click += new System.EventHandler(this.btn_record_Click);
            // 
            // frm_create_proposesolution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(730, 450);
            this.Controls.Add(this.btn_record);
            this.Controls.Add(this.lbl_done);
            this.Controls.Add(this.lbl_number_toeic);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.btn_play);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_part);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_create_proposesolution";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_create_proposesolution_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_part;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btn_play;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label lbl_number_toeic;
        private System.Windows.Forms.Label lbl_done;
        private System.Windows.Forms.Button btn_record;
    }
}