﻿namespace TryHard_
{
    partial class frm_panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_panel));
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.lbl_title = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.flp_menu = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnl_admincontrols = new System.Windows.Forms.Panel();
            this.btn_addexercise = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_createsection = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.pnl_admincontrols.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbx_exit
            // 
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(701, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 11;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(676, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 10;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 9;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(729, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 8;
            this.pbx_header.TabStop = false;
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.BackColor = System.Drawing.Color.Transparent;
            this.lbl_title.Font = new System.Drawing.Font("Cambria", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbl_title.Location = new System.Drawing.Point(12, 91);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(151, 32);
            this.lbl_title.TabIndex = 12;
            this.lbl_title.Text = "Mock Tests";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Calibri", 13F);
            this.lbl_name.Location = new System.Drawing.Point(444, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(0, 22);
            this.lbl_name.TabIndex = 18;
            // 
            // flp_menu
            // 
            this.flp_menu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_menu.AutoScroll = true;
            this.flp_menu.AutoScrollMargin = new System.Drawing.Size(1, 1);
            this.flp_menu.AutoScrollMinSize = new System.Drawing.Size(2, 2);
            this.flp_menu.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flp_menu.BackColor = System.Drawing.Color.White;
            this.flp_menu.Location = new System.Drawing.Point(24, 142);
            this.flp_menu.MaximumSize = new System.Drawing.Size(680, 180);
            this.flp_menu.Name = "flp_menu";
            this.flp_menu.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.flp_menu.Size = new System.Drawing.Size(680, 180);
            this.flp_menu.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 10F);
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "Create Section:";
            // 
            // pnl_admincontrols
            // 
            this.pnl_admincontrols.BackColor = System.Drawing.Color.White;
            this.pnl_admincontrols.Controls.Add(this.btn_addexercise);
            this.pnl_admincontrols.Controls.Add(this.label2);
            this.pnl_admincontrols.Controls.Add(this.btn_createsection);
            this.pnl_admincontrols.Controls.Add(this.label1);
            this.pnl_admincontrols.Location = new System.Drawing.Point(24, 343);
            this.pnl_admincontrols.Name = "pnl_admincontrols";
            this.pnl_admincontrols.Size = new System.Drawing.Size(678, 38);
            this.pnl_admincontrols.TabIndex = 20;
            // 
            // btn_addexercise
            // 
            this.btn_addexercise.BackgroundImage = global::TryHard_.Properties.Resources.insert;
            this.btn_addexercise.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_addexercise.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_addexercise.FlatAppearance.BorderSize = 0;
            this.btn_addexercise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_addexercise.Location = new System.Drawing.Point(652, 10);
            this.btn_addexercise.Name = "btn_addexercise";
            this.btn_addexercise.Size = new System.Drawing.Size(20, 20);
            this.btn_addexercise.TabIndex = 112;
            this.btn_addexercise.UseVisualStyleBackColor = true;
            this.btn_addexercise.Click += new System.EventHandler(this.btn_addexercise_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 10F);
            this.label2.Location = new System.Drawing.Point(573, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 111;
            this.label2.Text = "Add Exercise";
            // 
            // btn_createsection
            // 
            this.btn_createsection.BackgroundImage = global::TryHard_.Properties.Resources.insert;
            this.btn_createsection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_createsection.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_createsection.FlatAppearance.BorderSize = 0;
            this.btn_createsection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_createsection.Location = new System.Drawing.Point(95, 10);
            this.btn_createsection.Name = "btn_createsection";
            this.btn_createsection.Size = new System.Drawing.Size(20, 20);
            this.btn_createsection.TabIndex = 110;
            this.btn_createsection.UseVisualStyleBackColor = true;
            this.btn_createsection.Click += new System.EventHandler(this.btn_createsection_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(450, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(220, 19);
            this.label3.TabIndex = 143;
            this.label3.Text = "Joel Alexander Cortez Ramírez";
            this.label3.Visible = false;
            // 
            // frm_panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(729, 400);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pnl_admincontrols);
            this.Controls.Add(this.flp_menu);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(729, 400);
            this.Name = "frm_panel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_panel_Load);
            this.Shown += new System.EventHandler(this.frm_panel_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.pnl_admincontrols.ResumeLayout(false);
            this.pnl_admincontrols.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.FlowLayoutPanel flp_menu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnl_admincontrols;
        private System.Windows.Forms.Button btn_createsection;
        private System.Windows.Forms.Button btn_addexercise;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}