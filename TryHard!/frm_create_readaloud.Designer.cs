﻿namespace TryHard_
{
    partial class frm_create_readaloud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_create_readaloud));
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.lbl_part = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_info = new System.Windows.Forms.Label();
            this.txt_text1 = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.txt_text2 = new System.Windows.Forms.TextBox();
            this.btn_next = new System.Windows.Forms.Button();
            this.lbl_number_toeic = new System.Windows.Forms.Label();
            this.lkl_text_1 = new System.Windows.Forms.LinkLabel();
            this.lkl_text_2 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.SuspendLayout();
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(698, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 33;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(673, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 32;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 31;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(730, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 30;
            this.pbx_header.TabStop = false;
            // 
            // lbl_part
            // 
            this.lbl_part.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_part.AutoSize = true;
            this.lbl_part.BackColor = System.Drawing.Color.Transparent;
            this.lbl_part.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_part.Location = new System.Drawing.Point(462, 100);
            this.lbl_part.Name = "lbl_part";
            this.lbl_part.Size = new System.Drawing.Size(91, 17);
            this.lbl_part.TabIndex = 35;
            this.lbl_part.Text = "(Read Aloud)";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(462, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 34;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // lbl_info
            // 
            this.lbl_info.AutoSize = true;
            this.lbl_info.BackColor = System.Drawing.Color.Transparent;
            this.lbl_info.Font = new System.Drawing.Font("Cambria", 12F);
            this.lbl_info.Location = new System.Drawing.Point(72, 130);
            this.lbl_info.Name = "lbl_info";
            this.lbl_info.Size = new System.Drawing.Size(72, 19);
            this.lbl_info.TabIndex = 36;
            this.lbl_info.Text = "Text: 1/2";
            // 
            // txt_text1
            // 
            this.txt_text1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_text1.BackColor = System.Drawing.Color.White;
            this.txt_text1.Font = new System.Drawing.Font("Cambria", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_text1.ForeColor = System.Drawing.Color.DimGray;
            this.txt_text1.Location = new System.Drawing.Point(108, 157);
            this.txt_text1.Multiline = true;
            this.txt_text1.Name = "txt_text1";
            this.txt_text1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_text1.Size = new System.Drawing.Size(518, 212);
            this.txt_text1.TabIndex = 37;
            this.txt_text1.UseSystemPasswordChar = true;
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_save.BackColor = System.Drawing.Color.White;
            this.btn_save.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_save.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.Color.White;
            this.btn_save.Location = new System.Drawing.Point(447, 384);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(84, 41);
            this.btn_save.TabIndex = 38;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // txt_text2
            // 
            this.txt_text2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_text2.BackColor = System.Drawing.Color.White;
            this.txt_text2.Font = new System.Drawing.Font("Cambria", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_text2.ForeColor = System.Drawing.Color.DimGray;
            this.txt_text2.Location = new System.Drawing.Point(108, 157);
            this.txt_text2.Multiline = true;
            this.txt_text2.Name = "txt_text2";
            this.txt_text2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_text2.Size = new System.Drawing.Size(518, 212);
            this.txt_text2.TabIndex = 40;
            this.txt_text2.UseSystemPasswordChar = true;
            this.txt_text2.Visible = false;
            // 
            // btn_next
            // 
            this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next.BackColor = System.Drawing.Color.White;
            this.btn_next.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.btn_next.ForeColor = System.Drawing.Color.White;
            this.btn_next.Location = new System.Drawing.Point(542, 384);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(84, 41);
            this.btn_next.TabIndex = 42;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // lbl_number_toeic
            // 
            this.lbl_number_toeic.AutoSize = true;
            this.lbl_number_toeic.BackColor = System.Drawing.Color.Transparent;
            this.lbl_number_toeic.Font = new System.Drawing.Font("Cambria", 13F);
            this.lbl_number_toeic.Location = new System.Drawing.Point(20, 69);
            this.lbl_number_toeic.Name = "lbl_number_toeic";
            this.lbl_number_toeic.Size = new System.Drawing.Size(74, 21);
            this.lbl_number_toeic.TabIndex = 43;
            this.lbl_number_toeic.Text = "TOEIC #";
            // 
            // lkl_text_1
            // 
            this.lkl_text_1.ActiveLinkColor = System.Drawing.Color.DodgerBlue;
            this.lkl_text_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lkl_text_1.AutoSize = true;
            this.lkl_text_1.BackColor = System.Drawing.Color.Transparent;
            this.lkl_text_1.Font = new System.Drawing.Font("Cambria", 12F);
            this.lkl_text_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lkl_text_1.LinkColor = System.Drawing.Color.Green;
            this.lkl_text_1.Location = new System.Drawing.Point(581, 132);
            this.lkl_text_1.Name = "lkl_text_1";
            this.lkl_text_1.Size = new System.Drawing.Size(18, 19);
            this.lkl_text_1.TabIndex = 44;
            this.lkl_text_1.TabStop = true;
            this.lkl_text_1.Text = "1";
            this.lkl_text_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkl_text_1_LinkClicked);
            // 
            // lkl_text_2
            // 
            this.lkl_text_2.ActiveLinkColor = System.Drawing.Color.DodgerBlue;
            this.lkl_text_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lkl_text_2.AutoSize = true;
            this.lkl_text_2.BackColor = System.Drawing.Color.Transparent;
            this.lkl_text_2.Font = new System.Drawing.Font("Cambria", 12F);
            this.lkl_text_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lkl_text_2.LinkColor = System.Drawing.Color.Green;
            this.lkl_text_2.Location = new System.Drawing.Point(608, 132);
            this.lkl_text_2.Name = "lkl_text_2";
            this.lkl_text_2.Size = new System.Drawing.Size(18, 19);
            this.lkl_text_2.TabIndex = 45;
            this.lkl_text_2.TabStop = true;
            this.lkl_text_2.Text = "2";
            this.lkl_text_2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkl_text_2_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Cambria", 12F);
            this.label1.Location = new System.Drawing.Point(596, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 19);
            this.label1.TabIndex = 46;
            this.label1.Text = "-";
            // 
            // frm_create_readaloud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(730, 450);
            this.Controls.Add(this.lkl_text_2);
            this.Controls.Add(this.lkl_text_1);
            this.Controls.Add(this.lbl_number_toeic);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.txt_text1);
            this.Controls.Add(this.lbl_info);
            this.Controls.Add(this.lbl_part);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.Controls.Add(this.txt_text2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_create_readaloud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_create_readaloud_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Label lbl_part;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_info;
        private System.Windows.Forms.TextBox txt_text1;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.TextBox txt_text2;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Label lbl_number_toeic;
        private System.Windows.Forms.LinkLabel lkl_text_1;
        private System.Windows.Forms.LinkLabel lkl_text_2;
        private System.Windows.Forms.Label label1;
    }
}