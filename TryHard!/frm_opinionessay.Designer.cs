﻿namespace TryHard_
{
    partial class frm_opinionessay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_response = new System.Windows.Forms.TextBox();
            this.lbl_statement = new System.Windows.Forms.Label();
            this.lbl_quantity = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_response
            // 
            this.txt_response.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_response.BackColor = System.Drawing.Color.White;
            this.txt_response.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_response.ForeColor = System.Drawing.Color.DimGray;
            this.txt_response.Location = new System.Drawing.Point(0, 105);
            this.txt_response.Multiline = true;
            this.txt_response.Name = "txt_response";
            this.txt_response.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_response.Size = new System.Drawing.Size(700, 259);
            this.txt_response.TabIndex = 55;
            this.txt_response.UseSystemPasswordChar = true;
            this.txt_response.TextChanged += new System.EventHandler(this.txt_response_TextChanged);
            // 
            // lbl_statement
            // 
            this.lbl_statement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_statement.BackColor = System.Drawing.Color.Transparent;
            this.lbl_statement.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl_statement.Location = new System.Drawing.Point(0, 9);
            this.lbl_statement.Name = "lbl_statement";
            this.lbl_statement.Size = new System.Drawing.Size(700, 84);
            this.lbl_statement.TabIndex = 54;
            this.lbl_statement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_quantity
            // 
            this.lbl_quantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_quantity.BackColor = System.Drawing.Color.Transparent;
            this.lbl_quantity.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl_quantity.Location = new System.Drawing.Point(2, 370);
            this.lbl_quantity.Name = "lbl_quantity";
            this.lbl_quantity.Size = new System.Drawing.Size(349, 24);
            this.lbl_quantity.TabIndex = 56;
            this.lbl_quantity.Text = "Words: 0";
            this.lbl_quantity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frm_opinionessay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(700, 400);
            this.Controls.Add(this.lbl_quantity);
            this.Controls.Add(this.txt_response);
            this.Controls.Add(this.lbl_statement);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_opinionessay";
            this.Load += new System.EventHandler(this.frm_opinionessay_Load);
            this.Shown += new System.EventHandler(this.frm_opinionessay_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_response;
        private System.Windows.Forms.Label lbl_statement;
        private System.Windows.Forms.Label lbl_quantity;
    }
}