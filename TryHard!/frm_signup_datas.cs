﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_signup_datas : Form
    {        
        Classes.Functions fun = new Classes.Functions();
        Classes.Mega host = new Classes.Mega();

        public frm_signup_datas()
        {
            InitializeComponent();
        }

        private void frm_signup_datas_Load(object sender, EventArgs e)
        {
            try
            {
                cbo_privilege.Items.Add("Manager");
                cbo_privilege.Items.Add("User");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btn_continue_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace (txt_names.Text) && !string.IsNullOrWhiteSpace(txt_lastnames.Text) &&
                    !string.IsNullOrWhiteSpace(cbo_privilege.Text))
                {
                    Classes.Vars.Name = Classes.Cifrado.Encriptar(txt_names.Text);
                    Classes.Vars.Privilege = Classes.Cifrado.Encriptar(cbo_privilege.Text);
                    Classes.Vars.Last_Names = Classes.Cifrado.Encriptar(txt_lastnames.Text);
                    Classes.Vars.FullName = txt_names.Text + " " + txt_lastnames.Text;
                    fun.DisableControls(cbo_privilege, 2);
                    fun.DisableControls(txt_names, 2);
                    fun.DisableControls(txt_lastnames, 2);
                    lbl_code.Visible = true;
                    txt_code.Visible = true;
                    lbl_abort.Visible = true;
                    txt_code.Focus();
                }
                else
                {
                    MessageBox.Show("Please fill the blank spaces", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            catch (Exception)
            {

                return;
            }
            
        }

        private void lbl_abort_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            fun.DisableControls(cbo_privilege, 1);
            fun.DisableControls(txt_names, 1);
            fun.DisableControls(txt_lastnames, 1);
            lbl_abort.Visible = false;
            lbl_code.Visible = false;
            txt_code.Visible = false;
        }

        private void txt_code_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) || (e.KeyChar == (char)Keys.Enter))
            {
                if ((int)e.KeyChar == (int)Keys.Enter)
                {
                    e.Handled = false;
                    Classes.Vars.Code = Classes.Cifrado.Encriptar(txt_code.Text);
                    //Search for the file;
                    if (cbo_privilege.Text == "Manager")
                    {
                        if (txt_code.Text == "55897")
                        {                            
                            Form frm = new frm_signup_user();
                            //frm.Owner = this;
                            frm.Show();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Incorrect Code", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        if (host.fileExists(Classes.Vars.Code + ".tryhard") == true)
                        {                            
                            //frm.Owner = this;
                            this.Invoke(new MethodInvoker(delegate {
                                Thread t = new Thread(() =>
                                {
                                    Classes.Functions.Path = @"C:\TryHard\Section";
                                    fun.CreateFolder();
                                    string path = Classes.Functions.Path + @"\" + Classes.Vars.Code + ".tryhard";
                                    if (!File.Exists(path))
                                    {
                                        host.download(path, Classes.Vars.Code + ".tryhard");
                                    }
                                    else
                                    {
                                        //File.Delete(path);
                                        //host.download(path, id);
                                    }
                                    string[] lineas = File.ReadAllLines(path);
                                    Classes.Vars.Provider = lineas[1];                                    
                                    this.Invoke(new MethodInvoker(delegate {
                                        Form frm = new frm_signup_user();
                                        frm.Show();
                                        this.Close(); }));
                                });
                                t.Start();
                            }));
                            
                        }
                        else
                            MessageBox.Show("Incorrect Code", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                e.Handled = true;
                return;
            }
            
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            frm_login login = new frm_login();
            login.Show();
            this.Close();
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }
    }
}
