﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_writtenrequest : Form
    {
        private Classes.Performance performance;

        public frm_writtenrequest()
        {
            InitializeComponent();
        }

        private void frm_writtenrequest_Load(object sender, EventArgs e)
        {
            Classes.Vars.panel_timer_controller.Visible = true;
            Classes.Vars.label_topic_timer.Text = "RESPOND TIME";
            Classes.Vars.label_direction = lbl_direction;
            Classes.Vars.txt_response = txt_response;
            Classes.Vars.txt_email = txt_email;
            txt_response.Focus();
        }

        private void frm_writtenrequest_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            performance.written_request();
        }
    }
}
