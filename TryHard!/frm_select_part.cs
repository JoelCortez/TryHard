﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_select_part : Form
    {
        Classes.SQLite sql = new Classes.SQLite();

        public frm_select_part()
        {
            InitializeComponent();
        }

        private void cb_individual_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_individual_speaking.Checked == true)
            {                
                Deselect(cb_speaking, cb_writing, cb_both, cb_individual_writing);
                cbo_options_speaking.Enabled = true;
                if (!string.IsNullOrEmpty(cbo_options_speaking.Text))
                {
                    assign_part(1);
                    btn_continue.Enabled = true;
                }
            }
            else
            {
                cbo_options_speaking.Enabled = false;
                btn_continue.Enabled = false;
            }
        }

        private void cb_speaking_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_speaking.Checked == true)
            {
                Deselect(cb_writing, cb_both, cb_individual_speaking, cb_individual_writing);
                Classes.Vars.Part_selection = 1;
                btn_continue.Enabled = true;
            }
            else
                btn_continue.Enabled = false;
        }

        private void Deselect(CheckBox check1, CheckBox check2, CheckBox check3, CheckBox check4)
        {
            check1.Checked = false;
            check2.Checked = false;
            check3.Checked = false;
            check4.Checked = false;
        }

        private void cb_writing_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_writing.Checked == true)
            {                
                Deselect(cb_speaking, cb_both, cb_individual_speaking, cb_individual_writing);
                Classes.Vars.Part_selection = 2;
                btn_continue.Enabled = true;
            }
            else
                btn_continue.Enabled = false;
        }

        private void cb_both_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_both.Checked == true)
            {
                Deselect(cb_individual_speaking, cb_speaking, cb_writing, cb_individual_writing);
                Classes.Vars.Part_selection = 3;
                btn_continue.Enabled = true;
            }
            else
                btn_continue.Enabled = false;
        }

        private void btn_continue_Click(object sender, EventArgs e)
        {
            Form frm = new Form();
            if (Classes.Vars.State == 1)
            {
                if (Classes.Vars.Part_selection == 1 || Classes.Vars.Part_selection == 3 || Classes.Vars.Part_selection == 4)
                {
                    frm = new frm_create_readaloud();
                }
                if (Classes.Vars.Part_selection == 2 || Classes.Vars.Part_selection == 10)
                {
                    frm = new frm_create_basedonpicture();
                }
                switch (Classes.Vars.Part_selection)
                {
                    case 5:
                        frm = new frm_create_describepicture();
                        break;
                    case 6:
                        frm = new frm_create_respondquestions();
                        break;
                    case 7:
                        frm = new frm_create_infoprovided();
                        break;
                    case 8:
                        frm = new frm_create_proposesolution();
                        break;
                    case 9:
                        frm = new frm_create_expressopinion();
                        break;
                    case 10:
                        frm = new frm_create_basedonpicture();
                        break;
                    case 11:
                        frm = new frm_create_writtenrequest();
                        break;
                    case 12:
                        frm = new frm_create_opinionessay();
                        break;
                }
                //DATABASE
                Classes.Functions fun = new Classes.Functions();
                Classes.Functions.Path = @"C:\TryHard\Data";
                fun.CreateFolder();
                //Classes.Vars.ID = "SBmarKhHFhY+huu9gtIfew==";
                Classes.Vars.Charging = 1;
                sql.create_db_toeic();
                sql.insert_number_toeic();
                Classes.Vars.Number_toeic = int.Parse(sql.check_number_exists().Rows[0][0].ToString());
                frm.Show();
                this.Close();
            }
            else
            {
                switch(Classes.Vars.Part_selection)
                {
                    case 1:
                        if (check_parts(0, 5))
                        {
                            for (int i = 0; i < 9; i++)
                            {
                                Classes.Vars.parts[i] = 0;
                            }
                            for (int i = 0; i < 6; i++)
                            {
                                Classes.Vars.parts[i] = 1;
                            }
                            frm = new frm_Updownload();
                            frm.Show();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Speaking Part is incompleted, make sure you try to download an existing exercise");
                        }
                        break;
                    case 2:
                        if (check_parts(6, 8))
                        {
                            for (int i = 0; i < 9; i++)
                            {
                                Classes.Vars.parts[i] = 0;
                            }
                            for (int i = 6; i < 9; i++)
                            {
                                Classes.Vars.parts[i] = 1;
                            }
                            frm = new frm_Updownload();
                            frm.Show();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Writing Part is incompleted, make sure you try to download an existing exercise");
                        }
                        break;
                    case 3:
                        if (check_parts(0, 8))
                        {
                            for (int i = 0; i < 9; i++)
                            {
                                Classes.Vars.parts[i] = 1;
                            }
                            frm = new frm_Updownload();
                            frm.Show();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("The whole test is incompleted, make sure you try to download an existing exercise");
                        }
                        break;
                    default:
                        int position = Classes.Vars.Part_selection - 4;
                        if (check_parts(position, position))
                        {
                            //MessageBox.Show(frm_Updownload.pgb_step.ToString());
                            for (int i = 0; i < 9; i++)
                            {
                                Classes.Vars.parts[i] = 0;
                            }
                            Classes.Vars.parts[position] = 1;
                            int steps_to = frm_Updownload.pgb_step;
                            frm = new frm_Updownload();
                            frm.Show();
                            frm_Updownload.pgb_step = steps_to - 1;
                            //MessageBox.Show(frm_Updownload.pgb_step.ToString());
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Make sure you try to download an existing exercise");
                        }
                        break;
                }                
            }
        }

        private void cbo_options_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbo_options_speaking.Text))
            {
                switch(cbo_options_speaking.SelectedItem.ToString())
                {
                    case "Reading Aloud": Classes.Vars.Part_selection = 4; frm_Updownload.pgb_step += 2;
                        break;
                    case "Describing a Picture": Classes.Vars.Part_selection = 5; frm_Updownload.pgb_step += 1;
                        break;
                    case "Respond To Questions": Classes.Vars.Part_selection = 6; frm_Updownload.pgb_step += 3;
                        break;
                    case "Respond with information provided": Classes.Vars.Part_selection = 7; frm_Updownload.pgb_step += 3;
                        break;
                    case "Propose a solution": Classes.Vars.Part_selection = 8; frm_Updownload.pgb_step += 1;
                        break;
                    case "Express an opinion": Classes.Vars.Part_selection = 9; frm_Updownload.pgb_step += 1;
                        break;                    
                }
                btn_continue.Enabled = true;
            }                
            else
                btn_continue.Enabled = false;
        }

        private void cbo_individual_writing_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_individual_writing.Checked == true)
            {
                Deselect(cb_speaking, cb_writing, cb_both, cb_individual_speaking);
                cbo_options_writing.Enabled = true;
                if (!string.IsNullOrEmpty(cbo_options_writing.Text))
                {
                    assign_part(2);
                    btn_continue.Enabled = true;
                }
            }
            else
            {
                cbo_options_writing.Enabled = false;
                btn_continue.Enabled = false;
            }
        }

        private void cbo_options_writing_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbo_options_writing.Text))
            {
                switch (cbo_options_writing.SelectedItem.ToString())
                {                    
                    case "Based on a Picture":
                        Classes.Vars.Part_selection = 10; frm_Updownload.pgb_step += 5;
                        break;
                    case "Written Request":
                        Classes.Vars.Part_selection = 11; frm_Updownload.pgb_step += 2;
                        break;
                    case "Opinion Essay":
                        Classes.Vars.Part_selection = 12; frm_Updownload.pgb_step += 1;
                        break;
                }
                btn_continue.Enabled = true;
            }
            else
                btn_continue.Enabled = false;
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            /*Classes.Vars.ID = "SBmarKhHFhY+huu9gtIfew==";
            try
            {
                MessageBox.Show(sql.table.Rows[0][0].ToString());
            }
            catch (Exception)
            {
                MessageBox.Show("Wrong");
            }*/
            Classes.MainEvents.minimizar(this);
        }

        private void assign_part(int state)
        {
            if (state == 1)
            {
                switch (cbo_options_speaking.Text)
                {
                    case "Reading Aloud":
                        Classes.Vars.Part_selection = 4;
                        break;
                    case "Describing a Picture":
                        Classes.Vars.Part_selection = 5;
                        break;
                    case "Respond To Questions":
                        Classes.Vars.Part_selection = 6;
                        break;
                    case "Respond with information provided":
                        Classes.Vars.Part_selection = 7;
                        break;
                    case "Propose a solution":
                        Classes.Vars.Part_selection = 8;
                        break;
                    case "Express an opinion":
                        Classes.Vars.Part_selection = 9;
                        break;
                }
            }
            else if(state == 2)
            {
                switch (cbo_options_writing.Text)
                {
                    case "Based on a Picture":
                        Classes.Vars.Part_selection = 10;
                        break;
                    case "Written Request":
                        Classes.Vars.Part_selection = 11;
                        break;
                    case "Opinion Essay":
                        Classes.Vars.Part_selection = 12;
                        break;
                }
            }
        }

        private void frm_select_part_Load(object sender, EventArgs e)
        {
            frm_Updownload.pgb_step = 3;
            lbl_name.Text = Classes.Vars.FullName;
            if(Classes.Vars.State_exercise != 1)
            {
                if(Classes.Vars.State != 1)
                {
                    start_loading(Classes.Vars.Provider);
                    //cb_speaking.Enabled = false;
                    //cb_writing.Enabled = false;
                    //cb_both.Enabled = false;
                }
            }
            else
            {
                
            }
        }

        private void start_loading(string id)
        {
            DataTable table = sql.show_datas_speaking(id);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (table.Rows[i][j].ToString() != "0")
                    {                       
                        Classes.Vars.parts[j] = 1;
                    }
                }
            }
            table = sql.show_datas_writing(id);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (table.Rows[i][j].ToString() != "0")
                    {                        
                        Classes.Vars.parts[j + 6] = 1;
                    }
                }
            }           
        }

        private bool check_parts(int start_position, int final_position)
        {
            bool state = true;
            for(int i = start_position; i <= final_position; i++)
            {
                if(Classes.Vars.parts[i].ToString() == "0")
                {
                    state = false;
                }
            }
            return state;
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            Classes.Vars.Charging = 1;
            Classes.Vars.Performing = false;
            frm_panel frm = new frm_panel();
            Classes.Vars.form = frm;
            frm.Show();
            this.Close();
        }
    }
}
