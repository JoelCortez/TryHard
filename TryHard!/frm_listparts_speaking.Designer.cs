﻿namespace TryHard_
{
    partial class frm_listparts_speaking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_listparts_speaking));
            this.lbl_welcome = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_welcome
            // 
            this.lbl_welcome.AutoSize = true;
            this.lbl_welcome.BackColor = System.Drawing.Color.Transparent;
            this.lbl_welcome.Font = new System.Drawing.Font("Cambria", 13F, System.Drawing.FontStyle.Bold);
            this.lbl_welcome.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbl_welcome.Location = new System.Drawing.Point(31, 122);
            this.lbl_welcome.Name = "lbl_welcome";
            this.lbl_welcome.Size = new System.Drawing.Size(341, 21);
            this.lbl_welcome.TabIndex = 124;
            this.lbl_welcome.Text = "Student: Joel Alexander Cortez Ramírez";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(415, 70);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(220, 19);
            this.lbl_name.TabIndex = 123;
            this.lbl_name.Text = "Joel Alexander Cortez Ramírez";
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(651, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 122;
            this.pbx_exit.TabStop = false;
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(626, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 121;
            this.pbx_min.TabStop = false;
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 120;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(683, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 119;
            this.pbx_header.TabStop = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(485, 270);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 16);
            this.label4.TabIndex = 148;
            this.label4.Text = "Express an Opinion";
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(454, 266);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(25, 25);
            this.button4.TabIndex = 147;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(278, 270);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 16);
            this.label5.TabIndex = 146;
            this.label5.Text = "Propose a Solution";
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(247, 266);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(25, 25);
            this.button5.TabIndex = 145;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(66, 270);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 52);
            this.label6.TabIndex = 144;
            this.label6.Text = "Respond with Information Provided";
            // 
            // button6
            // 
            this.button6.BackgroundImage = global::TryHard_.Properties.Resources.correct;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(35, 266);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(25, 25);
            this.button6.TabIndex = 143;
            this.button6.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(485, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 16);
            this.label3.TabIndex = 142;
            this.label3.Text = "Respond To Questions";
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(454, 185);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(25, 25);
            this.button3.TabIndex = 141;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(278, 189);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 16);
            this.label2.TabIndex = 140;
            this.label2.Text = "Describe a Picture";
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::TryHard_.Properties.Resources.correct;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(247, 185);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 25);
            this.button2.TabIndex = 139;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(66, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 16);
            this.label1.TabIndex = 138;
            this.label1.Text = "Reading Aloud 1-2";
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::TryHard_.Properties.Resources.wrong;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(35, 185);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 25);
            this.button1.TabIndex = 137;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // frm_listparts_speaking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(683, 374);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl_welcome);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_listparts_speaking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_listparts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_welcome;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}