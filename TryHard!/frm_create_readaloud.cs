﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_create_readaloud : Form
    {
        Classes.Functions fun = new Classes.Functions();
        Classes.SQLite sql = new Classes.SQLite();
        public frm_create_readaloud()
        {
            InitializeComponent();
        }     

        private void frm_create_readaloud_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            if (Classes.Vars.State_exercise == 1)
            {
                try
                {
                    for (int i = 1; i <= 2; i++)
                    {
                        string path = @"C:\TryHard\Files\ReadingAloud\" + Classes.Vars.ID + "_reading_" + i + "_" + Classes.Vars.Number_toeic + ".tryhard";
                        this.Controls["txt_text" + i].Text = File.ReadAllText(path);                        
                    }
                    btn_save.Text = "Modify";
                    txt_text1.ReadOnly = true;
                    txt_text2.ReadOnly = true;
                    btn_next.Text = "Close";
                }
                catch (Exception ex)
                {
                    Controls.Remove(btn_save);
                    txt_text1.ReadOnly = true;
                    txt_text2.ReadOnly = true;
                    btn_next.Text = "Close";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                if (Classes.Vars.Part_selection == 4)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
        }

        
        private void btn_next_Click(object sender, EventArgs e)
        {
            Classes.Vars.Editing = 1;
            if (!string.IsNullOrWhiteSpace(txt_text1.Text) && !string.IsNullOrWhiteSpace(txt_text2.Text))
            {
                if (btn_next.Text == "Close")
                {
                    this.Close();
                }
                else
                {                
                    save_reading();
                    Form frm = new Form();
                    if (btn_next.Text == "Finish")
                    {
                        if (Classes.Vars.State_exercise == 1)
                        {
                            this.Close();
                            return;
                        }
                        else
                        {
                            frm = new frm_allparts();
                        }
                    }
                    else
                    {
                        frm = new frm_create_describepicture();
                    }
                    frm.Show();
                    this.Close();
                }                
            }
            else
            {
                MessageBox.Show("You should complete all the texts to continue", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

        }

        private void save_reading()
        {
            try
            {
                Classes.Functions.Path = @"C:\TryHard\Files\ReadingAloud";
                fun.CreateFolder();
                string text;
                text = txt_text1.Text;
                string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_reading_1_" + Classes.Vars.Number_toeic + ".tryhard";
                File.WriteAllText(path, text);
                text = txt_text2.Text;
                path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_reading_2_" + Classes.Vars.Number_toeic + ".tryhard";
                File.WriteAllText(path, text);
                sql.update_parts("reading", 1);
                if (sql.exercise_state("reading"))
                {
                    Classes.Vars.parts[0] = 1;
                }
                
            }
            catch (Exception d)
            {
                MessageBox.Show(d.Message);
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if(btn_save.Text == "Modify")
            {
                txt_text1.ReadOnly = false;
                txt_text2.ReadOnly = false;
                this.Controls.Remove(btn_save);
                btn_next.Text = "Finish";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(txt_text1.Text) && !string.IsNullOrWhiteSpace(txt_text2.Text))
                {
                    save_reading();
                    Form frm = new frm_allparts();
                    frm.Show();
                    this.Close();
                }
                else
                {
                    if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                }
            }
        }

        private void lkl_text_1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txt_text1.Visible = true;
            txt_text2.Visible = false;
            lbl_info.Text = "Text: 1/2";
        }

        private void lkl_text_2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txt_text1.Visible = false;
            txt_text2.Visible = true;
            lbl_info.Text = "Text: 2/2";            
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }
    }
}

