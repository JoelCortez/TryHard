﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_create_basedonpicture : Form
    {
        OpenFileDialog ofl = new OpenFileDialog();
        public static String[] filenames;
        public static String[,] words;
        public static int ofl_state = 1;
        private int state_img = 0;
        Classes.Events events = new Classes.Events();
        Classes.Functions fun = new Classes.Functions();
        Classes.SQLite sql = new Classes.SQLite();        

        public frm_create_basedonpicture()
        {
            InitializeComponent();
        }

        private void frm_create_basedonpicture_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            ofl.Filter = "PNG|*.png|JPG|*.jpg";
            ofl.Title = "Select a picture";
            filenames = new string[5];
            words = new string[5, 2];
            if (Classes.Vars.State_exercise == 1)
            {
                btn_next.Text = "Close";
                btn_save.Text = "Modify";
                ofl_state = 0;
                for (int i = 0; i < 5; i++)
                {
                    Classes.Vars.Number_Picture = i;
                    events.Add_event(this);
                }
                Classes.Functions.Path = @"C:\TryHard\Files\Based";
                try
                {
                    string[] lineas = new string[10];
                    string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_based_words_" + Classes.Vars.Number_toeic + ".tryhard";
                    lineas = File.ReadAllLines(path);
                    int index = 0;
                    for (int i = 0; i < 5; i++)
                    {
                        for (int k = 0; k < 2; k++)
                        {
                            words[i, k] = lineas[index];
                            index++;
                        }
                        path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_based_words_picture_" + (i + 1).ToString() + "_" + Classes.Vars.Number_toeic + ".png";
                        filenames[i] = path;
                        this.Controls["btn_img_" + (i +  1).ToString()].BackgroundImage = Image.FromFile(filenames[i]);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                if (Classes.Vars.Part_selection == 10)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
        }

        private void btn_img_1_Click(object sender, EventArgs e)
        {
            events.open_dialog(0, this);
        }        

        private void btn_img_2_Click(object sender, EventArgs e)
        {
            events.open_dialog(1, this);
        }

        private void btn_img_3_Click(object sender, EventArgs e)
        {
            events.open_dialog(2, this);
        }

        private void btn_img_4_Click(object sender, EventArgs e)
        {
            events.open_dialog(3, this);
        }

        private void btn_next_Click(object sender, EventArgs e)
        {            
            Classes.Vars.Editing = 1;
            if (!check_filename() && !check_words())
            {
                try
                {                    
                    save_based();
                    //for (int i = 0; i < 9; i++)
                    //{
                        //MessageBox.Show((i + 1).ToString() + " Based- " + Classes.Vars.parts[i].ToString());
                    //}
                    Form frm = new Form();
                    if (btn_next.Text == "Close")
                    {                        
                        this.Close();
                        return;
                    }
                    else
                    {
                        if (btn_next.Text == "Finish")
                        {
                            state_img = 1;
                            if (Classes.Vars.State_exercise == 1)
                            {
                                this.Close();
                                return;
                            }
                            else
                            {
                                frm = new frm_allparts();
                            }
                        }
                        else
                        {
                            frm = new frm_create_writtenrequest();
                        }
                    }
                    frm.Show();
                    this.Close();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("You should complete all the parts", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            MessageBox.Show(Classes.Vars.Editing.ToString());
        }

        private void btn_img_5_Click(object sender, EventArgs e)
        {
            events.open_dialog(4, this);
        }

        private void save_based()
        {
            int index = 0;
            string[] lineas = new string[10];
            for (int i = 0; i < 5; i++)
            {
                for (int k = 0; k < 2; k++)
                {
                    lineas[index] = words[i, k];
                    index++;
                }
            }
            Classes.Functions.Path = @"C:\TryHard\Files\Based";
            fun.CreateFolder();
            string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_based_words_" + Classes.Vars.Number_toeic + ".tryhard";
            File.WriteAllLines(path, lineas);
            for (int i = 1; i <= 5; i++)
            {
                path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_based_words_picture_" + i.ToString() + "_" + Classes.Vars.Number_toeic + ".png";
                if (filenames[i-1] != path)
                {
                    File.Copy(filenames[i-1], path, true);
                }
            }
            sql.update_parts("based", 1);
            if (sql.exercise_state("based"))
            {
                Classes.Vars.parts[6] = 1;
            }
        }
        private bool check_filename()
        {
            bool vf = false;
            for (int i = 0; i < 5; i++)
            {
                if (String.IsNullOrEmpty(filenames[i]))
                {
                    vf = true;
                    break;
                }
                else
                    vf = false;
            }
            return vf;
        }

        private bool check_words()
        {
            bool vf = false;
            for (int i = 0; i < 5; i++)
            {
                for (int a = 0; a < 2; a++)
                {
                    if (String.IsNullOrEmpty(words[i, a]))
                    {
                        vf = true;
                        break;                        
                    }
                    else
                    {
                        vf = false;
                    }
                }
                if (vf == true)
                {
                    break;
                }
            }
            return vf;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (btn_save.Text == "Modify")
            {
                ofl_state = 1;
                this.Controls.Remove(btn_save);
                btn_next.Text = "Finish";                
            }
            else if (btn_save.Text == "Save")
            {
                if (!check_filename() && !check_words())
                {
                    try
                    {
                        save_based();
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                }
            }
        }        
        private void frm_create_basedonpicture_FormClosing(object sender, FormClosingEventArgs e)
        {
            for (int i = 1; i <= 5; i++)
            {
                this.Controls["btn_img_" + i.ToString()].BackgroundImage.Dispose();
            }
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }
    }
}
