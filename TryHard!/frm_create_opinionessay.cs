﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace TryHard_
{
    public partial class frm_create_opinionessay : Form
    {
        public frm_create_opinionessay()
        {
            InitializeComponent();
        }

        public static string[] texts;
        Classes.Functions fun = new Classes.Functions();
        Classes.SQLite sql = new Classes.SQLite();

        private void frm_create_opinionessay_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            texts = new string[2];
            if (Classes.Vars.State_exercise == 1)
            {
                btn_next.Text = "Close";
                btn_save.Text = "Modify";
                string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_statement_" + Classes.Vars.Number_toeic + ".tryhard";
                texts[0] = File.ReadAllText(path);
                path = path.Replace("statement", "directions");
                texts[1] = File.ReadAllText(path);
                txt_statement.Text = texts[0];
                txt_directions.Text = texts[1];
                fun.DisableControls(txt_statement, 2);
                fun.DisableControls(txt_directions, 2);
            }
            else
            {
                if (Classes.Vars.Part_selection == 12)
                {
                    this.Controls.Remove(btn_save);
                    btn_next.Text = "Finish";
                }
            }
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
        }

        private void save_opinion()
        {
            Classes.Functions.Path = @"C:\TryHard\Files\Opinion";
            fun.CreateFolder();
            string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_statement_" + Classes.Vars.Number_toeic + ".tryhard";
            File.WriteAllText(path, texts[0]);
            path = path.Replace("statement", "directions");
            File.WriteAllText(path, texts[1]);
            sql.update_parts("essay", 1);
            if (sql.exercise_state("essay"))
            {
                Classes.Vars.parts[8] = 1;
            }
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            Classes.Vars.Editing = 1;
            texts[0] = txt_statement.Text;
            texts[1] = txt_directions.Text;
            if (!String.IsNullOrWhiteSpace(texts[0]))// && !String.IsNullOrWhiteSpace(texts[1]))
            {
                try
                {
                    save_opinion();
                    Form frm = new Form();
                    if (btn_next.Text == "Close")
                    {
                        this.Close();
                        return;
                    }
                    else
                    {
                        if (btn_next.Text == "Finish")
                        {
                            if (Classes.Vars.State_exercise == 1)
                            {
                                this.Close();
                                return;
                            }
                            else
                            {
                                frm = new frm_allparts();
                            }
                        }
                        else
                        {
                            frm = new frm_allparts();
                        }
                    }
                    frm.Show();
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("You should complete all the parts", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (btn_save.Text == "Modify")
            {
                this.Controls.Remove(btn_save);
                btn_next.Text = "Finish";
                fun.DisableControls(txt_directions, 1);
                fun.DisableControls(txt_statement, 1);
            }
            else if (btn_save.Text == "Save")
            {
                texts[0] = txt_statement.Text;
                texts[1] = txt_directions.Text;
                if (!String.IsNullOrWhiteSpace(texts[0]))// && !String.IsNullOrWhiteSpace(texts[1]))
                {
                    try
                    {
                        save_opinion();
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Form frm = new frm_allparts();
                        frm.Show();
                        this.Close();
                    }
                }
            }
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }
    }
}
