﻿namespace TryHard_
{
    partial class frm_signup_datas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_signup_datas));
            this.pbx_exit = new System.Windows.Forms.PictureBox();
            this.pbx_min = new System.Windows.Forms.PictureBox();
            this.pbx_title = new System.Windows.Forms.PictureBox();
            this.pbx_header = new System.Windows.Forms.PictureBox();
            this.btn_continue = new System.Windows.Forms.Button();
            this.txt_lastnames = new System.Windows.Forms.TextBox();
            this.lbl_lastnames = new System.Windows.Forms.Label();
            this.txt_names = new System.Windows.Forms.TextBox();
            this.lbl_names = new System.Windows.Forms.Label();
            this.lbl_welcome = new System.Windows.Forms.Label();
            this.cbo_privilege = new System.Windows.Forms.ComboBox();
            this.lbl_privilege = new System.Windows.Forms.Label();
            this.lbl_abort = new System.Windows.Forms.LinkLabel();
            this.txt_code = new System.Windows.Forms.TextBox();
            this.lbl_code = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).BeginInit();
            this.SuspendLayout();
            // 
            // pbx_exit
            // 
            this.pbx_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_exit.BackColor = System.Drawing.Color.Transparent;
            this.pbx_exit.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_exit.Image = global::TryHard_.Properties.Resources.exit;
            this.pbx_exit.Location = new System.Drawing.Point(479, 12);
            this.pbx_exit.Name = "pbx_exit";
            this.pbx_exit.Size = new System.Drawing.Size(20, 20);
            this.pbx_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_exit.TabIndex = 7;
            this.pbx_exit.TabStop = false;
            this.pbx_exit.Click += new System.EventHandler(this.pbx_exit_Click);
            // 
            // pbx_min
            // 
            this.pbx_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbx_min.BackColor = System.Drawing.Color.Transparent;
            this.pbx_min.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbx_min.Image = global::TryHard_.Properties.Resources.min;
            this.pbx_min.Location = new System.Drawing.Point(454, 12);
            this.pbx_min.Name = "pbx_min";
            this.pbx_min.Size = new System.Drawing.Size(20, 20);
            this.pbx_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_min.TabIndex = 6;
            this.pbx_min.TabStop = false;
            this.pbx_min.Click += new System.EventHandler(this.pbx_min_Click);
            // 
            // pbx_title
            // 
            this.pbx_title.BackColor = System.Drawing.Color.Transparent;
            this.pbx_title.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.pbx_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbx_title.Image = ((System.Drawing.Image)(resources.GetObject("pbx_title.Image")));
            this.pbx_title.Location = new System.Drawing.Point(0, 0);
            this.pbx_title.Name = "pbx_title";
            this.pbx_title.Size = new System.Drawing.Size(165, 50);
            this.pbx_title.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_title.TabIndex = 5;
            this.pbx_title.TabStop = false;
            // 
            // pbx_header
            // 
            this.pbx_header.BackColor = System.Drawing.Color.DodgerBlue;
            this.pbx_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbx_header.Image = global::TryHard_.Properties.Resources.header;
            this.pbx_header.Location = new System.Drawing.Point(0, 0);
            this.pbx_header.Name = "pbx_header";
            this.pbx_header.Size = new System.Drawing.Size(511, 50);
            this.pbx_header.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_header.TabIndex = 4;
            this.pbx_header.TabStop = false;
            // 
            // btn_continue
            // 
            this.btn_continue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_continue.BackColor = System.Drawing.Color.White;
            this.btn_continue.BackgroundImage = global::TryHard_.Properties.Resources.header;
            this.btn_continue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_continue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_continue.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_continue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_continue.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold);
            this.btn_continue.ForeColor = System.Drawing.Color.White;
            this.btn_continue.Location = new System.Drawing.Point(193, 260);
            this.btn_continue.Name = "btn_continue";
            this.btn_continue.Size = new System.Drawing.Size(124, 41);
            this.btn_continue.TabIndex = 3;
            this.btn_continue.Text = "Continue";
            this.btn_continue.UseVisualStyleBackColor = false;
            this.btn_continue.Click += new System.EventHandler(this.btn_continue_Click);
            // 
            // txt_lastnames
            // 
            this.txt_lastnames.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_lastnames.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_lastnames.ForeColor = System.Drawing.Color.DimGray;
            this.txt_lastnames.Location = new System.Drawing.Point(190, 219);
            this.txt_lastnames.Name = "txt_lastnames";
            this.txt_lastnames.Size = new System.Drawing.Size(178, 25);
            this.txt_lastnames.TabIndex = 2;
            // 
            // lbl_lastnames
            // 
            this.lbl_lastnames.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_lastnames.AutoSize = true;
            this.lbl_lastnames.BackColor = System.Drawing.Color.Transparent;
            this.lbl_lastnames.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_lastnames.Location = new System.Drawing.Point(81, 222);
            this.lbl_lastnames.Name = "lbl_lastnames";
            this.lbl_lastnames.Size = new System.Drawing.Size(83, 17);
            this.lbl_lastnames.TabIndex = 13;
            this.lbl_lastnames.Text = "Last Names:";
            // 
            // txt_names
            // 
            this.txt_names.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_names.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_names.ForeColor = System.Drawing.Color.DimGray;
            this.txt_names.Location = new System.Drawing.Point(190, 173);
            this.txt_names.Name = "txt_names";
            this.txt_names.Size = new System.Drawing.Size(178, 25);
            this.txt_names.TabIndex = 1;
            // 
            // lbl_names
            // 
            this.lbl_names.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_names.AutoSize = true;
            this.lbl_names.BackColor = System.Drawing.Color.Transparent;
            this.lbl_names.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_names.Location = new System.Drawing.Point(81, 178);
            this.lbl_names.Name = "lbl_names";
            this.lbl_names.Size = new System.Drawing.Size(54, 17);
            this.lbl_names.TabIndex = 11;
            this.lbl_names.Text = "Names:";
            // 
            // lbl_welcome
            // 
            this.lbl_welcome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_welcome.AutoSize = true;
            this.lbl_welcome.BackColor = System.Drawing.Color.Transparent;
            this.lbl_welcome.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold);
            this.lbl_welcome.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbl_welcome.Location = new System.Drawing.Point(183, 65);
            this.lbl_welcome.Name = "lbl_welcome";
            this.lbl_welcome.Size = new System.Drawing.Size(145, 40);
            this.lbl_welcome.TabIndex = 10;
            this.lbl_welcome.Text = "Sign Up";
            // 
            // cbo_privilege
            // 
            this.cbo_privilege.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_privilege.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_privilege.Font = new System.Drawing.Font("Cambria", 11F);
            this.cbo_privilege.ForeColor = System.Drawing.Color.DimGray;
            this.cbo_privilege.FormattingEnabled = true;
            this.cbo_privilege.Location = new System.Drawing.Point(190, 124);
            this.cbo_privilege.Name = "cbo_privilege";
            this.cbo_privilege.Size = new System.Drawing.Size(111, 25);
            this.cbo_privilege.TabIndex = 0;
            // 
            // lbl_privilege
            // 
            this.lbl_privilege.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_privilege.AutoSize = true;
            this.lbl_privilege.BackColor = System.Drawing.Color.Transparent;
            this.lbl_privilege.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_privilege.Location = new System.Drawing.Point(81, 127);
            this.lbl_privilege.Name = "lbl_privilege";
            this.lbl_privilege.Size = new System.Drawing.Size(68, 17);
            this.lbl_privilege.TabIndex = 17;
            this.lbl_privilege.Text = "Privilege:";
            // 
            // lbl_abort
            // 
            this.lbl_abort.ActiveLinkColor = System.Drawing.Color.DodgerBlue;
            this.lbl_abort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_abort.AutoSize = true;
            this.lbl_abort.BackColor = System.Drawing.Color.Transparent;
            this.lbl_abort.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_abort.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbl_abort.LinkColor = System.Drawing.Color.Green;
            this.lbl_abort.Location = new System.Drawing.Point(12, 294);
            this.lbl_abort.Name = "lbl_abort";
            this.lbl_abort.Size = new System.Drawing.Size(54, 19);
            this.lbl_abort.TabIndex = 4;
            this.lbl_abort.TabStop = true;
            this.lbl_abort.Text = "Cancel";
            this.lbl_abort.Visible = false;
            this.lbl_abort.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl_abort_LinkClicked);
            // 
            // txt_code
            // 
            this.txt_code.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_code.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_code.ForeColor = System.Drawing.Color.DimGray;
            this.txt_code.Location = new System.Drawing.Point(429, 284);
            this.txt_code.MaxLength = 5;
            this.txt_code.Name = "txt_code";
            this.txt_code.Size = new System.Drawing.Size(64, 25);
            this.txt_code.TabIndex = 19;
            this.txt_code.Visible = false;
            this.txt_code.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_code_KeyPress);
            // 
            // lbl_code
            // 
            this.lbl_code.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_code.AutoSize = true;
            this.lbl_code.BackColor = System.Drawing.Color.Transparent;
            this.lbl_code.Font = new System.Drawing.Font("Cambria", 11F);
            this.lbl_code.Location = new System.Drawing.Point(380, 287);
            this.lbl_code.Name = "lbl_code";
            this.lbl_code.Size = new System.Drawing.Size(43, 17);
            this.lbl_code.TabIndex = 18;
            this.lbl_code.Text = "Code:";
            this.lbl_code.Visible = false;
            // 
            // frm_signup_datas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TryHard_.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(511, 322);
            this.Controls.Add(this.txt_code);
            this.Controls.Add(this.lbl_code);
            this.Controls.Add(this.lbl_abort);
            this.Controls.Add(this.lbl_privilege);
            this.Controls.Add(this.cbo_privilege);
            this.Controls.Add(this.btn_continue);
            this.Controls.Add(this.txt_lastnames);
            this.Controls.Add(this.lbl_lastnames);
            this.Controls.Add(this.txt_names);
            this.Controls.Add(this.lbl_names);
            this.Controls.Add(this.lbl_welcome);
            this.Controls.Add(this.pbx_exit);
            this.Controls.Add(this.pbx_min);
            this.Controls.Add(this.pbx_title);
            this.Controls.Add(this.pbx_header);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_signup_datas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sign Up";
            this.Load += new System.EventHandler(this.frm_signup_datas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbx_exit;
        private System.Windows.Forms.PictureBox pbx_min;
        private System.Windows.Forms.PictureBox pbx_title;
        private System.Windows.Forms.PictureBox pbx_header;
        private System.Windows.Forms.Button btn_continue;
        private System.Windows.Forms.TextBox txt_lastnames;
        private System.Windows.Forms.Label lbl_lastnames;
        private System.Windows.Forms.TextBox txt_names;
        private System.Windows.Forms.Label lbl_names;
        private System.Windows.Forms.Label lbl_welcome;
        private System.Windows.Forms.ComboBox cbo_privilege;
        private System.Windows.Forms.Label lbl_privilege;
        private System.Windows.Forms.LinkLabel lbl_abort;
        private System.Windows.Forms.TextBox txt_code;
        private System.Windows.Forms.Label lbl_code;
    }
}