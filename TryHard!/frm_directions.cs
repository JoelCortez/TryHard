﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_directions : Form
    {
        private Classes.Performance performance = new Classes.Performance();

        private string path = @"C:\TryHard\res\";
        public frm_directions()
        {
            InitializeComponent();
        }

        private void frm_directions_Load(object sender, EventArgs e)
        {
            Classes.Vars.lbl_text = lbl_text;
            set_direction();
            performance.preparation_direction_time();
        }

        private void set_direction()
        {
            switch(Classes.Vars.Part_selection)
            {
                case 4:
                    Classes.Vars.label_part.Text = "(Read Aloud)";
                    Classes.Functions.Path = path + @"1\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"1\Directions.txt");
                    break;
                case 5:
                    Classes.Vars.label_part.Text = "(Describe a picture)";
                    Classes.Functions.Path = path + @"2\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"2\Directions.txt");
                    break;
                case 6:
                    Classes.Vars.label_part.Text = "(Respond to Questions)";
                    Classes.Functions.Path = path + @"3\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"3\Directions.txt");
                    break;
                case 7:
                    Classes.Vars.label_part.Text = "(Respond to Questions with information provided)";
                    Classes.Functions.Path = path + @"4\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"4\Directions.txt");
                    break;
                case 8:
                    Classes.Vars.label_part.Text = "(Propose a solution)";
                    Classes.Functions.Path = path + @"5\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"5\Directions.txt");
                    break;
                case 9:
                    Classes.Vars.label_part.Text = "(Express an opinion)";
                    Classes.Functions.Path = path + @"6\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"6\Directions.txt");
                    break;
                case 10:
                    Classes.Vars.label_part.Text = "(Based on a picture)";
                    Classes.Functions.Path = path + @"7\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"7\Directions.txt");
                    break;
                case 11:
                    Classes.Vars.label_part.Text = "(Respond to Written Request)";
                    Classes.Functions.Path = path + @"8\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"8\Directions.txt");
                    break;
                case 12:
                    Classes.Vars.label_part.Text = "(Opinion Essay)";
                    Classes.Functions.Path = path + @"9\Directions.mp3";
                    lbl_text.Text = File.ReadAllText(path + @"9\Directions.txt");
                    break;
            }
        }
    }
}
