﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TryHard_
{
    public partial class frm_describepicture : Form
    {
        private Classes.Performance performance;
        public frm_describepicture()
        {
            InitializeComponent();
        }

        private void frm_describepicture_Load(object sender, EventArgs e)
        {
            pbx_picture.Image = Image.FromFile(@"C:\TryHard\Files\Describe\" + Classes.Vars.Provider + "_describing_" + Classes.Vars.Number_toeic + ".png");
            Classes.Vars.panel_timer_controller.Visible = true;
        }

        private void frm_describepicture_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            Classes.Vars.label_timer.Text = "00:30";
            performance.preparation_time(30);            
        }
    }
}
