﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace TryHard_
{
    public partial class frm_panel : Form
    {        
        public frm_panel()
        {
            InitializeComponent();
        }

        Classes.SQLite sql = new Classes.SQLite();
        private DataTable data_table;

        private void frm_panel_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;         
            if (Classes.Cifrado.Desencriptar(Classes.Vars.Privilege) == "Manager")
            {
                pnl_admincontrols.Visible = true;
                Classes.Vars.State = 1;
                //sql.show_list_toeics();
            }
            else
            {
                pnl_admincontrols.Visible = false;
                Classes.Vars.State = 2;                
            }
            lbl_name.Text = Classes.Cifrado.Desencriptar(Classes.Vars.Name) + " " + Classes.Cifrado.Desencriptar(Classes.Vars.Last_Names);
            Thread t = new Thread(()=>
            {
                add_toeics();
                add_controls();
            });
            t.Start();
        }

        private void add_toeics()
        {
            try
            {
                //MessageBox.Show("Starting");
                this.Invoke(new MethodInvoker(delegate { Cursor = Cursors.WaitCursor; }));
                if (Classes.Vars.Upload_state == 0)
                {
                    string db;
                    if (Classes.Vars.State == 1)
                    {
                        db = Classes.Vars.ID;
                    }
                    else
                    {
                        db = Classes.Vars.Provider;
                    }                 
                    //MessageBox.Show(db);
                    Classes.Mega mega = new Classes.Mega();
                    bool file_mega = mega.fileExists(db + "_list.db");
                    bool file_local = Classes.Functions.file_exist(@"C:\TryHard\Data\" + db + "_list.db");
                    if (file_local && file_mega)
                    {
                        if(Classes.Vars.State == 1)
                        {
                            if(MessageBox.Show("You have a database in the web server and also you have one in your computer. Do you want to work with the one storaged on the net?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                string path = @"C:\TryHard\Data\" + db + "_list.db";
                                File.Delete(path);
                                mega.download(path, db + "_list.db");
                                //MessageBox.Show("Done! 1");
                            }
                        }
                        else
                        {
                            if(Classes.Vars.Charging == 1)
                            {
                                string path = @"C:\TryHard\Data\" + db + "_list.db";
                                File.Delete(path);
                                mega.download(path, db + "_list.db");
                            }
                            //MessageBox.Show("Done! 1");                            
                        }  
                    }
                    else
                    {
                        if (file_mega)
                        {
                            string path = @"C:\TryHard\Data\" + db + "_list.db";
                            mega.download(path, db + "_list.db");
                            //MessageBox.Show("Done! 2");
                            //return;
                        }
                        else
                        {
                            if(file_local)
                            {
                                //MessageBox.Show("Done! 3");
                                //return;
                            }
                        }
                    }
                }
                Classes.Vars.Upload_state = 1;
                //MessageBox.Show("Done!");
            }
            catch (CG.Web.MegaApiClient.ApiException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }       

        private void add_controls()
        {
            try
            {
                string db;
                if (Classes.Vars.State == 1)
                {
                    db = Classes.Vars.ID;
                }
                else
                {
                    db = Classes.Vars.Provider;
                }
                if (File.Exists(@"C:\TryHard\Data\" + db + "_list.db"))
                {
                    data_table = new DataTable();
                    data_table = sql.show_list_toeics(db);
                    for (int i = 0; i < data_table.Rows.Count; i++)
                    {
                        for (int j = 0; j < data_table.Columns.Count; j++)
                        {
                            //MessageBox.Show(data_table.Rows[i][j].ToString());
                            this.Invoke(new MethodInvoker(delegate {
                                string tag = data_table.Rows[i][j].ToString();
                                flp_menu.Controls.Add(Classes.Vars.panel_control(tag));
                                flp_menu.Controls["toeic_" + tag].Controls.Add(Classes.Vars.Label_Toeic(int.Parse(tag)));
                                flp_menu.Controls["toeic_" + tag].Controls.Add(Classes.Vars.icon_test());
                                if(Classes.Vars.State != 1)
                                {
                                    flp_menu.Controls["toeic_" + tag].Controls.Add(Classes.Vars.btn_download(tag));
                                }
                                if (Classes.Vars.State == 1)
                                {
                                    flp_menu.Controls["toeic_" + tag].Controls.Add(Classes.Vars.btn_modify(tag));
                                }
                            }));
                        }

                    }
                }
                this.Invoke(new MethodInvoker(delegate { Cursor = Cursors.Default; }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_createsection_Click(object sender, EventArgs e)
        {
            Form frm_createS = new frm_create_section();
            frm_createS.ShowDialog();
        }

        private void btn_addexercise_Click(object sender, EventArgs e)
        {
            frm_select_part selectp = new frm_select_part();
            selectp.Show();
            this.Hide();
        }

        private void frm_panel_Shown(object sender, EventArgs e)
        {
            //Thread t = new Thread(add_controls);
            //t.Start();
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            Classes.Vars.Charging = 0;
            frm_login frm = new frm_login();
            Classes.Vars.form = frm;
            frm.Show();
            this.Close();
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }
    }
}
