﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_infoprovided : Form
    {
        private Classes.Performance performance;

        public frm_infoprovided()
        {
            InitializeComponent();
        }

        private void frm_infoprovided_Load(object sender, EventArgs e)
        {
            Classes.Vars.lbl_text = lbl_text;
            Classes.Vars.pbx_picture = pbx_picture;
        }

        private void frm_infoprovided_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            //lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\ReadingAloud\" + Classes.Vars.Provider + "_reading_" + 1 + "_" + Classes.Vars.Number_toeic + ".tryhard");
            //Classes.Vars.label_timer.Text = "00:45";
            //performance.preparation_time(45);
            //performance.position--;
            performance.read_before();
        }
    }
}
