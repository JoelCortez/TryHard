﻿namespace TryHard_
{
    partial class frm_writtenrequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_direction = new System.Windows.Forms.Label();
            this.txt_response = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_direction
            // 
            this.lbl_direction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_direction.BackColor = System.Drawing.Color.Transparent;
            this.lbl_direction.Font = new System.Drawing.Font("Calibri", 11F);
            this.lbl_direction.Location = new System.Drawing.Point(0, 347);
            this.lbl_direction.Name = "lbl_direction";
            this.lbl_direction.Size = new System.Drawing.Size(700, 54);
            this.lbl_direction.TabIndex = 33;
            this.lbl_direction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_response
            // 
            this.txt_response.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_response.BackColor = System.Drawing.Color.White;
            this.txt_response.Font = new System.Drawing.Font("Cambria", 11F);
            this.txt_response.ForeColor = System.Drawing.Color.DimGray;
            this.txt_response.Location = new System.Drawing.Point(0, 183);
            this.txt_response.Multiline = true;
            this.txt_response.Name = "txt_response";
            this.txt_response.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_response.Size = new System.Drawing.Size(700, 148);
            this.txt_response.TabIndex = 53;
            this.txt_response.UseSystemPasswordChar = true;
            // 
            // txt_email
            // 
            this.txt_email.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_email.BackColor = System.Drawing.Color.White;
            this.txt_email.Font = new System.Drawing.Font("Cambria", 10F);
            this.txt_email.ForeColor = System.Drawing.Color.DimGray;
            this.txt_email.Location = new System.Drawing.Point(0, 4);
            this.txt_email.Multiline = true;
            this.txt_email.Name = "txt_email";
            this.txt_email.ReadOnly = true;
            this.txt_email.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_email.Size = new System.Drawing.Size(700, 148);
            this.txt_email.TabIndex = 54;
            this.txt_email.UseSystemPasswordChar = true;
            // 
            // frm_writtenrequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(700, 400);
            this.Controls.Add(this.txt_email);
            this.Controls.Add(this.txt_response);
            this.Controls.Add(this.lbl_direction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_writtenrequest";
            this.Load += new System.EventHandler(this.frm_writtenrequest_Load);
            this.Shown += new System.EventHandler(this.frm_writtenrequest_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_direction;
        private System.Windows.Forms.TextBox txt_response;
        private System.Windows.Forms.TextBox txt_email;
    }
}