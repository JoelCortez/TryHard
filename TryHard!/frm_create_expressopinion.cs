﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using System.IO;


namespace TryHard_
{
    public partial class frm_create_expressopinion : Form
    {
        OpenFileDialog ofl = new OpenFileDialog();
        WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        public static string filename;

        public frm_create_expressopinion()
        {
            InitializeComponent();
        }

        private void frm_create_expressopinion_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            Classes.Vars.Editing = 1;
            if (Classes.Vars.State_exercise == 1)
            {
                try
                {
                    string path = @"C:\TryHard\Files\Express\" + Classes.Vars.ID + "_express_" + Classes.Vars.Number_toeic + ".mp3";
                    filename = path;
                    path = path.Replace(".mp3", ".tryhard");
                    txt_text.Text = File.ReadAllText(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                txt_text.ReadOnly = true;
                btn_search.Enabled = false;
                btn_save.Text = "Modify";
                btn_next.Text = "Close";
            }
            else
            {
                if (Classes.Vars.Part_selection == 9 || Classes.Vars.Part_selection == 1)
                {
                    btn_next.Text = "Finish";
                    this.Controls.Remove(btn_save);
                }                
            }
            lbl_number_toeic.Text = "TOEIC # " + Classes.Vars.Number_toeic;
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
            wmp.URL = "";
            ofl.Filter = "Mp3|*.mp3|Wav|*.wav";
            if (ofl.ShowDialog() == DialogResult.OK)
            {
                filename = ofl.FileName.ToString();
                if(Classes.Vars.State_exercise == 1)
                {
                    lbl_done.Text = "Changed!";
                }
                else
                {
                    lbl_done.Text = "Done!";
                }
                lbl_done.Visible = true;
            }

        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
            wmp.URL = "";
            if (!string.IsNullOrEmpty(filename))
            {
                if (!string.IsNullOrWhiteSpace(txt_text.Text))
                {
                    save_expression();
                    Form frm = new Form();
                    if(btn_next.Text == "Close")
                    {
                        this.Close();
                        return;
                    }
                    else
                    {
                        if (btn_next.Text == "Finish")
                        {
                            if (Classes.Vars.State_exercise == 1)
                            {
                                this.Close();
                                return;
                            }
                            else
                            {
                                frm = new frm_allparts();
                            }
                        }
                        else
                        {
                            frm = new frm_create_basedonpicture();
                        }
                    }
                    frm.Show();
                    this.Close();

                }
                else
                {
                    MessageBox.Show("Please, complete the statement", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                wmp.settings.volume = 100;
                wmp.URL = filename;
                wmp.controls.play();
            }
            else
            {
                MessageBox.Show("Please, select an audio", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void save_expression()
        {
            Classes.Functions fun = new Classes.Functions();
            Classes.Functions.Path = @"C:\TryHard\Files\Express";
            fun.CreateFolder();
            string path = Classes.Functions.Path + @"\" + Classes.Vars.ID + "_express_" + Classes.Vars.Number_toeic + ".mp3";
            if(filename != path)
            {
                File.Copy(filename, path, true);
            }
            path = path.Replace(".mp3", ".tryhard");
            File.WriteAllText(path, txt_text.Text);
            Classes.SQLite sql = new Classes.SQLite();
            sql.update_parts("opinion", 1);
            if (sql.exercise_state("opinion"))
            {
                Classes.Vars.parts[5] = 1;
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if(btn_save.Text == "Modify")
                {
                    btn_next.Text = "Finish";
                    this.Controls.Remove(btn_save);
                    btn_search.Enabled = true;
                    txt_text.ReadOnly = false;
                }
                else
                {
                    if (!string.IsNullOrEmpty(filename))
                    {
                        if (!string.IsNullOrWhiteSpace(txt_text.Text))
                        {
                            save_expression();
                            Form frm = new frm_allparts();
                            frm.Show();
                            this.Close();

                        }
                        else
                        {
                            if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                            {
                                Form frm = new frm_allparts();
                                frm.Show();
                                this.Close();
                            }
                        }
                    }
                    else
                    {
                        if (MessageBox.Show("You should complete all the parts to save, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        {
                            Form frm = new frm_allparts();
                            frm.Show();
                            this.Close();
                        }
                    }
                }
            }
            catch (Exception)
            {                
            }
        }

        private void btn_record_Click(object sender, EventArgs e)
        {
            Form frm = new frm_record();
            frm.Owner = this;
            frm_record.exercise = 4;
            frm.ShowDialog();
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You'll close the window, Do you want to continue working?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                Form frm = new frm_allparts();
                frm.Show();
                this.Close();
            }
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }
    }
}
