﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TryHard_
{
    public partial class frm_readaloud : Form
    {
        private Classes.Performance performance;

        public frm_readaloud()
        {
            InitializeComponent();
        }

        private void frm_readaloud_Load(object sender, EventArgs e)
        {
            //lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\Reading Aloud\" + "_reading_" + a + "_" + Classes.Vars.Number_toeic + ".tryhard");
            Classes.Vars.panel_timer_controller.Visible = true;
            Classes.Vars.panel_timer_controller.Refresh();
            Classes.Vars.lbl_text = lbl_text;
        }

        private void frm_readaloud_Shown(object sender, EventArgs e)
        {
            performance = new Classes.Performance();
            lbl_text.Text = File.ReadAllText(@"C:\TryHard\Files\ReadingAloud\" + Classes.Vars.Provider + "_reading_" + 1 + "_" + Classes.Vars.Number_toeic + ".tryhard");
            Classes.Vars.label_timer.Text = "00:45";
            performance.preparation_time(45);
        }
    }
}
