﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;

namespace TryHard_
{
    public partial class frm_Updownload : Form
    {        
        public frm_Updownload()
        {
            InitializeComponent();
        }

        Classes.SQLite sql = new Classes.SQLite();
        public static int pgb_step = 3;
        private DataTable table_speaking, table_writing;
        public static int process = 1;
        public static int state = 2;
        Thread t,t0;

        private void copy_resources()
        {
            string destDirName = @"C:\TryHard\res";
            if (!Directory.Exists(destDirName))
            {                
                Classes.Functions.Path = destDirName;
                Classes.Functions fun = new Classes.Functions();
                fun.CreateFolder();
                string sourcePath = "res";
                string fileName = "";
                string[] files = Directory.GetFiles(sourcePath);

                foreach (string s in files)
                {
                    fileName = Path.GetFileName(s);
                    string destFile = Path.Combine(destDirName, fileName);
                    File.Copy(s, destFile, true);
                }
                for (int i = 1; i < 10; i++)
                {
                    Classes.Functions.Path = destDirName + @"\" + i.ToString();
                    fun.CreateFolder();
                    files = Directory.GetFiles(@"res\" + i.ToString());

                    foreach (string s in files)
                    {
                        fileName = Path.GetFileName(s);
                        string destFile = Path.Combine(Classes.Functions.Path + @"\", fileName);
                        File.Copy(s, destFile, true);
                    }
                }
            }
        }

        private void btn_continue_Click(object sender, EventArgs e)
        {

            //ModifyProgressBarColor.SetState(pgb_download, 3);
            //pgb_download.PerformStep();
            //string tg = ((System.Windows.Forms.Button)sender).Tag.ToString();
            if (Classes.Vars.State == 1)
            {
                frm_panel frm = new frm_panel();
                Classes.Vars.form = frm;
                frm.Show();
                this.Close();
            }
            else
            {
                if (btn_continue.Text == "Close")
                {
                    frm_panel frm = new frm_panel();
                    Classes.Vars.form = frm;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    //copy_resources();                    
                    frm_background frm = new frm_background();
                    Classes.Vars.form = frm;
                    frm.Show();
                    this.Close();
                }

            }
        }

        private void frm_Updownload_Shown(object sender, EventArgs e)
        {
            pgb_download.Step = 100 / pgb_step;
            Classes.Threads.progressbar = pgb_download;
            Classes.Threads.label = lbl_info;
            Classes.Threads.button = btn_continue;
            Classes.Threads sub_procesos = new Classes.Threads();
            if (Classes.Vars.State == 1)
            {                
                t = new Thread(() =>
                {                    
                    sub_procesos.Upload_files(table_speaking, table_writing, pgb_download, lbl_info, state);
                });
                t.Start();
            }
            else
            {
                Classes.Threads th = new Classes.Threads();
                t = new Thread(() =>
                {
                    //pgb_download.Step = 100 / process;
                    int limit = 9, initial = 0;
                    if(Classes.Vars.Part_selection == 1)
                    {
                        limit = 6;
                    }
                    else if(Classes.Vars.Part_selection == 2)
                    {
                        limit = 9;
                        initial = 6;
                    }
                    sub_procesos.Download_files(pgb_download, lbl_info, Classes.Vars.Provider, limit, initial);
                });
                t0 = new Thread(() =>
                {
                    
                    if (!th.verify_files_exist(Classes.Vars.Provider))
                    {
                        this.Invoke(new MethodInvoker(delegate { Cursor = Cursors.Default; }));
                        t.Start();
                    }
                    else
                    {
                        MessageBox.Show("Almost to show background");
                        this.Invoke(new MethodInvoker(delegate {
                            Cursor = Cursors.Default;
                            frm_background frm = new frm_background();
                            Classes.Vars.form = frm;
                            frm.Show();
                            this.Close();
                        }));                        
                    }
                });
                t0.Start();
            }
        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            Classes.Vars.Charging = 1;
            frm_panel frm = new frm_panel();
            Classes.Vars.form = frm;
            frm.Show();
            this.Close();
        }

        private void pbx_min_Click(object sender, EventArgs e)
        {
            Classes.MainEvents.minimizar(this);
        }

        private void frm_Updownload_Load(object sender, EventArgs e)
        {
            lbl_name.Text = Classes.Vars.FullName;
            process = 1;
            pgb_step = 3;
            //state = 2;
            if (Classes.Vars.State == 1)
            {                
                check_numbers(Classes.Vars.ID);
            }
            else if(Classes.Vars.State == 2)
            {
                this.Invoke(new MethodInvoker(delegate { Cursor = Cursors.WaitCursor; }));
                //copy_resources();
            }
        }


        private void check_numbers(string id)
        {
            if (state == 1)
            {
                table_speaking = sql.show_datas_speaking(id);
                for (int i = 0; i < table_speaking.Rows.Count; i++)
                {
                    for (int j = 0; j < table_speaking.Columns.Count; j++)
                    {
                        if (table_speaking.Rows[i][j].ToString() != "0")
                        {
                            switch (j)
                            {
                                case 0:
                                    pgb_step += 2;
                                    break;
                                case 2:
                                    pgb_step += 3;
                                    break;
                                case 3:
                                    pgb_step += 3;
                                    break;
                                default:
                                    pgb_step++;
                                    break;
                            }
                            process += 1;
                        }
                    }
                }
                table_writing = sql.show_datas_writing(id);
                for (int i = 0; i < table_writing.Rows.Count; i++)
                {
                    for (int j = 0; j < table_writing.Columns.Count; j++)
                    {
                        if (table_writing.Rows[i][j].ToString() != "0")
                        {
                            switch (j)
                            {
                                case 0:
                                    pgb_step += 5;
                                    break;
                                case 1:
                                    pgb_step += 2;
                                    break;
                                default:
                                    pgb_step++;
                                    break;
                            }
                            process += 1;
                        }
                    }
                }
            }
            else if(state == 2)
            {
                process = 0;
                //table_speaking = sql.show_datas_speaking(id);
                for (int i = 0; i < 6; i++)
                {
                    if (Classes.Vars.parts[i].ToString() != "0")
                    {
                        switch (i)
                        {
                            case 0:
                                pgb_step += 2;
                                break;
                            case 2:
                                pgb_step += 3;
                                break;
                            case 3:
                                pgb_step += 3;
                                break;
                            default:
                                pgb_step++;
                                break;
                        }
                        process += 1;
                    }
                }
                //table_writing = sql.show_datas_writing(id);
                for (int i = 0; i < 3; i++)
                {
                    if (Classes.Vars.parts[i + 6].ToString() != "0")
                    {
                        switch (i)
                        {
                            case 0:
                                pgb_step += 5;
                                break;
                            case 1:
                                pgb_step += 2;
                                break;
                            default:
                                pgb_step++;
                                break;
                        }
                        process += 1;
                    }
                }
            }
            //pgb_download.Step = 100 / pgb_step;
        }
    }

    public static class ModifyProgressBarColor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
        public static void SetState(this ProgressBar pBar, int state)
        {
            SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }
    }
}
