﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryHard_
{
    public partial class frm_user_performance : Form
    {
        public frm_user_performance()
        {
            InitializeComponent();
        }

        private void frm_user_performance_Load(object sender, EventArgs e)
        {

        }

        private void pbx_exit_Click(object sender, EventArgs e)
        {
            frm_panel frm = new frm_panel();
            Classes.Vars.form = frm;
            frm.Show();
            this.Close();
        }
    }
}
